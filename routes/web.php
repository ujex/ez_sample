<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompetitionController;
use App\Http\Controllers\StageController;
use App\Http\Controllers\RaceController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\PrintResultController;
use App\Http\Controllers\PrintStartController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\CalcController;
use App\Http\Controllers\PageController;

require_once __DIR__.'/../app/Helpers/view.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::get('/p/competition', [CompetitionController::class, 'indexAction'])->name('race.competition');
Route::match(['get', 'post'], '/p/competition/add/{id?}', [CompetitionController::class, 'addAction'])->name('race.competition.add');


Route::get('/p/stage', [StageController::class, 'indexAction'])->name('race.stage');
Route::match(['get', 'post'], '/p/stage/edit/{id}', [StageController::class, 'editAction'])->name('race.stage.edit');
Route::match(['get', 'post'], '/p/stage/enrol/{id}', [StageController::class, 'enrolAction'])->name('race.stage.enrol');
Route::get('/p/stage/copy/{id}', [StageController::class, 'copyAction'])->name('race.stage.copy');

Route::match(['get', 'post'], '/p/stage/import-player/{id}', [StageController::class, 'importPlayerAction'])->name('race.stage.import-player');
Route::match(['get', 'post'], '/p/stage/import-ranking/{id}', [StageController::class, 'importRankingAction'])->name('race.stage.import-ranking');


Route::get('/p/race/{id}', [RaceController::class, 'indexAction'])->name('race.race');
Route::match(['get', 'post'], '/p/race/add/{id?}', [RaceController::class, 'addAction'])->name('race.race.add');
Route::match(['get', 'post'], '/p/race/edit/{id}', [RaceController::class, 'editAction'])->name('race.race.edit');
Route::match(['GET', 'POST'], '/p/race/set-result/{stage}/{race?}', [RaceController::class, 'setResultAction'])->name('race.race.set-result');
Route::match(['GET', 'POST'], '/p/race/show-start/{race}', [RaceController::class, 'showStartAction'])->name('race.race.show-start');
Route::match(['GET', 'POST'], '/p/race/show-result/{race}', [RaceController::class, 'showResultAction'])->name('race.race.show-result');


//prints
Route::get('/prints/result/race/{type}/{id}', [PrintResultController::class, 'indexAction'])->name('prints.result.race');
Route::get('/prints/result/team/{type}/{id}', [PrintResultController::class, 'teamAction'])->name('prints.result.team');
Route::get('/prints/result/general/{type}/{id}', [PrintResultController::class, 'generalAction'])->name('prints.result.general');
Route::get('/prints/start/race/{type}/{ord}/{id}', [PrintStartController::class, 'indexAction'])->name('prints.start.race');


Route::match(['GET', 'POST'], '/p/player/edit/{id}/{stage_id?}', [PlayerController::class, 'editAction'])->name('player.edit');
Route::match(['GET', 'POST'], '/p/player/lista-dubli/{id}', [PlayerController::class, 'doubleListAction'])->name('player.double-list');
Route::match(['GET', 'POST'], '/p/player/polacz-duble/{id}/{competition_id}', [PlayerController::class, 'doubleMergeAction'])->name('player.double-merge');

//calc
Route::get('/calc/general/{id}', [CalcController::class, 'generalAction'])->name('calc.general');
Route::get('/calc/team/{id}', [CalcController::class, 'teamAction'])->name('calc.team');
Route::get('/calc/recalculate/{id}', [CalcController::class, 'recalculateAction'])->name('calc.recalculate');

//Route::get('ttt', [PlayerController::class, 'n']);

//public
Route::get('/pobierz/{id}/{out?}/{name?}', [FileController::class, 'downloadAction'])->name('download')->where(['id' => '[0-9]+', 'name' => '[A-Za-z0-9 _ .-]+', 'out' => '[a-z]{1}']);

Route::get('/api/searcg/uci_id/{id}', [ApiController::class, 'searchUciId'])->name('s.api.search.uci_id')->where(['id' => '[0-9]{11}']);

Route::get('/', [SiteController::class, 'index'])->name('s.home');
Route::get('/zawody/{id}', [SiteController::class, 'show'])->name('s.stage');
Route::match(['get', 'post'], '/zapisy/{id}', [SiteController::class, 'sign'])->name('s.sign');

Route::match(['get', 'post'], '/szukaj/{q?}', [SiteController::class, 'search'])->name('s.search');
Route::get('/zgloszenia', [SiteController::class, 'listSignAction'])->name('s.list.sign');
Route::get('/wyscigi/{year?}', [SiteController::class, 'listAction'])->name('s.list')->where(['year' => '[0-9]+']);

Route::get('/strona/{slug}.html', [PageController::class, 'showAction'])->name('s.page');