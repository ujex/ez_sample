<?php
namespace App\Http\Traits;

trait TraitTime {
 
    public $stat = [];
    public $jersey = [];
    public $jersey_reserve = [];
    public static $jersey_is_dubel = false;
    
	public function myStrToTime($sTime)
	{
		$aTime = explode(':', $sTime);
        if(count($aTime) != 3){
            return null;
        }
		return (3600*$aTime[0])+(60*$aTime[1])+($aTime[2]);
	}
    
	public function myTimeToStr($sTime)
	{
		$h = floor($sTime / (60*60));
		$sTime = $sTime - (60*60*$h);
		$m = floor($sTime / (60));
		$sTime = $sTime - (60*$m);
		$s = $sTime;

        return sprintf("%02s", $h).':'.sprintf("%02s", $m).':'.sprintf("%02s", $s);
        
		#return date('H:i:s', $sTime);
	}
	
	public function calcDamage($sPlayerTime, $sLeaderTime)
	{
		$iPlayerTime = $this->myStrToTime( $sPlayerTime );
        $iLeaderTime = $this->myStrToTime( $sLeaderTime );
        if(!$iPlayerTime || !$iLeaderTime){
            return null;
        }
		$iDamage = $iPlayerTime - $iLeaderTime;

        $sRet = $this->niceTime($iDamage);

		return ($sRet?'+'.$sRet:'');
	}
    
    public function niceTime($iDamage){
		$h = floor($iDamage / (60*60));
		$iDamage = $iDamage - (60*60*$h);
		$m = floor($iDamage / (60));
		$iDamage = $iDamage - (60*$m);
		$s = $iDamage;
		$sRet = '';
		if($h){
			$sRet .= $h.':';
		}
		if($m||$sRet){
			$m = ($sRet?sprintf("%02s", $m):$m);
			$sRet .= $m.':';
		}
		if($s||$sRet){
			$s = ($sRet?sprintf("%02s", $s):$s);
			$sRet .= $s;
		}
        
        return ($sRet ? $sRet : '');
    }


    public function calcSpeed($lenght, $time)
	{
		return round(@(($lenght*1000) / $this->myStrToTime($time)) * 3.6, 3);
	}
    
    public function set_stat($key)
    {
        if(isset($this->stat[$key])){
            $this->stat[$key]++;
        }
    }
    
    public function get_stat()
    {
        return [
            'all' => $this->stat['sum'],
            'started' => $this->stat['sum'] - $this->stat['dns'],
            'finished' => $this->stat['sum'] - $this->stat['dns'] - $this->stat['dnf'] - $this->stat['otl'] - $this->stat['dsq'],
            'dsq' => $this->stat['dsq'],
            'otl' => $this->stat['otl'],
        ];
    }
    
    public function reset_stat()
    {
        $this->stat = [
            "dnf" => 0, 
            "dns" => 0, 
            "dsq" => 0, 
            "otl" => 0,
            "sum" => 0
        ];
    }
    
    public function set_jersey($key, $pos, $data, $jersey_opt = [])
    {
        if($pos > 2){
            return;
        }
        if(!isset($this->jersey[$key])){
            $this->jersey[$key]['title'] = isset($jersey_opt['title']) ? $jersey_opt['title'] : '';
            $this->jersey[$key]['sponsor'] = isset($jersey_opt['sponsor']) ? $jersey_opt['sponsor'] : '';
            $this->jersey[$key]['color'] = isset($jersey_opt['color']) ? $jersey_opt['color'] : '';
            $this->jersey[$key]['img_url'] = isset($jersey_opt['img_url']) ? $jersey_opt['img_url'] : '';
            $this->jersey[$key]['ord'] = isset($jersey_opt['ord']) ? $jersey_opt['ord'] : 9999;
        }
        if($pos == 1){
            $this->jersey[$key]['hold'] = [
                'name' => $data->player->getName(),
                'bib' => $data->bib,
                'team' => $data->team->name,
            ];
            if(in_array($data->bib, $this->jersey_reserve)){
                self::$jersey_is_dubel = true;
            }
            $this->jersey_reserve[] = $data->bib;
        }elseif($pos == 2 && self::$jersey_is_dubel){
            self::$jersey_is_dubel = false;
            $this->jersey[$key]['wear'] = [
                'name' => $data->player->getName(),
                'bib' => $data->bib,
                'team' => $data->team->name,
            ];
            $this->jersey_reserve[] = $data->bib;
        }
    }
    
    public function get_jerseys()
    {
        $jersey = collect($this->jersey);
        return $jersey->sortBy('ord');
    }
}