<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{

    public function searchUciId(Request $request)
    {
        $url = "https://sinko.pzkol.pl/pzkol_ws.nsf/excel?OpenAgent&key=tomacich_a0547617%(G8n1QSrVaCLFRv72rWpC)%GOAVUQYVLY&type=ID&rok=" . date('Y') . "&id=" . $request->id . "&dod=biurozawodow.eu";

        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $xml_string = file_get_contents($url, false, stream_context_create($arrContextOptions));
        if (!$xml_string){
            $response = ['status' => 'err', 'msg' => 'Nie odnaleziono zawodnika'];
            return response()->json($response);
        }
        $xml = simplexml_load_string($xml_string);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        if (isset($array['Result']['Status']) && $array['Result']['Status']){
            $response = ['status' => 'ok', 'msg' => 'Odnaleziono zawodnika', 'data' => [
                    'name' => $array['Person']['Imie'],
                    'surname' => $array['Person']['Nazwisko'],
                    'year' => $array['Person']['Rok'],
                    'team' => $array['Person']['Klub'],
                    'sex' => $array['Person']['Plec'],
                    'age' => date('Y') - $array['Person']['Rok'],
            ]];
        }else{
            $response = ['status' => 'err', 'msg' => 'Nie odnaleziono zawodnika'];
        }
        return response()->json($response);
    }

}
