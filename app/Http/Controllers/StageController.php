<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competition;
use App\Models\Race;
use App\Models\File;
use App\Models\Team;
use App\Models\Stage;
use App\Models\Country;
use App\Models\Ranking;
use App\Models\Player;
use App\Models\Playerteam;
use App\Models\StageResult;
use App\Models\Cls;
use App\Models\Category;
use Illuminate\Support\MessageBag;
use App\Http\Traits\TraitPoints;
use App\Http\Traits\TraitTime;
use Auth;
use Str;
use Image;
use DB;
use Carbon\Carbon;

class StageController extends Controller
{

    use TraitPoints,
        TraitTime;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAction(Request $request)
    {
        if ($request->q){
            $q = Str::slug($request->q);
            $query = Stage::with('competition')
                ->where('name', 'like', '%' . $q . '%')
                ->orderBy('date', 'desc');
        }else{
            $query = Stage::with('competition')
                ->orderBy('date', 'desc');
        }

        if (Auth::user()->role != 'admin'){
            $query->whereHas('competition', function ($query) use($request){
                $query->where('user_id', '=', Auth::user()->id);
            });
        }
        $stage = $query->paginate(config('ez.pagination.global'));

        return view('stage.index', [
            'data' => $stage,
        ]);
    }

    public function copyAction(Request $request)
    {
        $stage = Stage::find($request->id);
        if (!$stage){
            abort(404);
        }
        $stage_races = Stage::with('races')->where('id', $request->id)->first();

        $new_stage = $stage->replicate();
        $new_stage->no = $stage->no + 1;
        $new_stage->push();
        $stage_races->races->each(function($item) use($new_stage){
            $new_race = $item->replicate();
            $new_race->stage_id = $new_stage->id;
            $new_race->length = null;
            $new_race->info = null;
            $new_race->push();
        });
        return redirect()->route('race.competition.add', ['id' => $stage->competition_id]);
    }

    public function editAction(Request $request)
    {
        $errors = null;
        $stage = Stage::find($request->id);
        $request->session()->put('stage_id', $stage->id);
        if (!$stage){
            abort(404);
        }
        $stage_info = Stage::with(['races.category' => function($query){
                    $query->orderBy('category.range', 'asc');
                }])
            ->with('files', 'competition')
            ->find($request->id);

        if ($request->unlink){
            $file = File::find($request->file_id);
            if ($file){
                \Illuminate\Support\Facades\Storage::disk('public_uploads')->delete($file->path . '/' . $file->name);
                $file->delete();
            }
            return redirect()->route('race.stage.edit', ['id' => $stage->id]);
        }

        if ($request->unlink_img){
            if ($request->pos == 'img_top'){
                $stage->img_top = null;
                $stage->save();
            }
            if ($request->pos == 'img_bottom'){
                $stage->img_bottom = null;
                $stage->save();
            }
            if ($request->pos == 'image'){
                $stage->image = null;
                $stage->save();
            }
            return redirect()->route('race.stage.edit', ['id' => $stage->id]);
        }


        $img_top = '';
        $img_bottom = '';
        if ($request->isMethod('post')){
            if ($request->is_reg_form){
                $validator = \Validator::make($request->all(), $this->rule_file_reg());
                if ($validator->fails()){
                    $errors = $validator->messages();
                    // dd($errors);
                }else{
                    $file = new File();
                    $file->description = $request->desc;
                    $file->user_id = \Auth::user()->id;
                    $file->category = $request->category;

                    $file->save();
                    // File uploading
                    if ($file_upload = $request->file('reg_file')){
                        $file_name = $file->id . '_' . Str::limit(Str::of($file->description)->slug('_'), 33, '') . '.' . strtolower($file_upload->getClientOriginalExtension());
                        $file->path = 'pliki';
                        $file_upload->storeAs($file->path, $file_name, 'public_uploads');
                        $file->name = $file_name;
                        $file->name_original = $file_upload->getClientOriginalName();
                        $file->ext = strtolower($file_upload->getClientOriginalExtension());
                        $file->mime_type = $file_upload->getClientMimeType();
                        $file->save();
                    }
                    $stage->files()->attach($file->id);
                }
                return redirect()->route('race.stage.edit', ['id' => $stage->id]);
            }

            if ($file = $request->file('image_stage')){
                $file_name = (new \DateTime('now'))->format('Ymd_His') . '.' . strtolower($file->getClientOriginalExtension());

                $file->move(public_path('img/thumbnail/original'), $file_name);
                // Creating thumbnails
                Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(750, 565)->save(public_path('img/thumbnail/750/') . $file_name);
                Image::make(public_path('img/thumbnail/original/') . $file_name)->fit(70, 70)->save(public_path('img/thumbnail/70/') . $file_name);
                $stage->image = $file_name;
                #dd([$file_name, $stage]);
                $stage->save();
            }

            if ($request->name){
                // File uploading
                if ($file = $request->file('img_top')){
                    $fn = explode('.', $file->getClientOriginalName());
                    $img_top = str_replace('-', '', $stage->date) . '_' . $stage->id . rand(1, 999) . '_top.' . array_pop($fn);
                    $file->storeAs('img/pdf_baner', $img_top, 'public_uploads');
                }
                if ($file = $request->file('img_bottom')){
                    $fn = explode('.', $file->getClientOriginalName());
                    $img_bottom = str_replace('-', '', $stage->date) . '_' . $stage->id . rand(1, 999) . '_bottom.' . array_pop($fn);
                    $file->storeAs('img/pdf_baner', $img_bottom, 'public_uploads');
                }

                $validator = \Validator::make($request->all(), $this->rule());
                $stage->fill($request->all());

                if ($validator->fails()){
                    $errors = $validator->messages();
                }else{
                    if ($img_top){
                        $stage->img_top = $img_top;
                    }
                    if ($img_bottom){
                        $stage->img_bottom = $img_bottom;
                    }
                    $stage->save();
                    return redirect()->route('race.stage.edit', ['id' => $stage->id]);
                }
            }
        }
        $race = new Race();
        $category = Category::getToSelect();

        return view('stage.edit', [
            'stage' => $stage,
            'errors' => $request->session()->has('errors') ? $request->session()->get('errors') : $errors,
            'stage_info' => $stage_info,
            'race' => $race,
            'category' => $category,
        ]);
    }

    public function enrolAction(Request $request)
    {
        $status = [
            1 => ['status' => 1, 'name' => 'Nowy'],
            2 => ['status' => 2, 'name' => 'Potwierdzony'],
            0 => ['status' => 0, 'name' => 'Anulowany'],
            3 => ['status' => 3, 'name' => 'Nieopłacony'],
        ];
        $errors = null;
        $stage = Stage::find($request->id);
        if (!$stage){
            abort(404);
        }
        $stage_info = Stage::with(['races.category' => function($query){
                    $query->orderBy('category.range', 'asc');
                }])
            ->with('files', 'competition')
            ->find($request->id);
        $request->session()->put('stage_id', $stage_info->id);
        if (Auth::user()->role != 'admin'){
            if ($stage_info->competition->user_id != Auth::user()->id){
                return abort(401);
            }
        }

        $stage_result = StageResult::with('player', 'category', 'race', 'team')
            ->orderBy('created_at')
            ->where('stage_id', $request->id)
            ->get();

        if ($request->isMethod('post')){
            $p = $request->stage_race;
            $status = $request->stage_status;

            foreach ($p as $k => $v){
                $StageResult = StageResult::where([
                        'player_id' => $k,
                        'stage_id' => $stage->id,
                    ])->first();
                $StageResult->confirmed = $status[$k];
                if ($v){
                    $race = Race::find($v);
                    if ($race){
                        $StageResult->race_id = $race->id;
                        $StageResult->category_id = $race->category_id;
                        $StageResult->save();
                    }
                }
            }
            return redirect()->route('race.stage.enrol', ['id' => $stage->id]);
        }



        return view('stage.enrol', [
            'stage' => $stage,
            'errors' => $request->session()->has('errors') ? $request->session()->get('errors') : $errors,
            'stage_info' => $stage_info,
            'stage_result' => $stage_result,
            'status' => $status,
//            'race' => $race,
//            'category' => $category,
        ]);
    }

    public function importPlayerAction(Request $request)
    {
        $stage = Stage::with('races')->find($request->id);

        if ($request->isMethod('post')){
            if ($file_upload = $request->file('csv_file')){
                $file_name = $stage->id . '-' . date('Ymd-His') . '.' . strtolower($file_upload->getClientOriginalExtension());
                $file_upload->move(storage_path('import'), $file_name);
            }
            DB::beginTransaction();
            $st = true;
            if (($handle = fopen(storage_path('import/' . $file_name), "r")) !== FALSE){
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($st){
                        $st = false;
                        continue;
                    }
                    $import_row = $this->import_row($data);

                    $team = Playerteam::where('name_clean', Str::of($import_row['team'])->slug('-'))->first();
                    if (!$team){
                        $team = new Playerteam();
                        $team->name = ez_uc($import_row['team']);
                        $team->save();
                    }
                    if ($import_row['country']){
                        $country = Country::where('code', 'like', '%' . $import_row['country'] . '%')->first();
                    }

                    $player_info = new Player($import_row);
                    $search_key = $player_info->getSearchKey();

                    $player = Player::where('search_key', $search_key)->where('competition_id', $stage->competition->id)->first();
                    if (!$player){
                        $player = new Player();
                    }

                    $player->name = Str::ucfirst(ez_lc($import_row['name']));
                    $player->surname = Str::ucfirst(ez_lc($import_row['surname']));
                    $player->birth_date = date('Y-m-d', strtotime($import_row['birth_date']));
                    $player->phone = $import_row['phone'];
                    $player->email = $import_row['email'];
                    $player->sex = $import_row['sex'];
                    $player->country_id = $country->id ?? 144;

                    $player->uci_id = trim(str_replace(' ', '', $import_row['uci_id']));

                    $player->team_id = $team->id;
                    $player_age = Carbon::now()->format('Y') - Carbon::parse($player->birth_date)->format('Y');

                    $player->competition_id = $stage->competition->id;
                    $player->save();
                    // dodanie zawodnika do eyścigu
                    $stage_result = StageResult::where([
                            'player_id' => $player->id,
                            'stage_id' => $stage->id,
                        ])->first();
                    if (!$stage_result){
                        $stage_result = new StageResult();
                        $stage_result->resetResult();
                        $stage_result->player_id = $player->id;
                        $stage_result->stage_id = $stage->id;
                    }
                    $stage_result->timestamps = false;
                    $stage_result->created_at = $import_row['created_at'];
                    $stage_result->bib = $import_row['bib'];
                    $stage_result->team_id = $player->team_id;
                    $stage_result->competition_id = $stage->competition->id;

                    $category_set = false;
                    foreach ($stage->races as $race){
                        if (Str::of($race->name)->slug('-') == Str::of($import_row['category'])->slug('-')){
                            $stage_result->race_id = $race->id;
                            $stage_result->category_id = $race->category->id;
                            $category_set = true;
                        }
                    }
                    if (!$category_set){
                        foreach ($stage->races as $race){
                            if (in_array($player_age, explode('|', $race->category->range)) && $player->sex == $race->category->sex){
                                $stage_result->race_id = $race->id;
                                $stage_result->category_id = $race->category->id;
                            }
                        }
                    }

                    $stage_result->save();
                }
            }
            DB::commit();
            return redirect()->route('race.stage.enrol', ['id' => $stage->id]);
        }

        dd($request);
    }

    public function importRankingAction(Request $request)
    {
        $stage = Stage::find($request->id);

        if ($request->isMethod('post')){
            if ($file_upload = $request->file('csv_file')){
                $file_name = $stage->id . '-ranking-' . date('Ymd-His') . '.' . strtolower($file_upload->getClientOriginalExtension());
                $file_upload->move(storage_path('import'), $file_name);
            }
            DB::beginTransaction();
            Ranking::where('stage_id', $stage->id)->delete();
                
            $st = true;
            if (($handle = fopen(storage_path('import/' . $file_name), "r")) !== FALSE){
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($st){
                        $st = false;
                        continue;
                    }
                    if ($data[0]){
                        $uci_id = trim(str_replace(' ', '', $data[0]));
                        $category = trim($data[1]);
                        if (isset($data[2]) && $data[2]){
                            $pts_uci = trim(str_replace(',', '.', $data[2]));
                        }else{
                            $pts_uci = 0;
                        }
                        if (isset($data[3]) && $data[3]){
                            $pts_pzkol = trim(str_replace(',', '.', $data[3]));
                        }else{
                            $pts_pzkol = 0;
                        }

                        $ranking = new Ranking();
                        $ranking->uci_id = $uci_id;
                        $ranking->category = $category;
                        if ($pts_uci){
                            $ranking->pts_uci = $pts_uci;
                        }
                        if ($pts_pzkol){
                            $ranking->pts_pzkol = $pts_pzkol;
                        }
                        $ranking->stage_id = $stage->id;
                        $ranking->save();
                    }
                }
            }
            DB::commit();
            return redirect()->route('race.stage.edit', ['id' => $stage->id]);
        }
    }

    private function rule()
    {
        return [
            'name' => 'required|max:255'
        ];
    }

    private function rule_file_reg()
    {
        return [
            'desc' => 'required|max:256',
            'reg_file' => 'required|file|mimes:' . config('ez.validator.file_upload_in_calendar'),
        ];
    }

    public function deleteResultAction(Request $request)
    {
        if (\Auth::user()->role != 'admin'){
            return abort(401);
        }
        StageResult::where('stage_id', $request->id)->delete();
        ez_reset_ai('stage_result');

        return redirect(\URL::previous());
    }

    private function import_row($data)
    {
        $birth_date = '1900-01-01';
        if ($data[4]){
            if (strlen(trim($data[4])) == 4){
                $birth_date = trim($data[4]) . '-01-01';
            }else{
                $birth_date = date('Y-m-d', strtotime($data[4]));
            }
        }
        $ret = [
            'name' => $data[2] ? Str::ucfirst(ez_lc($data[2])) : '',
            'surname' => $data[3] ? Str::ucfirst(ez_lc($data[3])) : '',
            'uci_id' => $data[1] ? trim(str_replace(' ', '', $data[1])) : null,
            'birth_date' => $birth_date,
            'phone' => $data[8] ? $data[8] : null,
            'email' => $data[7] ? $data[7] : null,
            'sex' => ($data[5] == 'M') ? 'M' : 'F',
            'team' => $data[6] ? ez_uc($data[6]) : 'NIESTOWARZYSZONY',
            'category' => $data[9] ? $data[9] : '',
            'bib' => $data[0] ? $data[0] : 0,
            'country' => $data[10] ? $data[10] : 'POL',
            'created_at' => $data[11] ? $data[11] : date('Y-m-d H:i:s'),
        ];
        return $ret;
    }

}
