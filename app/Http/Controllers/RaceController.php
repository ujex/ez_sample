<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Race;
use App\Models\Tag;
use App\Models\Category;
use App\Models\Stage;
use App\Models\StageResult;
use App\Repositories\StageResultRepository;
use Illuminate\Support\MessageBag;
use App\Http\Traits\TraitTime;
use App\Calc\Calc;
use Auth;
use Str;

class RaceController extends Controller
{

    use TraitTime;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showStartAction(Request $request)
    {
        $races_array = collect(explode(',', $request->race));

        $stage_start = StageResult::with('stage.competition', 'race')
            ->select(['stage_result.*', 'cls.point'])
            ->active()
            ->join('players', 'stage_result.player_id', '=', 'players.id')
            ->join('playerteams', 'stage_result.team_id', '=', 'playerteams.id')
            ->leftJoin('cls', function($join){
                $join->on('stage_result.player_id', '=', 'cls.player_id')
                ->on('stage_result.competition_id', '=', 'cls.competition_id')
                ->on('stage_result.category_id', '=', 'cls.category_id');
            })
            ->with('team')
            ->with('player.country')
            ->whereIn('stage_result.race_id', $races_array)
            #->orderBy('time_start', 'asc')->orderBy('bib', 'asc')->orderBy('playerteams.name', 'asc')
            ->orderBy('time_start', 'asc')->orderBy('bib', 'asc')->orderBy('players.surname', 'asc')
            #->orderBy('rank_score', 'asc')->orderBy('rank_position', 'asc')->orderBy('bib', 'asc')
            ->get();
        $race = Race::whereIn('id', $races_array)->first();
        $stage = Stage::find($race->stage_id);
        $request->session()->put('stage_id', $stage->id);

        if ($request->isMethod('post')){
            foreach ($request->all() as $row){
                if (is_array($row)){
                    $result = StageResult::find($row['id']);
                    if ($row['bib'] || $row['bib'] === '0'){
                        $result->bib = $row['bib'];
                        if ($row['ext']){
                            $result->ext = $row['ext'];
                        }else{
                            $result->ext = null;
                        }
                        $result->save();
                    }else{
                        $result->bib = 0;
                        $result->save();
                    }
                }
            }
            return redirect()->route('race.race.show-start', ['race' => $request->race]);
        }

        return view('race.show-start', [
            'stage_start' => $stage_start,
            'races' => $request->race,
            'race' => $race,
            'stage' => $stage,
        ]);
    }

    public function setResultAction(Request $request)
    {
        $errors = new MessageBag();
        $stage_result_form = new StageResult();
        $races_array = collect(explode(',', $request->race));
        $race = Race::whereIn('id', $races_array)->first();
        $stage = Stage::find($race->stage_id);
        $request->session()->put('stage_id', $stage->id);

        if ($request->isMethod('post')){
            if ($request->length_change){
                $race->length = $request->length;
                $race->save();
                return redirect()->route('race.race.set-result', ['stage' => $request->stage, 'race' => $request->race]);
            }

            $stage_result_list = StageResult::whereIn('race_id', $races_array)
                ->active()
                ->where('bib', $request->bib)
                ->get();
            if ($stage_result_list->count() == 1){
                $stage_result = $stage_result_list->first();
                // Reset
                if ($request->reset == 'Reset'){
                    $stage_result->resetResult();
                    // @todo to chyba musi zniknac zeby po resecie od razu zapisac nowe dane?
                    $stage_result->save();
                    return redirect()->route('race.race.set-result', ['stage' => $request->stage, 'race' => $request->race]);
                }
                if ($stage_result->is_set){
                    $errors->add('is_set', 'Numer ' . $request->bib . ' jest już wprowadzony! Wynik: ' . ($stage_result->is_finish ? 'Miejsce ' . $stage_result->pos : $stage_result->info));
                }else{
                    // Saving
                    if (in_array($request->info, ["dnf", "dns", "dsq", "otl"])){
                        $stage_result->info = $request->info;
                    }else{
                        if ($request->info){
                            $stage_result->info = $request->info;
                        }else{
                            $time = $request->time_h . ':' . $request->time_m . ':' . $request->time_s;
                            if ($request->minus){
                                $time = strtotime($time);
                                $time = date('H:i:s', ($time - $request->minus));
                            }
                            $stage_result->time = $time;
                            $stage_result->time_set = $request->time_set ?? '000';
                        }
                        $stage_result->pos = $request->pos;
                        $stage_result->pos_finish = $request->pos;
                        $stage_result->is_finish = 1;
                    }
                    $stage_result->is_set = 1;
                    $stage_result->save();

                    Calc::model($stage->competition)->calcRacePoints($race);

                    return redirect()->route('race.race.set-result', ['stage' => $request->stage, 'race' => $request->race]);
                }
            }elseif ($stage_result_list->count() > 1){
                $errors->add('is_many', 'Numer ' . $request->bib . ' jest zduplikowany ' . $stage_result_list->count() . ' razy!');
            }else{
                $errors->add('is_no', 'Nie ma takiego numeru: ' . $request->bib . '!');
            }
        }
        $results_all = StageResultRepository::modelForRaces($races_array);
        $results_partial = StageResult::whereIn('race_id', $races_array)
            ->active()
            ->where('is_finish', 1)
            ->orderBy('pos_finish', 'desc')
            ->get();

        $next_record = [
            'time' => '00:00:00',
            'time_h' => '00',
            'time_m' => '00',
            'time_s' => '00',
            'time_set' => '000',
            'pos' => '1',
            'info' => null
        ];
        $time = '00:00:00';
        if ($count = $results_partial->count()){
            $next_record['pos'] = ($results_partial->first()->pos_finish + 1);
            $next_record['time'] = $results_partial->first()->time;
            $time_arr = explode(':', $next_record['time']);
            $next_record['time_h'] = $time_arr[0];
            $next_record['time_m'] = $time_arr[1];
            $next_record['time_s'] = $time_arr[2];
            $next_record['time_set'] = $results_partial->first()->time_set;
            if ($results_partial->first()->info){
                $next_record['info'] = $results_partial->first()->info;
            }
        }
        $results_all_to_set = $results_all->filter(function($item){
            if (!$item->is_finish && !$item->info){
                return true;
            }
        });
        return view('race.set-result', [
            'results_all' => $results_all,
            'stage_result' => $stage_result_form,
            'next_record' => $next_record,
            'standings' => [
                'all' => $results_all->count(),
                'to_set' => $results_all_to_set->count()
            ],
            'races' => $request->race,
            'errors' => $errors,
            'stage' => $stage,
            'race' => $race,
        ]);
    }

    public function showResultAction(Request $request)
    {
        $races_array = collect(explode(',', $request->race));
        if ($request->sort == 'tt'){
            $stage_result = StageResult::with('stage', 'race')
                ->active()
                ->with('team')
                ->with('player.country')
                ->whereIn('race_id', $races_array)
                ->orderBy('is_finish', 'desc')->orderBy('time', 'asc')->orderBy('time_set', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc')
                ->get();
        }else{
            $stage_result = StageResult::with('stage', 'race')
                ->active()
                ->with('team')
                ->with('player.country')
                ->whereIn('race_id', $races_array)
                ->orderBy('is_finish', 'desc')->orderBy('pos_finish', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc')
                ->get();
        }
        $race = Race::whereIn('id', $races_array)->first();
        $stage = Stage::find($race->stage_id);
        $request->session()->put('stage_id', $stage->id);

        if ($request->isMethod('post')){
            foreach ($request->all() as $row){
                if (is_array($row)){
                    $result = StageResult::find($row['id']);
                    if ($row['is_finish']){
                        unset($row['id']);
                        $result->fill($row);
                        if (!$result->pos){
                            $result->pos = 0;
                        }
                        if (!$result->time){
                            $result->time = '00:00:00';
                        }
                        $result->pos_finish = $result->pos;
                        if (!$result->info){
                            $result->info = null;
                        }
                        if (!$result->point){
                            $result->point = null;
                        }
                    }else{
                        $result->resetResult();
                        if ($row['info']){
                            $result->info = $row['info'];
                            $result->is_set = $row['is_set'];
                        }
                    }
                    $result->save();
                }
            }

            return redirect()->route('race.race.show-result', ['race' => $request->race]);
        }
        return view('race.show-result', [
            'stage_result' => $stage_result,
            'races' => $request->race,
            'race' => $race,
            'stage' => $stage,
        ]);
    }

    public function indexAction(Request $request)
    {
        $stages = Stage::with('competition')->with(['races' => function($query){
                    $query->select(['races.*', 'category.range', 'category.sex'])
                    ->join('category', 'races.category_id', '=', 'category.id')
                    ->with(['results' => function($query){
                            $query->active();
                        }])
                    ->orderBy('start_time', 'asc')->orderBy('range', 'asc')->orderBy('sex', 'asc');
                }])
                ->where('id', $request->id)
                ->get();

            return view('race.index', [
                'data' => $stages,
            ]);
        }

        public function addAction(Request $request)
        {
            $race = new Race($request->all());
            if ($request->isMethod('post')){
                $validator = \Validator::make($request->all(), $this->rule());

                if ($validator->fails()){
                    $errors = $validator->messages();
                }else{
                    $race->save();
                }
            }

            return redirect()->route('race.stage.edit', ['id' => $request->stage_id]);
        }

        public function editAction(Request $request)
        {
            $errors = null;
            $race = Race::find($request->id);

            if ($request->isMethod('post')){
                $validator = \Validator::make($request->all(), $this->rule());
                $race->fill($request->all());
                if ($validator->fails()){
                    $errors = $validator->messages();
                }else{
                    $race->save();

                    return redirect(\URL::previous());
                }
            }
            $category = Category::getToSelect();

            return view('race.edit', [
                'errors' => $errors,
                'race' => $race,
                'category' => $category
            ]);
        }

        public function moveToNextStageAction($id)
        {
            $curr_stage = Stage::with('races')->find($id);
            $next_stage = Stage::where([
                    'competition_id' => $curr_stage->competition_id,
                    'no' => $curr_stage->no + 1,
                ])->first();
            if (!$next_stage){
                abort(404);
            }

            if ($curr_stage->races->count()){
                foreach ($curr_stage->races as $race){
                    $new_race = new Race();
                    $new_race->name = $race->name;
                    $new_race->stage_id = $next_stage->id;
                    $new_race->category_id = $race->category_id;

                    $new_race->save();
                }
            }

            return redirect(\URL::previous());
        }

        private function rule()
        {
            return [
                'name' => 'required|max:255'
            ];
        }

    }
    