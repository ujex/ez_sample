<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Race;
use App\Models\Stage;
use App\Http\Traits\TraitTime;
use App\Calc\Calc;
use Auth;
use Str;

class CalcController extends Controller
{

    use TraitTime;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generalAction(Request $request)
    {
        $stage = Stage::find($request->id);
        if (!$stage){
            abort(404);
        }
        if (Auth::user()->role != 'admin'){
            if ($stage->competition->user_id != Auth::user()->id){
                return abort(401);
            }
        }

        $data_i = Calc::model($stage->competition)->generalIndividual($stage->competition);
        
        $data_t = Calc::model($stage->competition)->generalTeam($stage->competition);

        return redirect(\URL::previous());
        return view('debug', [
            'code' => [$data_i, $data_t]
        ]);
    }

    public function teamAction(Request $request)
    {
        $stage = Stage::find($request->id);
        if (!$stage){
            abort(404);
        }
        if (Auth::user()->role != 'admin'){
            if ($stage->competition->user_id != Auth::user()->id){
                return abort(401);
            }
        }

        $data = Calc::model($stage->competition)->calcTeam($stage);

        return redirect(\URL::previous());
        return view('debug', [
            'code' => $data
        ]);
    }

    public function recalculateAction(Request $request)
    {
        $race = Race::find($request->id);
        if (!$race){
            abort(404);
        }
        if (Auth::user()->role != 'admin'){
            if ($race->stage->competition->user_id != Auth::user()->id){
                return abort(401);
            }
        }
        $data = Calc::model($race->stage->competition)->calcRacePoints($race);


        return redirect(\URL::previous());
        return view('debug', [
            'code' => $data
        ]);
    }

}
