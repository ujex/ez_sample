<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stage;
use App\Models\Player;
use App\Models\Playerteam;
use App\Models\Race;
use App\Models\StageResult;
use Illuminate\Support\MessageBag;
use Str;
use Carbon\Carbon;
use Auth;

class FrontController extends Controller
{

    public function __construct()
    {
        $next_stage = Stage::with('competition')
            ->where('date', '>=', date('Y-m-d'))
            ->whereNotNull('date_sign')->where('date_sign', '>=', date('Y-m-d'))
            ->orderBy('date')
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->limit(5)
            ->get();

        $last_stage = Stage::with('competition')
            ->where('date', '<=', date('Y-m-d'))
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->orderBy('date', 'desc')
            ->get();

        view()->composer('*', function ($view) use ($next_stage, $last_stage){
            $view->with('next_stage', $next_stage);
            $view->with('last_stage', $last_stage);
        });
    }

}
