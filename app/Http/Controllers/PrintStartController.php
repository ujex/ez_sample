<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Race;
use App\Models\StageResult;
use App\Http\Traits\TraitTime;
use Auth;
use Str;
use App\Http\Controllers\Prints\Pdf;
use App\Http\Controllers\Prints\Xls;

class PrintStartController extends Controller
{

    use TraitTime;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAction(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $ord = $request->ord;

        setlocale(LC_TIME, config('app.locale'));
        $races_array = collect(explode(',', $id));
        $data = Race::with('stage.competition')
            ->with(['results' => function($query) use ($ord){
                    $query->select(['stage_result.*', 'cls.pos as cls_pos', 'cls.point as cls_point', 'rankings.pts_uci',
                        'rankings.pts_pzkol'])
                    ->with('team')
                    ->active()
                    ->join('players', 'stage_result.player_id', '=', 'players.id')
                    ->join('races', 'stage_result.race_id', '=', 'races.id')
                    ->leftJoin('cls', function($join){
                        $join->on('stage_result.player_id', '=', 'cls.player_id')
                        ->on('stage_result.competition_id', '=', 'cls.competition_id')
                        ->on('stage_result.category_id', '=', 'cls.category_id');
                    })
                    ->leftJoin('rankings', function($join){
                        $join->on('stage_result.stage_id', '=', 'rankings.stage_id')
                        ->on('players.uci_id', '=', 'rankings.uci_id')
                        ->on('races.name', '=', 'rankings.category');
                    })
                    ->with('player.country');
                    switch ($ord){
                        case 'bib':
                            $query->orderBy('time_start', 'asc')->orderBy('stage_result.bib', 'asc');
                            break;
                        case 'cls':
                            $query->orderBy('cls_point', 'desc')->orderBy('cls_pos', 'asc')->inRandomOrder();
                            break;
                        case 'rand':
                            $query->inRandomOrder();
                            break;
                        case 'rank':
                            $query->orderBy('rankings.pts_uci', 'desc')->orderBy('rankings.pts_pzkol', 'desc')->inRandomOrder();
                            break;
                        default:
                            $query->orderBy('time_start', 'asc')->orderBy('stage_result.bib', 'asc');
                            break;
                    }

                    #$query->orderBy(\DB::raw('field(right(stage_result.bib, 1), 5,6,7,4,9,2,8,0,3,1)'), 'asc')->orderBy('stage_result.bib', 'asc');
                    #$query->orderBy('time_start', 'asc')->orderBy('stage_result.bib', 'asc');
                    #$query->orderBy('time_start', 'asc')->orderBy('cls_point', 'desc')->orderBy('cls_pos', 'asc')->inRandomOrder();
                    #$query->orderBy('rank_score', 'asc')->orderBy('rank_position', 'asc')->orderBy('stage_result.bib', 'asc');
                    #$query->orderBy('rank_position', 'asc')->inRandomOrder();
                    #$query->orderBy('team_id', 'asc')->orderBy('surname', 'asc')->orderBy('name', 'asc');
                    #$query->orderBy('cls_point', 'desc')->orderBy('cls_pos', 'asc')->inRandomOrder();
                }])
                ->whereIn('id', $races_array)
                ->get();
            if ($data->isEmpty()){
                abort(404);
            }

            foreach ($data as $race){
                $race->ez_name = $race->stage->name;
                $race->ez_place = ($race->stage->place ? $race->stage->place : $race->stage->competition->place) . ' - ' . ez_date_long($race->stage->date);
                $race->ez_organiser = $race->stage->organiser;
                $race->ez_is_amator = false;
                $race->ez_com = '';
                $race->ez_dir = '';
                $race->ez_is_pts_uci = false;
                $race->ez_is_pts_pzkol = false;

                $race->ez_stat = [
                    'all' => $race->results->count()
                ];

                foreach ($race->results as $result){
                    $result->ez_score = $result->player->rank_score;
                    $result->ez_position = $result->player->rank_position;
                    $result->ez_code = $result->player->type == 'A' ? '(A)' : $result->player->uciId();
                    $result->ez_shufle = $result->player->name_clean;

                    // Classification info
                    #$result->ez_cls = $result->ez_score . '/' . $result->ez_position;
                    $result->ez_cls = $result->cls_pos ? $result->cls_pos : '-';
                    #$result->ez_cls = $result->player->rank_position < 99999 ? $result->player->rank_position : '-';

                    if ($result->player->type == 'A'){
                        $race->ez_is_amator = true;
                    }
                    if ($result->pts_uci){
                        $race->ez_is_pts_uci = true;
                    }
                    if ($result->pts_pzkol){
                        $race->ez_is_pts_pzkol = true;
                    }
                }
            }

            switch ($type){
                case 'xls':
                    $results = new Xls();
                    break;
                case 'pdf':
                    $results = new Pdf();
                    break;
            }
            return $results->start_list($data, $ord);

            return view('debug', [
                'code' => $data
            ]);
        }

    }
    