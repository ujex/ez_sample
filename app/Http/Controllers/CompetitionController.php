<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competition;
use App\Models\Stage;
use App\Models\User;
use App\Repositories\CompetitionRepository;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\CompetitionResult;

class CompetitionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAction(Request $request)
    {
        if ($request->q){
            $q = str_slug($request->q);
            $competition = Competition::with('stages', 'org')
                ->orderBy('date_end', 'desc')
                ->where('name', 'like', '%' . $q . '%')
                ->paginate(config('ez.pagination.global'));
        }else{
            $competition = Competition::with('stages', 'org')
                ->orderBy('date_end', 'desc')
                ->paginate(config('ez.pagination.global'));
        }

        return view('competition.index', [
            'data' => $competition,
            'comp_sets' => $request->session()->get('comp_raw', [])
        ]);
    }

    public function addAction(Request $request)
    {
        $errors = null;
        if ($request->id){
            $competition = Competition::find($request->id);
            $competition_info = Competition::with(['stages' => function($query){
                        $query->orderBy('no', 'asc');
                    }])
                ->find($request->id);
        }else{
            $competition = new Competition($request->all());
            $competition_info = null;
        }

        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            $competition->fill($request->all());

            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $competition->save();
                if (!$request->id){
                    $stage = new Stage();
                    $stage->no = '1';
                    $stage->name = 'Stage 1';
                    $stage->place = $competition->place;
                    $stage->date = $competition->date_start;
                    $stage->competition_id = $competition->id;
                    $stage->save();
                }
                return redirect()->route('race.competition');
            }
        }
        $calc_type = [];
        foreach(config('calc') as $k=>$v){
            $calc_type[$k] = $v['name'];
        }

        $organisers = User::getToSelect();

        return view('competition.add', [
            'competition' => $competition,
            'errors' => $errors,
            'competition_info' => $competition_info,
            'organisers' => $organisers,
            'calc_type' => $calc_type
        ]);
    }

    private function rule()
    {
        return [
            'name' => 'required|max:255',
            'calc_model' => 'required|max:16',
        ];
    }

}
