<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Race;
use App\Models\Stage;
use App\Models\Cls;
use App\Models\ClsTeam;
use App\Models\StageTeam;
use App\Http\Traits\TraitTime;
use Auth;
use Str;
use App\Http\Controllers\Prints\Pdf;
use App\Http\Controllers\Prints\Xls;

class PrintResultController extends Controller
{

    use TraitTime;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAction(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        setlocale(LC_TIME, config('app.locale'));
        $races_array = collect(explode(',', $id));
        $ext = ($request->ext ? $request->ext : null);
        $region = ($request->region ? $request->region : null);
        $data = Race::getResults($ext, $region)
            ->with('stage.competition')
            ->whereIn('races.id', $races_array)
            ->orderBy('start_time', 'asc')
            ->get();
        if ($data->isEmpty()){
            abort(404);
        }

        foreach ($data as $race){
            $race->ez_name = $race->stage->name;
            $race->ez_place = ($race->stage->place ? $race->stage->place : $race->stage->competition->place) . ' - ' . ez_date_long($race->stage->date);
            $race->ez_organiser = $race->stage->organiser;
            $race->ez_winner_time = ($race->results->count() ? $race->results->first()->time : 0); //.'.'.$data->first()->time_set;
            $race->ez_length = (float) $race->length;
            $race->ez_speed = $this->calcSpeed($race->length, $race->ez_winner_time);
            $race->ez_is_amator = false;
            $race->ez_is_ext = false;

            $race->ez_com = '';

            $race->ez_dir = '';


            $this->reset_stat();
            $i = 1;
            $pos_ext = [];
            $price_arr = config('price');
            $prices = [];
            if (isset($price_arr[$race->stage_id])){
                if (isset($price_arr[$race->stage_id][$race->category_id])){
                    $prices = $price_arr[$race->stage_id][$race->category_id . $ext];
                }
            }
            foreach ($race->results as $result){
                if ($ext || $region){
                    $result->ez_pos = $result->is_finish ? $i : '';
                }else{
                    $result->ez_pos = $result->is_finish ? $result->pos : '';
                }
                if (isset($prices[$result->ez_pos]) && $prices[$result->ez_pos]){
                    $result->ez_prize = $prices[$result->ez_pos];
                }else{
                    $result->ez_prize = 0;
                }
                if ($result->ext){
                    $race->ez_is_ext = true;
                    if (isset($pos_ext[$result->ext])){
                        $pos_ext[$result->ext] += 1;
                    }else{
                        $pos_ext[$result->ext] = 1;
                    }
                    $result->ez_ext_pos = $result->is_finish ? $pos_ext[$result->ext] : '';
                }
                $result->ez_code = $result->player->type == 'A' ? '(A)' : $result->player->uciId();
                $result->ez_result = $result->info ? ez_uc($result->info) : $result->time;
                $result->ez_diff = $result->info ? '' : $this->calcDamage($result->time, $race->ez_winner_time);
                $result->ez_point = (float) $result->point ? round($result->point) : '';

                if ($result->player->type == 'A'){
                    $race->ez_is_amator = true;
                }
                $this->set_stat(ez_lc($result->info));
                $this->set_stat('sum');
                $i++;
            }
            $race->ez_stat = $this->get_stat();
        }

        switch ($type){
            case 'xls':
                $results = new Xls();
                break;
            case 'pdf':
                $results = new Pdf();
                break;
        }
        return $results->result($data, ($ext ? $ext : $region));

        return view('debug', [
            'code' => $data
        ]);
    }

    public function teamAction(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        $data = StageTeam::where('stage_id', $id)
            ->with('team', 'stage')
            ->orderBy('pos', 'asc')
            ->where('is_finish', true)
            ->get();

        if ($data->isEmpty()){
            abort(404);
        }
        foreach ($data as $result){
            $result->ez_players = json_decode($result->player_count);
            $result->ez_point = number_format(round($result->point, 2), 2);
        }
        switch ($type){
            case 'pdf':
                $results = new Pdf();
                break;
        }

        return $results->result_team($data);

        dd([
            $data
        ]);
    }

    public function generalAction(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        setlocale(LC_TIME, config('app.locale'));
        $stage = Stage::find($id);
        $stages = Stage::where('competition_id', $stage->competition_id)->orderBy('no', 'asc')->get();
        $arr_stages = [];
        $races_array = collect(explode(',', $request->races));
        $ctg_array = [];
        if($races_array->count()){
            $races = Race::whereIn('id', $races_array)->get();
            if($races){
                foreach($races as $r){
                    $ctg_array[] = $r->category_id;
                }
            }
            
        }
        foreach($stages as $s){
            $arr_stages[$s->id] = '#' . $s->no;
        }
        $q = Cls::with('player.country', 'team')
            ->where('competition_id', $stage->competition_id)
            ->where('is_finish', true)
            ->orderBy('category_id', 'asc')
            ->orderBy('pos', 'asc');
        if($ctg_array){
            $q->whereIn('category_id', $ctg_array);
        }
        $cls = $q->get();
        if($cls->isEmpty()){
            abort(404);
        }
        $data = [];
        $category = 0;
        
        $price_arr = config('price');

        foreach($cls as $result){
            if($result->category_id != $category){
                $category = $result->category_id;
                $ctg = Race::where('stage_id', $id)->where('category_id', $category)->first();
                $data[$category] = [
                    'name' => $ctg,
                    'result' => []
                ];
                
                $prices = [];
                if(isset($price_arr['general'][$stage->competition_id])){
                    if(isset($price_arr['general'][$stage->competition_id][$category])){
                        $prices = $price_arr['general'][$stage->competition_id][$category];
                    }
                }
            }
            $data[$category]['result'][$result->id]['name'] = ez_uc($result->player->surname) . ' ' . $result->player->name;
            $data[$category]['result'][$result->id]['fname'] = $result->player->name;
            $data[$category]['result'][$result->id]['sname'] = $result->player->surname;
            $data[$category]['result'][$result->id]['code'] = $result->player->uciId();
            $data[$category]['result'][$result->id]['uciid'] = $result->player->uci_id;
            $data[$category]['result'][$result->id]['year'] = $result->player->birth_year();
            $data[$category]['result'][$result->id]['team'] = ez_uc(isset($result->team->name) ? $result->team->name : '').' '.ez_uc($result->player->sponsor);
            $data[$category]['result'][$result->id]['point'] = round($result->point, 0);
            $data[$category]['result'][$result->id]['pos'] = $result->pos;
            if(isset($prices[$result->pos]) && $prices[$result->pos]){
                $data[$category]['result'][$result->id]['prize'] = $prices[$result->pos];
            }else{
                $data[$category]['result'][$result->id]['prize'] = 0;
            }
            $stage_count = json_decode($result->stage_count, true);

            foreach($arr_stages as $k=>$v){
                if(isset($stage_count[$k])){
                    $data[$category]['result'][$result->id][$k] = $stage_count[$k];
                }
            }
        }

        $cls_team = ClsTeam::with('team')
            ->where('competition_id', $stage->competition_id)
            ->where('is_finish', true)
            ->orderBy('pos', 'asc')
            ->get();
        foreach($cls_team as $result){
            $result->ez_stages = json_decode($result->stage_count, true);
            $result->ez_point = round($result->point, 2);
        }
        
        
        
        switch ($type){
            case 'pdf':
                $results = new Pdf();
                break;
        }

        return $results->result_general($data, $cls_team, $arr_stages, $stages, $stage);
        
        return view('debug', [
            'code' => $data
        ]);
    }

}
