<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stage;
use App\Models\Player;
use App\Models\Playerteam;
use App\Models\Race;
use App\Models\StageResult;
use Illuminate\Support\MessageBag;
use Str;
use Carbon\Carbon;
use Auth;

class SiteController extends FrontController
{

    public function index(Request $request)
    {
        $stage = Stage::with('competition', 'races')
            ->where('date', '>=', date('Y-m-d'))
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->orderBy('date')
            ->get();

        return view('site.index', [
            'stage' => $stage,
        ]);
    }

    public function listSignAction(Request $request)
    {
        $query = Stage::with('competition', 'races')
            ->where('date', '>=', date('Y-m-d'))
            ->whereNotNull('date_sign')->where('date_sign', '>=', date('Y-m-d'))
            ->orderBy('date')
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->limit(15)
        ;
        $stage = $query->get();

        return view('site.index', [
            'stage' => $stage,
        ]);
    }

    public function listAction(Request $request)
    {
        $query = Stage::with('competition', 'races')
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->orderBy('date')
        ;
        if ($request->year){
            $year_start = $request->year . '-01-01';
            $year_end = $request->year . '-12-31';
            $query->where('date', '>=', $year_start);
            $query->where('date', '<=', $year_end);
        }
        $stage = $query->get();

        return view('site.index', [
            'stage' => $stage,
        ]);
    }

    public function search(Request $request)
    {
        if ($request->isMethod('post')){
            return redirect()->route('s.search', ['q' => $request->s_q]);
        }
        $query = Stage::with('competition', 'races')
            ->whereNotIn('competition_id', config('ez.hidden_series'))
            ->orderBy('date')
        ;
        if ($request->q){
            $q = $request->q;
            $query->where(function($query) use($q){
                $query->where('name', 'like', '%' . $q . '%')
                    ->orWhere('tags', 'like', '%' . $q . '%')
                    ->orWhere('place', 'like', '%' . $q . '%');
            });
        }
        $stage = $query->get();

        return view('site.index', [
            'stage' => $stage,
        ]);
    }

    public function show(Request $request)
    {
        $stage = Stage::with('competition', 'races')
            ->with(['results' => function($query){
                    $query->join('players', 'stage_result.player_id', '=', 'players.id');
                    $query->where('confirmed', 1);

                    $query->orderBy('players.surname');
                }])
            ->with(['races.category' => function($query){
                    $query->orderBy('category.range', 'asc');
                }])
            ->where('id', $request->id)
            ->first();

        if (!$stage){
            abort(404);
        }

        return view('site.show', [
            'stage' => $stage,
            'title' => $stage->name,
        ]);
    }

    public function sign(Request $request)
    {

        $query = Stage::with('competition', 'races')
            ->with(['results' => function($query){
                    $query->where('confirmed', 1);
                }])
            ->with(['races.category' => function($query){
                    $query->orderBy('category.range', 'asc');
                }])
            ->where('id', $request->id)
        ;
        $stage = $query->first();
        if (!$stage){
            return redirect()->route('s.stage', ['id' => $request->id]);
        }


        $errors = new MessageBag();
        $player_info = new Player($request->all());
        if ($request->isMethod('post')){
            $validator = \Validator::make($request->all(), $this->rule());
            if ($validator->fails()){
                $errors = $validator->messages();
            }else{
                $team_name = $request->team;
                if (!$team_name){
                    $team_name = 'NIESTOWARZYSZONY';
                }
                #dd($team_name);
                $team = Playerteam::where('name_clean', Str::of($team_name)->slug('-'))->first();
                if (!$team){
                    $team = new Playerteam();
                    $team->name = ez_uc($team_name);
                    $team->save();
                }

                $search_key = $player_info->getSearchKey();

                $player = Player::where('search_key', $search_key)->where('competition_id', $stage->competition->id)->first();
                if (!$player){
                    $player = new Player();
                }
                $player->name = Str::ucfirst(ez_lc($request->name));
                $player->surname = Str::ucfirst(ez_lc($request->surname));
                $player->birth_date = date('Y-m-d', strtotime($request->birth_date));
                $player->phone = $request->phone;
                $player->email = $request->email;
                $player->sex = $request->sex;
                $player->country_id = $request->country_id;
                $player->uci_id = trim(str_replace(' ', '', $request->uci_id));

                if (!$request->uci_id){
                    $player->uci_id = null;
                }
                if (!$request->address){
                    $player->address = null;
                }
                if (!$request->phone){
                    $player->phone = null;
                }
                if (!$request->email){
                    $player->email = null;
                }
                $player->team_id = $team->id;
                $player_age = Carbon::now()->format('Y') - Carbon::parse($player->birth_date)->format('Y');

                $player->competition_id = $stage->competition->id;
                $player->save();

                // dodanie zawodnika do eyścigu
                $stage_result = StageResult::where([
                        'player_id' => $player->id,
                        'stage_id' => $stage->id,
                    ])->first();
                if (!$stage_result){
                    $stage_result = new StageResult();
                    $stage_result->resetResult();
                    $stage_result->player_id = $player->id;
                    $stage_result->stage_id = $stage->id;
                }
                #dd($StageResult);
                $stage_result->bib = ($request->bib ?? 0);
                $stage_result->team_id = $player->team_id;
                $stage_result->competition_id = $stage->competition->id;

                $raceDb = Race::find($request->ctg);
                if ($raceDb){
                    $stage_result->race_id = $raceDb->id;
                    $stage_result->category_id = $raceDb->category_id;
                }else{
                    foreach ($stage->races as $race){
                        if (in_array($player_age, explode('|', $race->category->range)) && $player->sex == $race->category->sex){
                            $stage_result->race_id = $race->id;
                            $stage_result->category_id = $race->category->id;
                        }
                    }
                }

                $stage_result->save();

                $request->session()->flash('info', __('site.reg.saved'));
                return redirect()->route('s.sign', ['id' => $stage->id]);
            }
        }

        $races = [];
        $racesjs = [];
        if (count($stage->races)){
            $races[null] = 'Wybierz';

            foreach ($stage->races as $r){
                $races[$r->id] = $r->name;
                $racesjs[$r->id] = ['name' => $r->id . '_' . ($r->category->sex == 'F' ? 'K' : 'M') . '_' . $r->category->range];
            }
        }

        return view('site.sign', [
            'stage' => $stage,
            'player' => $player_info,
            'errors' => $errors,
            'races' => $races,
            'racesjs' => $racesjs,
            'country' => \App\Models\Country::getMap(),
            'title' => 'Zapisy do ' . $stage->name . ', ' . $stage->place . ' ' . $stage->date,
        ]);
    }

    private function rule()
    {
        return [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'birth_date' => 'required|date',
            'email' => 'required|email',
            'ctg' => 'required',
//            'ctg' => 'required|integer|min:1',
//            'phone' => 'required|max:64',
//            'uci_id' => 'nullable|numeric|digits:11',
//            'regulation' => 'required',
        ];
    }

}
