<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Stage;
use App\Models\Race;
use App\Models\StageResult;
use App\Models\Playerteam;
use Carbon\Carbon;
use Str;

class PlayerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editAction(Request $request)
    {
        $errors = null;
        $player = Player::find($request->id);
        if (!$player){
            abort(404);
        }
        $double_check = Player::where('search_key', $player->search_key)->where('competition_id', $player->competition_id)->where('id', '<>', $player->id)->first();
        $bib = '';
        $player_team = $player->team->name ?? '';
        $stage = null;
        if ($request->stage_id){
            $stage = Stage::find($request->stage_id);
            if (!$stage){
                abort(404);
            }
            $stage_result = StageResult::where([
                    'player_id' => $player->id,
                    'stage_id' => $request->stage_id,
                ])->first();

            if ($stage_result){
                $player_team = $stage_result->team->name;
                $bib = $stage_result->bib;
            }
        }

        if ($request->isMethod('post')){
            $redir_param = ['id' => $player->id];

            $validator = \Validator::make($request->all(), $this->rule());
            $player->fill($request->all());

            if (!$request->city){
                $player->city = null;
            }
            if (!$request->uci_id){
                $player->uci_id = null;
            }
            if (!$request->license){
                $player->license = null;
            }
            if (!$request->address){
                $player->address = null;
            }
            if (!$request->phone){
                $player->phone = null;
            }
            if (!$request->email){
                $player->email = null;
            }

            if ($validator->fails()){
                $errors = $validator->messages();
            }else{

                $team = Playerteam::where('name_clean', Str::of($request->team)->slug('-'))->first();
                if (!$team){
                    $team = new Playerteam();
                    $team->name = $request->team;
                    $team->save();
                }
                $player->team_id = $team->id;

                if ($request->stage_id){
                    $redir_param['stage_id'] = $request->stage_id;
                    $stage_result = StageResult::where([
                            'player_id' => $player->id,
                            'stage_id' => $request->stage_id,
                        ])->first();

                    if ($stage_result){
                        $stage_result->team_id = $player->team_id;
                        if ($request->bib){
                            $stage_result->bib = $request->bib;
                        }
                        $stage_result->save();
                    }
                }

                unset($player->team);
                unset($player->bib);
                $player->save();
                return redirect()->route('player.edit', $redir_param);
            }
        }

        return view('player.edit', [
            'player' => $player,
            'player_team' => $player_team,
            'bib' => $bib,
            'errors' => $errors,
            'stage' => $stage,
            'country' => \App\Models\Country::getMap(),
            'double_check' => $double_check,
            'stage_id' => $request->stage_id,
            'settings' => [
                'date_now' => Carbon::now(),
                'player_year' => Carbon::now()->format('Y') - Carbon::parse($player->birth_date)->format('Y')
            ]
        ]);
    }

    public function doubleListAction(Request $request)
    {
        $players = \DB::table('players')
            ->select([\DB::raw('count(*) as num'), 'search_key'])
            ->where('competition_id', $request->id)
            ->groupBy('search_key')
            ->having('num', '>', 1)
            ->get();
        if (!count($players)){
            $player = [];
        }else{
            foreach ($players as $p){
                $first_player = Player::where('search_key', $p->search_key)->where('competition_id', $request->id)->orderBy('id')->first();
                $tmp = [
                    'player' => $first_player,
                    'num' => $p->num,
                ];
                $player[] = $tmp;
            }
        }
        # dd($players);
        return view('player.double-list', [
            'player' => $player,
            'competition_id' => $request->id,
        ]);
    }

    public function doubleMergeAction(Request $request)
    {
        $player = Player::find($request->id);
        if (!$player){
            abort(404);
        }
        $player_double = Player::where('search_key', $player->search_key)->where('competition_id', $request->competition_id)->where('id', '<>', $player->id)->get();
        
        if($request->merge){
            $stage_result = StageResult::where('player_id', $request->merge)->where('competition_id', $request->competition_id)->get();
            if(count($stage_result)){
                foreach($stage_result as $sr){
                    $sr->player_id = $player->id;
                    $sr->save();
                }
            }
            Player::where('id', $request->merge)->delete();
            return redirect()->route('player.double-merge', ['id' => $player->id, 'competition_id' => $request->competition_id]);
        }
        
        return view('player.double-merge', [
            'player' => $player,
            'player_double' => $player_double,
            'competition_id' => $request->competition_id,
        ]);
    }

    private function rule()
    {
        return [
            'surname' => 'required|max:255',
            'name' => 'required|max:255',
            'birth_date' => 'required',
            'team' => 'required'
        ];
    }

}
