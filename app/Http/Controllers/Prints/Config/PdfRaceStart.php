<?php

namespace App\Http\Controllers\Prints\Config;

class PdfRaceStart extends PdfConfig
{

    public function print_table_header($ord = '')
    {
        $r_h = config('ez.print.row_height');
        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(10, $this->GetY());
        $this->Cell(10, $r_h, ez_uc(trans('print.header.bib')), 0, 0, 'L');
        $this->Cell(23, $r_h, ez_uc(trans('print.header.uci_id')), 0, 0, 'L');
        $this->Cell(45, $r_h, $this->ch(ez_uc(trans('print.header.name'))), 0, 0, 'L');

        if ($ord == 'cls'){
            $this->Cell(87, $r_h, $this->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
            $this->Cell(25, $r_h, $this->ch(ez_uc(trans('print.header.cls'))), 0, 0, 'L');
        }elseif ($ord == 'bib' || $ord == 'rand'){
            $this->Cell(110, $r_h, $this->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
        }else{
            $this->Cell(87, $r_h, $this->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
        }
    }

    public function print_stat($stat)
    {
        $r_h = config('ez.print.row_height');
        $y = $this->GetY();
        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(20, $y);

        $this->SetXY(15, ($y + $r_h));
        $this->Cell(43, 5, $this->ch(trans('print.enrol') . ': '), 0, 0, 'L');

        $this->SetFont('lsr', '', 8);
        //$this->SetXY(50, ($y + $r_h + 1));
        $this->SetX(50);
        $this->Cell(40, $r_h, $stat['all'], 0, 0, 'L');
    }

}
