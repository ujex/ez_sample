<?php
namespace App\Http\Controllers\Prints\Config;

use Codedge\Fpdf\Fpdf\Fpdf;
use App\Http\Controllers\Prints\Config\TraitPdfHtml;

class PdfConfig extends Fpdf
{
    use TraitPdfHtml;
        
    public $img_top_url = '';
    public $img_footer_url = '';
    public $img_top_height = 0;
    public $img_footer_height = 0;
    
	function __construct( $f = 'A4', $p = 'p')
	{
		parent::__construct( $p, 'mm', $f );
		$this->SetAuthor('');
		$this->SetCreator('');
		$this->SetAutoPageBreak(true, 10);
		$this->SetMargins(7, 7, 7);
		$this->AddFont('lsr','','lsr.php');
		$this->AddFont('lsr','B','lsb.php');

		$this->SetDisplayMode(89);
		$this->AliasNbPages();
        $this->SetFillColor(237, 237, 237);
	}

    public function print_page_header($name, $place, $organiser)
    {
        $this->SetFont('lsr','B', 13);
        $this->Cell(200, 10, $this->ch($name), 0, 1, 'C');
        $this->SetFont('lsr','', 11);
        $this->Cell(200, 7, $this->ch($place), 0, 1, 'C');
        if($organiser){
            $this->SetFont('lsr','B', 9);
            $y = $this->GetY();
            $this->Cell(30, 5, $this->ch(trans('print.organiser').': '), 0, 1, 'L');
            $this->SetFont('lsr','', 9);
            $this->SetXY(37, $y);
            $this->Write(5, $this->ch($organiser));
        }
    }
    
    public function print_info($info, $is_amator)
    {
        if($is_amator){
            $info = '<br>(A) - ' . trans('print.amator') . $info;
        }
        if($info){
            $this->SetFont('lsr','', 8);
            $this->SetXY(30, $this->GetY()+3);
            $this->WriteHTML($this->ch($info));
        }
    }
    
    public function print_communique($communique)
    {
        if($communique){
            $this->SetFont('lsr','', 8);
            $this->SetXY(20, $this->GetY());
            $this->WriteHTML($this->ch($communique));
        }
    }
    
    public function print_sign($commissarie, $director)
    {
        if(!$director && !$commissarie){
            return;
        }
        $y = $this->GetY()+7;
        $this->SetFont('lsr','', 8);
        if($director){
            $this->SetXY(20, $y);
            $this->Cell(80, 5, $this->ch(trans('print.commissarie_finish').': '.$director), 0, 1, 'L');
        }
        if($commissarie){
            $this->SetXY(100, $y);
            $this->Cell(80, 5, $this->ch(trans('print.commissarie').': '.$commissarie), 0, 1, 'R');
        }
    }
    
	public function ch($string)
	{
		return iconv('utf-8', 'iso-8859-2', $string);
	}
    
    public function set_image($img_top, $img_bootom)
    {
        if($img_top || $img_bootom){
            if($img_top && file_exists(public_path('img/pdf_baner/'.$img_top))){
                $this->img_top_url = asset('img/pdf_baner/'.$img_top);
            }
            if($img_bootom && file_exists(public_path('img/pdf_baner/'.$img_bootom))){
                $this->img_footer_url = asset('img/pdf_baner/'.$img_bootom);
            }
            if(1){
                $desc = explode('|', '42|42');
                $this->img_top_height = $desc[0];
                $this->img_footer_height = $desc[1];
            }
            // Overriding margin bottom
            $this->SetAutoPageBreak(true, ($this->img_footer_height + 10));
        }
        //dd($this);
    }

	function Header()
	{
        if($this->img_top_url){
            $this->Image($this->img_top_url, 5, 5, 200, $this->img_top_height);
            $this->Ln($this->img_top_height + 1);
        }
	}
    
	function Footer()
	{
        if($this->img_footer_url){
            $this->Image($this->img_footer_url, 5, (290-$this->img_footer_height-1), 200, $this->img_footer_height);
        }

		$this->Line(10, 290, 200, 290);
		$this->SetFont('lsr','', 6);
		$this->Text(10, 293, date('Y-m-d'));
		$this->Text(70, 293, $this->ch(trans('print.sign')));
		$this->Text(190, 293, trans('print.page').' '.$this->PageNo().'/{nb}');
	}
}