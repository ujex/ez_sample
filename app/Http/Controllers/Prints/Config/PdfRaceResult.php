<?php

namespace App\Http\Controllers\Prints\Config;

class PdfRaceResult extends PdfConfig
{

    public function print_table_header()
    {
        $r_h = config('ez.print.row_height');
        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(10, $this->GetY());
        $this->Cell(13, $r_h, ez_uc(trans('print.header.rank')), 0, 0, 'L');
        $this->Cell(10, $r_h, ez_uc(trans('print.header.bib')), 0, 0, 'L');
        $this->Cell(23, $r_h, ez_uc(trans('print.header.uci_id')), 0, 0, 'L');
        $this->Cell(43, $r_h, $this->ch(ez_uc(trans('print.header.name'))), 0, 0, 'L');
        $this->Cell(73, $r_h, $this->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
        $this->Cell(15, $r_h, ez_uc(trans('print.header.result')), 0, 0, 'L');
        $this->Cell(13, $r_h, ez_uc(trans('print.header.diff')), 0, 0, 'R');
//        if(config('ez.print.pts')){
//            $this->Cell(15, $r_h, ez_uc(trans('print.header.point')), 0, 0, 'R');
//        }
    }

    public function print_table_team_header()
    {
        $r_h = config('ez.print.row_height');
        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(10, $this->GetY());
        $this->Cell(13, $r_h, ez_uc(trans('print.header.rank')), 0, 0, 'L');
        $this->Cell(85, $r_h, $this->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
        $this->Cell(65, $r_h, ez_uc(trans('print.header.players')), 0, 0, 'L');
        $this->Cell(20, $r_h, ez_uc(trans('print.header.point')), 0, 0, 'R');
    }

    public function print_speed($length, $speed)
    {
        if ($length){
            $y = $this->GetY();
            $this->SetFont('lsr', '', 8);
            $this->SetXY(20, $y);
            $this->Cell(80, config('ez.print.row_height'), $this->ch(trans('print.length') . ': ' . number_format($length, 2)), 0, 1, 'L');
            $this->SetXY(100, $y);
            $this->Cell(80, config('ez.print.row_height'), $this->ch(trans('print.speed') . ': ' . $speed), 0, 1, 'R');
        }
    }

    public function print_stat($stat)
    {
        $r_h = config('ez.print.row_height');
        $y = $this->GetY();
        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(20, $y);

        $this->SetFont('lsr', 'B', 8);
        $this->SetXY(15, ($y + $r_h + 1));
        $this->Cell(43, 5, $this->ch(trans('print.enrol') . ': '), 0, 0, 'L');
        $this->SetFont('lsr', '', 8);
        $this->SetX(50);
        $this->Cell(40, $r_h, $stat['all'], 0, 0, 'L');

        $this->SetFont('lsr', 'B', 8);
        $y = $this->GetY();
        $this->SetXY(15, $y + ($r_h));
        $this->Cell(43, $r_h, $this->ch(trans('print.starting') . ': '), 0, 0, 'L');
        $this->SetFont('lsr', '', 8);
        $this->SetX(50);
        $this->Cell(40, $r_h, $stat['started'], 0, 0, 'L');

        $this->SetFont('lsr', 'B', 8);
        $y = $this->GetY();
        $this->SetXY(15, $y + ($r_h));
        $this->Cell(43, $r_h, $this->ch(trans('print.finishing') . ': '), 0, 0, 'L');
        $this->SetFont('lsr', '', 8);
        $this->SetX(50);
        $this->Cell(40, $r_h, $stat['finished'], 0, 0, 'L');

        $this->SetFont('lsr', 'B', 8);
        $y = $this->GetY();
        $this->SetXY(15, $y + ($r_h));
        $this->Cell(43, $r_h, trans('print.dsq') . ': ', 0, 0, 'L');
        $this->SetFont('lsr', '', 8);
        $this->SetX(50);
        $this->Cell(40, $r_h, $stat['dsq'], 0, 0, 'L');
    }

    public function print_communique($communique)
    {
        if ($communique){
            $this->SetFont('lsr', '', 8);
            $this->SetXY(20, $this->GetY());
            $this->WriteHTML($this->ch($communique));
        }
    }

}
