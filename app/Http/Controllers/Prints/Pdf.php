<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Prints\Config\PdfRaceResult;
use App\Http\Controllers\Prints\Config\PdfRaceStart;
use App\Http\Controllers\Prints\Config\PdfRaceStartTeam;
use App\Http\Traits\TraitTime;
use Str;

class Pdf
{

    use TraitTime;

    public function result($data)
    {
        $pdf = new PdfRaceResult();
        //Image settings
        if (isset($data->first()->stage)){
            $pdf->set_image($data->first()->stage->img_top, $data->first()->stage->img_bottom);
        }

        $pdf->AddFont('lsr', '', 'lsr.php');
        $pdf->AddFont('lsr', 'B', 'lsb.php');
        $pdf->SetTitle(isset($data->first()->stage) ? $data->first()->stage->competition->name : '', true);
        $pdf->SetSubject((isset($data->first()->stage) ? $data->first()->stage->competition->name : '') . ' - ' . trans('print.result'), true);

        foreach ($data as $race){
            $pdf->AddPage();
            // Page Head
            $pdf->print_page_header($race->ez_name, $race->ez_place, $race->ez_organiser);

            // Print Type
            $pdf->SetY($pdf->GetY() + 5);
            $pdf->SetFont('lsr', 'B', 13);
            $pdf->Cell(200, 10, $pdf->ch($race->name), 0, 1, 'C');
            $pdf->SetFont('lsr', '', 11);
            $pdf->Cell(200, 7, $pdf->ch(trans('print.result')), 0, 1, 'C');

            // Speed 
            $pdf->print_speed($race->ez_length, $race->ez_speed);

            // Table Head
            $pdf->print_table_header();

            // Table Rows
            $pdf->SetFont('lsr', '', 8);
            $r_h = config('ez.print.row_height');
            $i = 1;
            foreach ($race->results as $result){
                $pdf->SetXY(10, $pdf->GetY() + $r_h);
                $pdf->Cell(13, $r_h, $result->ez_pos, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(10, $r_h, $result->bib, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(23, $r_h, $result->ez_code, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(43, $r_h, $result->ext . $pdf->ch(ez_uc($result->player->surname) . ' ' . $result->player->name), 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(73, $r_h, $pdf->ch(ez_uc(Str::limit($result->team->name ?? '', 37))), 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(15, $r_h, $result->ez_result, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(13, $r_h, $result->ez_diff, 0, 0, 'R', ez_odd($i, true, false));
//                if(config('ez.print.pts')){
//                    $pdf->Cell(15, $r_h, $result->ez_point, 0, 0, 'R', ez_odd($i, true, false));
//                }
                $i++;
            }
            // Player Stats
            $pdf->print_stat($race->ez_stat);
            // Race info
            $pdf->print_info($race->info, $race->ez_is_amator);
            // Race communique
            $pdf->print_communique($race->communique);
            // Race director and commissarie
            $pdf->print_sign($race->ez_com, $race->ez_dir);
        }
        // I (stream) or F (download)
        $file_name = storage_path('app/results/') . 'result_' . date('YmdHis') . '.pdf';
        $pdf->Output('F', $file_name);
        #$pdf->Output('I', $file_name);

        return \Response::download($file_name);
        exit;
    }

    public function start_list($data, $ord = 'bib')
    {
        $pdf = new PdfRaceStart();
        //Image settings
        $pdf->set_image($data->first()->stage->img_top, $data->first()->stage->img_bottom);

        $pdf->max_page_height = 280;
        $pdf->AddFont('lsr', '', 'lsr.php');
        $pdf->AddFont('lsr', 'B', 'lsb.php');
        $pdf->SetTitle($data->first()->stage->competition->name, true);
        $pdf->SetSubject($data->first()->stage->competition->name . ' - ' . trans('print.start_list'), true);

        foreach ($data as $race){
            $pdf->AddPage();
            // Page Head
            $pdf->print_page_header($race->ez_name, $race->ez_place, $race->ez_organiser);

            // Print Type
            $pdf->SetY($pdf->GetY() + 5);
            $pdf->SetFont('lsr', 'B', 13);
            $pdf->Cell(200, 10, $pdf->ch($race->name), 0, 1, 'C');
            $pdf->SetFont('lsr', '', 11);
            $pdf->Cell(200, 7, $pdf->ch(trans('print.start_list')), 0, 1, 'C');

            // Table Head
            $r_h = config('ez.print.row_height');
            $pdf->SetFont('lsr', 'B', 8);
            $pdf->SetXY(10, $pdf->GetY());
            $pdf->Cell(10, $r_h, ez_uc(trans('print.header.bib')), 0, 0, 'L');
            $pdf->Cell(23, $r_h, ez_uc(trans('print.header.uci_id')), 0, 0, 'L');
            $pdf->Cell(45, $r_h, $pdf->ch(ez_uc(trans('print.header.name'))), 0, 0, 'L');

            if ($ord == 'cls'){
                $pdf->Cell(87, $r_h, $pdf->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
                $pdf->Cell(25, $r_h, $pdf->ch(ez_uc(trans('print.header.cls'))), 0, 0, 'R');
            }elseif ($race->ez_is_pts_uci || $race->ez_is_pts_pzkol){
                $pdf->Cell(80, $r_h, $pdf->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
                $pdf->Cell(16, $r_h, $pdf->ch(ez_uc(trans('print.header.cls_uci'))), 0, 0, 'R');
                $pdf->Cell(16, $r_h, $pdf->ch(ez_uc(trans('print.header.cls_pzkol'))), 0, 0, 'R');
            }else{
                $pdf->Cell(110, $r_h, $pdf->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
            }

            // Table Rows
            $pdf->SetFont('lsr', '', 8);
            $i = 1;
            foreach ($race->results as $result){
                $pdf->SetXY(10, $pdf->GetY() + $r_h);
                $pdf->Cell(10, $r_h, $result->bib, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(23, $r_h, $result->ez_code, 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(45, $r_h, $result->ext . $pdf->ch(ez_uc($result->player->surname) . ' ' . $result->player->name), 0, 0, 'L', ez_odd($i, true, false));
                if ($ord == 'cls'){
                    $pdf->Cell (87, $r_h, $pdf->ch (ez_uc (Str::limit ($result->team->name ?? '', 50))), 0, 0, 'L', ez_odd ($i, true, false));
                    $pdf->Cell(25, $r_h, $result->ez_cls, 0, 0, 'R', ez_odd($i, true, false));
                }elseif ($race->ez_is_pts_uci || $race->ez_is_pts_pzkol){
                    $pdf->Cell (80, $r_h, $pdf->ch (ez_uc (Str::limit ($result->team->name ?? '', 50))), 0, 0, 'L', ez_odd ($i, true, false));
                    $pdf->Cell(16, $r_h, ($result->pts_uci?round($result->pts_uci):''), 0, 0, 'R', ez_odd($i, true, false));
                    $pdf->Cell(16, $r_h, ($result->pts_pzkol?round($result->pts_pzkol):''), 0, 0, 'R', ez_odd($i, true, false));
                }else{
                    $pdf->Cell(110, $r_h, $pdf->ch(ez_uc(Str::limit($result->team->name ?? '', 57))), 0, 0, 'L', ez_odd($i, true, false));
                }
                $i++;
            }
            // Player Stats
            $pdf->print_stat($race->ez_stat);
            // Race info
            $pdf->print_info($race->info, $race->ez_is_amator);
            // Race communique
            $pdf->print_communique($race->communique);
            // Race director and commissarie
            $pdf->print_sign($race->ez_com, $race->ez_dir);
        }
        // I (stream) or F (download)
        $file_name = storage_path('app/results/') . 'start_list_' . date('YmdHis') . '.pdf';
        $pdf->Output('F', $file_name);
        #$pdf->Output('I', $file_name);

        return \Response::download($file_name);
        exit;
    }

    public function result_team($data)
    {
        $pdf = new PdfRaceResult();
        //Image settings
        if (isset($data->first()->stage)){
            $pdf->set_image($data->first()->stage->img_top, $data->first()->stage->img_bottom);
        }

        $pdf->AddFont('lsr', '', 'lsr.php');
        $pdf->AddFont('lsr', 'B', 'lsb.php');
        $pdf->SetTitle(isset($data->first()->stage) ? $data->first()->stage->name : '', true);
        $pdf->SetSubject((isset($data->first()->stage) ? $data->first()->stage->name : '') . ' - ' . trans('print.result_team'), true);

        $pdf->AddPage();

        // Print Type
        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetFont('lsr', 'B', 13);
        $pdf->Cell(200, 10, $pdf->ch($data->first()->stage->name), 0, 1, 'C');
        $pdf->SetFont('lsr', '', 11);
        $pdf->Cell(200, 7, $pdf->ch(trans('print.result_team')), 0, 1, 'C');

        // Table Head
        $pdf->print_table_team_header();

        // Table Rows
        $pdf->SetFont('lsr', '', 8);
        $r_h = config('ez.print.row_height');
        $r_h_t_y = $r_h;
        $i = 1;

        $max_height_buffer = ($pdf->img_footer_url ? $pdf->img_footer_height : 0);
        $max_height_bottom = 270 - $max_height_buffer - $r_h;
        foreach ($data as $result){
            $pre_height = $pdf->GetY() + ($r_h * count($result->ez_players));
            if ($pre_height > $max_height_bottom){
                $pdf->AddPage();
            }
            $r_h_t = $r_h * count($result->ez_players);
            if (!$r_h_t){
                $r_h_t = $r_h;
            }

            $y = $pdf->GetY() + $r_h_t_y;
            $pdf->SetXY(10, $y);
            $pdf->Cell(13, $r_h_t, $result->pos, 0, 0, 'L', ez_odd($i, true, false));
            $pdf->Cell(85, $r_h_t, $pdf->ch(ez_uc(Str::limit($result->team->name ?? '', 45))), 0, 0, 'L', ez_odd($i, true, false));
            //$pdf->MultiCell(85, $r_h_t, $pdf->ch(ez_uc($result->team->name . ' '.$result->team->name ?? '')), 0, 'L', ez_odd($i, true, false));
            $pdf->SetX(108);
            $yp = 1;
            foreach ($result->ez_players as $p){
                $pdf->Cell(65, $r_h, $pdf->ch($p->player . ' (' . number_format(round($p->point, 2), 2) . 'p.)'), 0, 0, 'L', ez_odd($i, true, false));
                #$pdf->Cell(65, $r_h, $pdf->ch($p->race . ' - ' . $p->player . ' (' . number_format(round($p->point, 2), 2) . 'p.)'), 0, 0, 'L', ez_odd($i, true, false));

                $pdf->SetXY(108, $y + ($r_h * $yp));
                $yp++;
            }
            $pdf->SetXY(173, $y);
            $pdf->Cell(20, $r_h_t, $result->ez_point, 0, 0, 'R', ez_odd($i, true, false));

            $r_h_t_y = $r_h_t;


            $i++;
        }
        // I (stream) or F (download)
        $file_name = storage_path('app/results/') . 'result_team_' . date('YmdHis') . '.pdf';
        $pdf->Output('F', $file_name);
        #$pdf->Output('I', $file_name);

        return \Response::download($file_name);
        exit;
    }

    public function result_general($data, $data_team, $arr_stages, $stages, $stage)
    {
        $pdf = new PdfRaceResult();
        //Image settings
        if (isset($stage)){
            $pdf->set_image($stage->img_top, $stage->img_bottom);
        }

        $pdf->AddFont('lsr', '', 'lsr.php');
        $pdf->AddFont('lsr', 'B', 'lsb.php');
        $pdf->SetTitle($data_team->first()->competition->name, true);
        $pdf->SetSubject($data_team->first()->competition->name . ' - ' . trans('print.result'), true);


        $raceNo = 0;
        foreach ($data as $race){
            $raceNo++;
            $pdf->AddPage();
            if ($raceNo == 1){
                $pdf->SetFont('lsr', 'B', 15);
                $pdf->Cell(200, 10, $pdf->ch($data_team->first()->competition->name), 0, 1, 'C');
            }

            // Print Type
            $pdf->SetY($pdf->GetY() + 5);
            $pdf->SetFont('lsr', 'B', 13);
            $pdf->Cell(200, 10, $pdf->ch($race['name']->name), 0, 1, 'C');
            $pdf->SetFont('lsr', '', 11);
            $pdf->Cell(200, 7, $pdf->ch(trans('print.cls')), 0, 1, 'C');

            // Table Head
            $r_h = config('ez.print.row_height');
            $pdf->SetFont('lsr', 'B', 8);
            $pdf->SetXY(10, $pdf->GetY());
            $pdf->Cell(13, $r_h, ez_uc(trans('print.header.rank')), 0, 0, 'L');
            $pdf->Cell(43, $r_h, $pdf->ch(ez_uc(trans('print.header.name'))), 0, 0, 'L');
            $pdf->Cell(73, $r_h, $pdf->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
            foreach ($arr_stages as $k => $v){
                $pdf->Cell(6, $r_h, $v, 0, 0, 'C');
            }
            $pdf->Cell(15, $r_h, ez_uc(trans('print.header.point')), 0, 0, 'L');


            // Table Rows
            $pdf->SetFont('lsr', '', 8);
            $i = 1;
            foreach ($race['result'] as $result){
                $pdf->SetXY(10, $pdf->GetY() + $r_h);
                $pdf->Cell(13, $r_h, $result['pos'], 0, 0, 'L', ez_odd($i, true, false));

                $pdf->Cell(43, $r_h, $pdf->ch($result['name']), 0, 0, 'L', ez_odd($i, true, false));
                $pdf->Cell(73, $r_h, $pdf->ch(ez_uc(Str::limit($result['team'] ?? '', 37))), 0, 0, 'L', ez_odd($i, true, false));
                foreach ($arr_stages as $k => $v){
                    if (isset($result[$k]['status']) && $result[$k]['status'] == 0){
                        $pdf->Cell(6, $r_h, isset($result[$k]) ? $result[$k]['point'] : '-', 0, 0, 'C', ez_odd($i, true, false));
                    }else{
                        $pdf->Cell(6, $r_h, isset($result[$k]) ? $result[$k]['point'] : '-', 0, 0, 'C', ez_odd($i, true, false));
                    }
                }
                $pdf->Cell(15, $r_h, $result['point'], 0, 0, 'C', ez_odd($i, true, false));

                $i++;
            }
            $pdf->SetXY(10, $pdf->GetY() + $r_h);
            foreach ($stages as $stage){
                $pdf->SetXY(10, $pdf->GetY() + $r_h);
                $pdf->Cell(15, $r_h, '', 0, 0, 'L');
                $pdf->Cell(15, $r_h, $pdf->ch(('#' . $stage->no . ': ')), 0, 0, 'L');
                $pdf->Cell(130, $r_h, $pdf->ch(($stage->name . ' (' . $stage->date . ')')), 0, 0, 'L');
            }
        }


        $pdf->AddPage();
        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetFont('lsr', '', 13);
        $pdf->Cell(200, 7, $pdf->ch(trans('print.cls_team')), 0, 1, 'C');

        // Table Head
        $r_h = config('ez.print.row_height');
        $pdf->SetFont('lsr', 'B', 8);
        $pdf->SetXY(10, $pdf->GetY());
        $pdf->Cell(13, $r_h, ez_uc(trans('print.header.rank')), 0, 0, 'L');
        $pdf->Cell(99, $r_h, $pdf->ch(ez_uc(trans('print.header.team'))), 0, 0, 'L');
        foreach ($arr_stages as $k => $v){
            $pdf->Cell(9, $r_h, $v, 0, 0, 'C');
        }
        $pdf->Cell(15, $r_h, ez_uc(trans('print.header.point')), 0, 0, 'L');

        $pdf->SetFont('lsr', '', 8);
        $i = 1;
        foreach ($data_team as $result){
            $pdf->SetXY(10, $pdf->GetY() + $r_h);
            $pdf->Cell(13, $r_h, $result->pos, 0, 0, 'L', ez_odd($i, true, false));

            $pdf->Cell(99, $r_h, $pdf->ch(ez_uc(Str::limit($result->team->name ?? '', 53))), 0, 0, 'L', ez_odd($i, true, false));
            foreach ($arr_stages as $k => $v){
                $pdf->Cell(9, $r_h, isset($result->ez_stages[$k]) ? round($result->ez_stages[$k]['point'], 2) : '-', 0, 0, 'C', ez_odd($i, true, false));
            }
            $pdf->Cell(15, $r_h, $result->ez_point, 0, 0, 'C', ez_odd($i, true, false));
            $i++;
        }

        $pdf->SetXY(10, $pdf->GetY() + $r_h);
        foreach ($stages as $stage){
            $pdf->SetXY(10, $pdf->GetY() + $r_h);
            $pdf->Cell(15, $r_h, '', 0, 0, 'L');
            $pdf->Cell(15, $r_h, $pdf->ch(('#' . $stage->no . ': ')), 0, 0, 'L');
            $pdf->Cell(130, $r_h, $pdf->ch(($stage->name . ' (' . $stage->date . ')')), 0, 0, 'L');
        }


        // I (stream) or F (download)
        $file_name = storage_path('app/results/') . 'result_general_' . date('YmdHis') . '.pdf';
        $pdf->Output('F', $file_name);
        #$pdf->Output('I', $file_name);

        return \Response::download($file_name);
        exit;
    }

}
