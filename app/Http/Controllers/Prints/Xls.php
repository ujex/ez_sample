<?php

namespace App\Http\Controllers\Prints;

use Excel;

class Xls
{

    public function result($data)
    {

        $fileName = 'result_' . date('YmdHis') . '.csv';

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use($data){
            $columns = [
                ez_uc(trans('print.header.rank')), ez_uc(trans('print.header.bib')), ez_uc(trans('print.header.uci_id')),
                ez_uc(trans('print.header.lastname')), ez_uc(trans('print.header.firstname')), ez_uc(trans('print.header.country')),
                ez_uc(trans('print.header.team')),
                ez_uc(trans('print.header.sex')), ez_uc(trans('print.header.result')), ez_uc(trans('print.header.category'))
            ];



            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ';');

            foreach ($data as $race){
                foreach ($race->results as $item){
                    $tmp = [
                    $item->ez_pos,
                    $item->bib,
                    $item->player->uci_id,
                    ($item->player->surname),
                    ($item->player->name),
                    $item->player->country->code,
                    ($item->team->name ?? ''),
                    $item->player->uciSex(),
                    $item->ez_result,
                    $race->name
                    ];
                    fputcsv($file, $tmp, ';');
                }
            }

            fclose($file);
        };

        return response()->streamDownload($callback, $fileName, $headers);
    }

    public function start_list($data)
    {
        $fileName = 'start_list_' . date('YmdHis') . '.csv';

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use($data){
            $columns = [
                ez_uc(trans('print.header.lp')), ez_uc(trans('print.header.bib')), ez_uc(trans('print.header.uci_id')),
                ez_uc(trans('print.header.lastname')), ez_uc(trans('print.header.firstname')), ez_uc(trans('print.header.country')),
                ez_uc(trans('print.header.team')),
                ez_uc(trans('print.header.sex')), ez_uc(trans('print.header.category'))
            ];



            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ';');

            foreach ($data as $race){
                $i = 1;
                foreach ($race->results as $item){
                   fputcsv($file, [
                        $i,
                        $item->bib,
                        $item->player->uci_id,
                        $item->player->surname,
                        $item->player->name,
                        $item->player->country->code,
                        $item->team->name ?? '',
                        $item->player->uciSex(),
                        $race->name
                    ], ';');
                    $i++;
                }
            }

            fclose($file);
        };

        return response()->streamDownload($callback, $fileName, $headers);
    }

}
