<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;

class FileController extends Controller
{

    public function __construct()
    {
        
    }

    public function downloadAction(Request $request)
    {
        $file = File::active()->where('id', $request->id)->where('name', $request->name)->first();
        if (!$file){
            abort(404);
        }
        $file->downloads += 1;
        $file->save();

        if ($request->out == 's'){
            return \Response::file(public_path($file->path) . '/' . $file->name);
        }
        return \Response::download(public_path($file->path) . '/' . $file->name, $file->name);
    }

}
