<?php

/**
 * Make string uppercase
 *
 * @param string $expression
 * @return string converted string
 */
function ez_uc($expression)
{
    return mb_convert_case($expression, MB_CASE_UPPER, "utf-8");
}

/**
 * Make string lowercase
 *
 * @param string $expression
 * @return string converted string
 */
function ez_lc($expression)
{
    return mb_convert_case($expression, MB_CASE_LOWER, "utf-8");
}

/**
 * Change national letter to standard 
 * 
 * @param string $expression
 * @return string converted string
 */
function ez_ba($expression)
{
    #return $expression;
    return str_replace(
    ['Ą', 'ą', 'Ć', 'ć', 'Ę', 'ę', 'Ł', 'ł', 'Ń', 'ń', 'Ó', 'ó', 'Ś', 'ś', 'Ź', 'ź', 'Ż', 'ż'], 
    ['A', 'a', 'C', 'c', 'E', 'e', 'L', 'l', 'N', 'n', 'O', 'o', 'S', 's', 'Z', 'z', 'Z', 'z'], $expression);
}

/**
 * Check odd in queue
 *
 * @param int $iterator
 * @param mixed $pos return this value if iterator is odd
 * @param mixed $neg return this value if iterator is not odd
 * @return mixed pos or neg
 */
function ez_odd($iterator, $pos, $neg = '')
{
    return ($iterator % 2) ? $pos : $neg;
}

/**
 * Price format
 *
 * @param float $price
 * @param string $dot point separator
 * @return string price
 */
function ez_price($price, $dot = '.')
{
    return number_format(round($price, 2), 2, $dot, ' ');
}

/**
 * Convert date to human readable
 * 
 * @param string $date
 * @return string formated $date
 */
function ez_date_long($date)
{
    if (config('app.locale') != 'pl'){
        return \Carbon\Carbon::parse($date)->formatLocalized('%d %B %Y');
    }
    $months = ['01' => 'Stycznia', '02' => 'Lutego', '03' => 'Marca', '04' => 'Kwietnia', '05' => 'Maja', '06' => 'Czerwca',
        '07' => 'Lipca', '08' => 'Sierpnia', '09' => 'Września', '10' => 'Października', '11' => 'Listopada', '12' => 'Grudnia'];
    $date_exploded = explode('-', $date);
    return $date_exploded[2] . ' ' . ez_lc($months[$date_exploded[1]]) . ' ' . $date_exploded[0];
}

/**
 * New line
 *
 * @return string new line
 */
function ez_br()
{
    return '<br>';
}

/**
 * Page break
 *
 * @return string page break
 */
function ez_pb()
{
    return '<br style="page-break-before: always">';
}

/**
 * Prints country flag
 * 
 * @param App\Models\Country $country
 * @param bolean $force if true check config
 * @param string $size flag size (16,32,64,128)
 * @return string
 */
function ez_flag(App\Models\Country $country, $force = false, $size = '16')
{
    if ($force){
        if (!config('ez.print.flag')){
            return '';
        }
    }
    if ($country->code_short){
        return '<img src="' . asset('assets/flags/' . $size . '/' . ($country->code_short) . '.png') . '" alt="' . $country->code_short . '" title="' . $country->code . '">';
    }
    return '';
}

/**
 * Reset auto increment
 *
 * @param string $table
 */
function ez_reset_ai($table){
    $max = \DB::table($table)->max('id');
    \Db::select('alter table `'.$table.'` auto_increment = ' . ($max+1));
}
