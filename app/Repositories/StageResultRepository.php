<?php

namespace App\Repositories;

use DB;
use App\Models\StageResult;
use Illuminate\Support\Collection;

class StageResultRepository
{
    /**
     * Get all results for races
     *
     * @return \Illuminate\Support\Collection
     */
    public static function forRaces(Collection $races_array)
    {
        $team = "select `t`.`name` as `t_name` from `teams` as `t` inner join `player_team` as `pt` on `pt`.`team_id` = `t`.`id` where `pt`.`player_id` = `p`.`id` limit 1";
        return DB::table('stage_result as sr')
            ->select('sr.*')
            ->addSelect('p.name', 'p.surname', 'p.uci_id', 'p.id as p_id')
            ->addSelect('c.code', 'c.code_short')
            ->addSelect('s.name as s_name')
            ->selectSub($team, 't_name')
            ->join('players as p', 'p.id', '=', 'sr.player_id')
            ->join('country as c', 'c.id', '=', 'p.country_id')
            ->join('stages as s', 's.id', '=', 'sr.stage_id')
            ->whereIn('sr.race_id', $races_array)
            ->orderBy('sr.is_finish', 'desc')->orderBy('sr.pos_finish', 'asc')->orderBy('sr.info', 'asc')->orderBy('sr.bib', 'asc')
            ->get();
    }
    
    /**
     * Get all results for races
     *
     * @return \Illuminate\Support\Collection
     */
    public static function modelForRaces(Collection $races_array)
    {
        return StageResult::with('stage', 'race')
//            ->with(['player.teams' => function($query){
//                $query->orderBy('belongs_from', 'desc');
//            }])
            ->active()
            ->with('team')
            ->with('player.country')
            ->whereIn('race_id', $races_array)
            ->orderBy('is_finish', 'desc')->orderBy('pos_finish', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc')
            ->get();
    }
    
    /**
     * Get all results for stage
     *
     * @return \Illuminate\Support\Collection
     */
    public static function modelForStage($stage)
    {
        return StageResult::with('stage')
            ->with('team')
            ->with('player.country')
            ->where('stage_id', $stage)
            ->orderBy('is_finish', 'desc')->orderBy('pos_finish', 'asc')->orderBy('info', 'asc')->orderBy('time_start', 'asc')->orderBy('bib', 'asc')
            ->get();
    }
}