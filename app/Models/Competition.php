<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{

    public $timestamps = true;
    protected $table = 'competitions';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'place', 'date_start', 'date_end', 'img_top', 'img_bottom', 'user_id','org_phone','org_email',
        'calc_model'];

    // protected $dates = ['date_start', 'date_end'];

    /**
     * Get the stages for the competition.
     */
    public function stages()
    {
        return $this->hasMany('App\Models\Stage');
    }

    public function org()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the cls for the competition.
     */
    public function cls()
    {
        return $this->hasMany('App\Models\Cls');
    }

}
