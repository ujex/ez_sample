<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{

    public $timestamps = true;
    protected $table = 'races';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'length', 'info', 'stage_id', 'category_id',
        'start_time'];

    public static function getResults($ext = '', $region = '')
    {
        return self::with(['results' => function($query) use($ext, $region){
                    $query->with('team')
                        ->with('player.country')
                        ->where('is_set', true)
                        ->orderBy('is_finish', 'desc')->orderBy('pos', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc');
                    if ($ext){
                        $query->where('ext', $ext);
                    }
                    $query->leftJoin('players', 'stage_result.player_id', '=', 'players.id');
                    if ($region){
                        $query->where('region', $region);
                    }
                }]);
    }

    /**
     * Get the stage record associated with the race.
     */
    public function stage()
    {
        return $this->belongsTo('App\Models\Stage');
    }

    /**
     * Get the category record associated with the race.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get the results for the race.
     */
    public function results()
    {
        return $this->hasMany('App\Models\StageResult');
    }

    public function save(array $options = [])
    {
        if ($this->length){
            $this->length = str_replace(',', '.', $this->length);
        }

        parent::save();
    }

}
