<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playerteam extends Model
{

    public $timestamps = true;
    protected $table = 'playerteams';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'name_clean', 'code', 'country_id'];

    /**
     * The players that belong to the team.
     */
    public function players()
    {
        return $this->hasMany('App\Models\Player');
    }

    /**
     * Get the country that owns the team.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function save(array $options = [])
    {
        if ($this->name){
            $this->name_clean = \Str::of($this->name)->slug('-');
        }
        parent::save();
    }


}
