<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = true;
    protected $table = 'country';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'code', 'ord'];
    
    
    public static function getMap()
    {
        return \Cache::remember('country_list', config('ez.cache_time.country_list'), function () {
            $country = self::orderBy('ord', 'asc')->get();
            $map = [];
            foreach($country as $c){
                $map[$c->id] = $c->name;
            }
            return $map;
        });
    }


    /**
     * Get the players for the country.
     */
    public function players()
    {
        return $this->hasMany('App\Models\Player');
    }
    
    /**
     * Get the teams for the country.
     */
    public function teams()
    {
        return $this->hasMany('App\Models\Team');
    }
}
