<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $table = 'category';
    protected $guarded = ['id'];
    protected $fillable = ['name', 'name_en', 'name_short', 'range', 'sex', 'ord'];
    

    public static function getToSelect()
    {
        $category = self::orderBy('ord', 'asc')
            ->orderBy('range', 'asc')
            ->orderBy('sex', 'asc')
            ->get();
        $map = [];
        foreach($category as $c){
            $map[$c->id] = $c->name . ' - ' . $c->sex . ' - ' . $c->ord . ' - ' . $c->range;
        }
        return $map;
    }
}
