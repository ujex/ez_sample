<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * @property $id
 * @property $pos
 * @property $time
 * @property $time_set
 * @property $info
 * @property $is_finish
 * @property $point
 * @property $ord_1
 * @property $ord_2
 * @property $ord_3
 * @property $player_count
 * @property $team_id
 * @property $stage_id
 * @property $race_id
 * @property $created_at
 * @property $updated_at
 */
class StageTeam extends Model
{
    public $timestamps = true;
    protected $table = 'stage_team';
    protected $guarded = ['id'];
    protected $fillable = ['pos', 'time', 'time_set', 'info', 'is_finish', 'point', 
        'ord_1', 'ord_2', 'ord_3', 'player_count', 'team_id', 'stage_id', 'race_id'];
    
    
    /**
     * Get the stage that owns the results.
     */
    public function stage()
    {
        return $this->belongsTo('App\Models\Stage');
    }
    
    /**
     * Get the race that owns the results.
     */
    public function race()
    {
        return $this->belongsTo('App\Models\Race');
    }
    
    /**
     * Get the team that owns the results.
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Playerteam');
    }
    
    public function getTime()
    {
        $time = Carbon::parse($this->time);
        return ($time->hour?$time->hour.'h ':'').$time->minute.'m '.$time->second.'s';
    }
    
    public function getIntTime()
    {
		$aTime = explode(':', $this->time);
		return (3600*$aTime[0])+(60*$aTime[1])+($aTime[2]);
    }

}
