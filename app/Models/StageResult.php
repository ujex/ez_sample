<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StageResult extends Model
{

    public $timestamps = true;
    protected $table = 'stage_result';
    protected $guarded = ['id'];
    protected $fillable = ['bib', 'ext', 'pos', 'pos_finish', 'time', 'time_set', 'time_start', 'time_start_hour',
        'info', 'is_finish', 'is_set',
        'point', 'bonkar', 'player_id', 'stage_id', 'race_id', 'category_id', 'team_id', 'competition_id'];

    public function resetResult()
    {
        $this->pos = 0;
        $this->pos_finish = 0;
        $this->time = '00:00:00';
        $this->time_set = '000';
        $this->info = null;
        $this->is_finish = 0;
        $this->is_set = 0;
        $this->point = null;
        $this->bonkar = 0;
    }

    public function scopeStarted($query)
    {
        return $query->where('info', 'not like', 'dns')->orWhereNull('info');
    }

    public function scopeActive($query)
    {
        return $query->where('confirmed', '>', 0);
    }

    /**
     * Get the stage that owns the results.
     */
    public function stage()
    {
        return $this->belongsTo('App\Models\Stage');
    }

    /**
     * Get the race that owns the results.
     */
    public function race()
    {
        return $this->belongsTo('App\Models\Race');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get the player that owns the results.
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player');
    }

    /**
     * Get the team that owns the results.
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Playerteam');
    }

    /**
     * Get the competition record associated with the stage.
     */
    public function competition()
    {
        return $this->belongsTo('App\Models\Competition');
    }

    public function getTime()
    {
        $time = Carbon::parse($this->time);
        return ($time->hour ? $time->hour . 'h ' : '') . $time->minute . 'm ' . $time->second . 's';
    }

    public function getIntTime()
    {
        $aTime = explode(':', $this->time);
        return (3600 * $aTime[0]) + (60 * $aTime[1]) + ($aTime[2]);
    }

    /**
     * Did not finish
     */
    const INFO_DNF = 'DNF';

    /**
     * Did not start
     */
    const INFO_DNS = 'DNS';

    /**
     * Disqualified
     */
    const INFO_DSQ = 'DSQ';

    /**
     * Lapped
     */
    const INFO_LAP = 'LAP';

    /**
     * Over Time Limit
     */
    const INFO_OTL = 'OTL';

    /**
     * Relegated
     */
    const INFO_REL = 'REL';

    /**
     * Overlapped
     */
    const INFO_OVL = 'OVL';

}
