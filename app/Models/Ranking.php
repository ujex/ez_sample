<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{

    public $timestamps = false;
    protected $table = 'rankings';
    protected $fillable = ['uci_id', 'category', 'pts_uci', 'pts_pzkol'];

}
