<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Player extends Model
{

    public $timestamps = true;
    protected $table = 'players';
    protected $fillable = ['name', 'surname', 'name_clean', 'birth_date', 'country_id', 'sex',
        'uci_id', 'city', 'address', 'sponsor', 'license', 'phone', 'email','region','team','ctg','bib'
    ];

    public function team()
    {
        return $this->belongsTo('App\Models\Playerteam');
    }

    /**
     * Year of birth
     * 
     * @return string
     */
    public function birth_year()
    {
        $time = Carbon::parse($this->birth_date);
        return $time->format('Y');
    }

    public function getName()
    {
        return $this->surname . ' ' . $this->name;
    }

    /**
     * Formated UCI ID
     * 
     * @return string
     */
    public function uciId()
    {
        return trim(chunk_split($this->uci_id, 3, ' '));
    }

    public function uciSex()
    {
        return $this->sex == 'F' ? 'W' : $this->sex;
    }

    public function polSex()
    {
        return $this->sex == 'F' ? 'K' : $this->sex;
    }

    /**
     * Formated phone
     * 
     * @return string
     */
    public function getPhone($format = 1)
    {
        if (!$this->phone){
            return null;
        }
        if ($format == 1){
            return trim(chunk_split('+' . substr($this->phone, 2), 3, ' '));
        }else if ($format == 2){
            return $this->phone;
        }
        return $this->phone;
    }

    /**
     * Get the country that owns the player.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the stage results for the player.
     */
    public function stage_results()
    {
        return $this->hasMany('App\Models\StageResult');
    }

    public function save(array $options = [])
    {
        if ($this->name && $this->surname){
            $this->name_clean = \Str::of($this->surname . ' ' . $this->name)->slug('-');
        }
        $this->search_key = $this->getSearchKey();
        if(!$this->country_id){
            $this->country_id = 144;
        }

        parent::save();
    }

    public function getSearchKey()
    {
        return \Str::of($this->surname . ' ' . $this->name . ' ' . $this->birth_year() . ' ' . trim(str_replace(' ', '', $this->uci_id)))->slug('-');
    }

    /**
     * Male
     */
    const MALE = 'M';

    /**
     * Female
     */
    const FEMALE = 'F';

    /**
     * Player type
     */
    const TYPE_P = 'P';

    /**
     * Szkółka type
     */
    const TYPE_SZ = 'Z';

    /**
     * Amator type
     */
    const TYPE_A = 'A';

    /**
     * Team Manager type
     */
    const TYPE_M = 'M';

    /**
     * Commissarie type
     */
    const TYPE_C = 'C';

    /**
     * Organiser type
     */
    const TYPE_O = 'O';

    /**
     * Medical type
     */
    const TYPE_L = 'L';

    /**
     * Stuff type
     */
    const TYPE_S = 'S';

    /**
     * Other type
     */
    const TYPE_I = 'I';
    const PHONE_PLUS = 1;
    const PHONE_00 = 2;

    public static function typeDesc($type)
    {
        return [
            'P' => 'Rider',
            'A' => 'Amator',
            'Z' => 'Szkółka',
            'M' => 'Team Manager',
            'C' => 'Commissarie',
            'O' => 'Organiser',
            'S' => 'Team support',
            'V' => 'Press',
            ][$type];
    }

    public function typeType()
    {
        return [
            'P' => 'Rider',
            'A' => 'Amator',
            'Z' => 'Szkółka',
            'M' => 'Team Manager',
            'C' => 'Commissarie',
            'O' => 'Organiser',
            'S' => 'Team support',
            'V' => 'Press',
            'L' => 'Medical',
            'I' => 'Other',
            ][$this->type];
    }

}
