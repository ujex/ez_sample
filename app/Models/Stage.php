<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Stage extends Model
{

    public $timestamps = false;
    protected $table = 'stages';
    protected $guarded = ['id'];
    protected $fillable = ['no', 'name', 'place', 'date', 'date_sign', 'img_top', 'img_bottom', 'image', 'competition_id','tags'];

    public static function getResults($calc_model)
    {
        return self::with(['results' => function($query) use($calc_model){
                    $query->with('team')
                        ->with('player.country')
                        ->where('is_set', true);
                    if ($calc_model == 'road_pt'){
                        $query->orderBy('is_finish', 'desc')->orderBy('pos_finish', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc');
                    }else{
                        $query->orderBy('is_finish', 'desc')->orderBy('pos', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc');
                    }
                }]);
    }

    public static function getMergedResults($races)
    {
        return self::with(['results' => function($query) use($races){
                    $query->with('team')
                        ->with('player.country', 'race')
                        ->where('is_set', true)
                        ->whereIn('race_id', $races)
                        ->orderBy('is_finish', 'desc')->orderBy('pos_finish', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc');
                }]);
    }

    /**
     * Get the competition record associated with the stage.
     */
    public function competition()
    {
        return $this->belongsTo('App\Models\Competition');
    }

    /**
     * Get the races for the stage.
     */
    public function races()
    {
        return $this->hasMany('App\Models\Race');
    }

    /**
     * Get the stage results for the stage.
     */
    public function results()
    {
        return $this->hasMany('App\Models\StageResult');
    }

    public function getDate()
    {
        return Carbon::parse($this->date);
    }

    public function team_results()
    {
        return $this->hasMany('App\Models\StageTeam');
    }

    /**
     * The files that belong to the competition.
     */
    public function files()
    {
        return $this->belongsToMany('App\Models\File');
    }

}
