<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * @property $id
 * @property $pos
 * @property $pos_before
 * @property $time
 * @property $time_set
 * @property $info
 * @property $is_finish
 * @property $point
 * @property $ord_1
 * @property $ord_2
 * @property $ord_3
 * @property $stage_count
 * @property $team_id
 * @property $competition_id
 * @property $created_at
 * @property $updated_at
 */
class ClsTeam extends Model
{
    public $timestamps = true;
    protected $table = 'cls_team';
    protected $guarded = ['id'];
    protected $fillable = ['pos', 'pos_before', 'time', 'time_set', 'info', 'is_finish', 'point', 
        'ord_1', 'ord_2', 'ord_3', 'stage_count', 'team_id', 'competition_id'];
    
    
    /**
     * Get the Competition that owns the classification.
     */
    public function competition()
    {
        return $this->belongsTo('App\Models\Competition');
    }
    
    /**
     * Get the team that owns the results.
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Playerteam');
    }
    
    public function getTime()
    {
        $time = Carbon::parse($this->time);
        return ($time->hour?$time->hour.'h ':'').$time->minute.'m '.$time->second.'s';
    }

}
