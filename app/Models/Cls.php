<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cls extends Model
{
    public $timestamps = true;
    protected $table = 'cls';
    protected $guarded = ['id'];
    protected $fillable = ['bib', 'ext', 'is_finish', 'pos', 'pos_before', 'time', 'time_clean', 'time_set', 'point', 'info', 'ord_1', 'ord_2', 'ord_3', 
        'stage_count', 'player_id', 'competition_id', 'race_id', 'category_id', 'team_id'];
    protected $attributes = [
        'time' => '00:00:00',
        'time_clean' => '00:00:00',
        'pos' => 0
    ];

    
    /**
     * Get the competition that owns the cls.
     */
    public function competition()
    {
        return $this->belongsTo('App\Models\Competition');
    }
    
    /**
     * Get the race that owns the cls.
     */
    public function race()
    {
        return $this->belongsTo('App\Models\Race');
    }
    
    /**
     * Get the category that owns the cls.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
    /**
     * Get the player that owns the cls.
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Player');
    }
    
    /**
     * Get the team that owns the cls.
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Playerteam');
    }
    
    
    public function getTime()
    {
        $time = Carbon::parse($this->time);
        return ($time->hour?$time->hour.'h ':'').$time->minute.'m '.$time->second.'s';
    }
    
    public function getIntTime()
    {
		$aTime = explode(':', $this->time);
		return (3600*$aTime[0])+(60*$aTime[1])+($aTime[2]);
    }
}
