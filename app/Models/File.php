<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    public $timestamps = true;
    protected $table = 'files';
    protected $fillable = ['name', 'path', 'name_original', 'description', 'category',
        'mime_type', 'ext', 'source', 'status', 'downloads', 'user_id'];
    protected $attributes = [
        'name' => '',
        'name_original' => '',
        'path' => '',
        'status' => 1,
    ];
    protected $dates = ['created_at', 'updated_at'];

    public function extIco()
    {
        switch ($this->ext){
            case 'doc':
            case 'docx':
            case 'odt':
                return 'fa-file-word-o';
            case 'pdf':
                return 'fa-file-pdf-o';
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                return 'fa-file-photo-o';
            case 'xls':
            case 'xlsx':
                return 'fa-file-excel-o';
            default:
                return 'fa-file-o';
        }
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * The competitions that belong to the file.
     */
    public function stagess()
    {
        return $this->belongsToMany('App\Models\Stage');
    }

    /**
     * Get the user record associated with the file.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Category Results
     */
    const CATEGORY_RESULT = 'result';

    /**
     * Category Regulation
     */
    const CATEGORY_REGULATION = 'regulation';

    /**
     * Category Others
     */
    const CATEGORY_OTHER = 'other';

}
