<?php

namespace App\Calc;

use App\Models\Competition;
use Str;

class Calc
{

    public static function model(Competition $competition)
    {
        $calc_model = Str::studly($competition->calc_model);
        $class_calc_model = '\App\Calc\\' . $calc_model;

        if (!class_exists($class_calc_model)){
            throw new \Exception('Brak modelu wyliczenia ' . $class_calc_model);
        }

        return new $class_calc_model(config('calc.' . $competition->calc_model));
    }

}
