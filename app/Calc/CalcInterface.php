<?php

namespace App\Calc;

use App\Models\Stage;
use App\Models\Competition;
use App\Models\Race;

interface CalcInterface
{

    public function calcTeam(Stage $stage);

    public function generalIndividual(Competition $competition);

    public function generalTeam(Competition $competition);

    public function calcRacePoints(Race $race);
}
