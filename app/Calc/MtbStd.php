<?php

namespace App\Calc;

use App\Models\Stage;
use App\Models\Cls;
use App\Models\Competition;
use App\Models\StageResult;
use App\Models\StageTeam;
use Carbon\Carbon;

class MtbStd extends CalcBase implements CalcInterface
{

    public function generalIndividual(Competition $competition)
    {
        $stages = Stage::with(['results' => function($query){
                    $query->with('team')
                    ->with('player.country')
                    ->active()
                    ->where('is_set', true);
                    $query->orderBy('is_finish', 'desc')->orderBy('pos', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc');
                }])
            ->where('competition_id', $competition->id)
            ->orderBy('no', 'asc')
            ->get();

        // Przeniesienie punktow do collection
        $cls = collect();
        $stage_counter = $stages->count();
        foreach ($stages as $stage){
            // przed kazdym etapem ustawiamy wszystkim 0 punktow w ostatnim wyscigu, jak zapunktuja to sie nadpisze
            foreach ($cls as $k => $item){
                $arr = $cls->pull($k);
                $arr['ord_1'] = 0;
                $cls->put($k, $arr);
            }
            foreach ($stage->results as $result){
                $point = round($result->point);
                if (!$point){
                    if ($this->config['only_with_points']){
                        continue;
                    }
                }
                if ($cls->has($result->player_id)){
                    $arr = $cls->pull($result->player_id);
                }else{
                    $arr = [
                        'player_id' => $result->player_id,
                        'category_id' => $result->category_id,
                        'race_id' => null,
                        'team_id' => null,
                        'ext' => $result->ext,
                        'pos' => 99999,
                        'is_finish' => 0,
                        'point' => 0,
                        'ord_1' => 0,
                        'stage_count' => [],
                        'counter' => 0,
                        'counter_finish' => 0,
                    ];
                }
                $arr['race_id'] = $result->race_id;
                $arr['team_id'] = $result->team_id;
                $arr['point'] += $point;
                $arr['ord_1'] = $point;
                if ($result->info != 'dns' && $result->info != 'dsq'){
                    $arr['counter'] += 1;
                }
                if ($result->info != 'dns' && $result->info != 'dsq' && $result->info != 'dnf'){
                    $arr['counter_finish'] += 1;
                }
                $arr_point = [
                    'stage' => $stage->id,
                    'stage_name' => $stage->name,
                    'pos' => $result->pos,
                    'point' => $point,
                    'status' => 1,
                ];
                $arr['stage_count'][$stage->id] = $arr_point;

                $cls->put($result->player_id, $arr);
            }
        }

        // jezeli nie wszystkie wyscigi w generalce
        if ($this->config['stage_count']){
            foreach ($cls as $k => $item){
                $arr = $cls->pull($k);
                if ($arr['counter'] > $this->config['stage_count']){
                    $lowest = 0;
                    $st = 0;
                    $z = true;
                    foreach ($arr['stage_count'] as $s){
                        if ($z || $lowest > $s['point']){
                            $lowest = $s['point'];
                            $st = $s['stage'];
                            $z = false;
                        }
                    }
                    $arr['stage_count'][$st]['status'] = 0;
                    $arr['point'] -= $lowest;
                }
                $cls->put($k, $arr);
            }
        }

        // utworzenie sortera
        foreach ($cls as $k => $item){
            $arr = $cls->pull($k);
            $arr['sort'] = sprintf('%1$05d%2$07d%3$07d', $arr['category_id'], $arr['point'], $arr['ord_1']);
            $cls->put($k, $arr);
        }

        // Sortowanie
        $cls = $cls->sortByDesc(function($item){
            return $item['sort'];
        });

        //Cleaning old classification for this competition
        \DB::table('cls')->where('competition_id', $competition->id)->delete();
        ez_reset_ai('cls');

        // zapis danych do bazy
        $insert = [];
        $pos = 0;
        $category = 0;
        foreach ($cls as $item){
            if ($item['category_id'] != $category){
                $category = $item['category_id'];
                $pos = 1;
            }
            $arr = [
                'ext' => $item['ext'],
                'pos' => $pos,
                'point' => $item['point'],
                'ord_1' => $item['ord_1'],
                'stage_count' => json_encode($item['stage_count']),
                'player_id' => $item['player_id'],
                'competition_id' => $competition->id,
                'category_id' => $item['category_id'],
                'race_id' => $item['race_id'],
                'team_id' => $item['team_id'],
                'counter' => $item['counter'],
                'counter_finish' => $item['counter_finish'],
            ];

            // poniżesze rozpatrujemy dla wystartowane lub ukonczone
            $key = $this->config['stages_key'];
            if ($this->config['all_stages']){
                // musi zaliczyc wszystkie etapy
                if ($stage_counter == $item[$key]){
                    $arr['pos'] = $pos;
                    $arr['is_finish'] = 1;
                    $pos++;
                }else{
                    $arr['pos'] = 0;
                    $arr['is_finish'] = 0;
                }
            }elseif ($this->config['stage_min_count']){
                //musi zaliczyc wymagana ilosc etapow
                if ($this->config['stage_min_count'] <= $item[$key]){
                    $arr['pos'] = $pos;
                    $arr['is_finish'] = 1;
                    $pos++;
                }else{
                    $arr['pos'] = 0;
                    $arr['is_finish'] = 0;
                }
            }else{
                $arr['pos'] = $pos;
                $arr['is_finish'] = 1;
                $pos++;
            }

            $insert[] = $arr;
        }
        #dd($insert);
        Cls::insert($insert);

        return $cls;
    }

    public function generalTeam(Competition $competition)
    {
        $stages = Stage::with('competition')
            ->with(['team_results' => function($query){
                    //$query->where('is_finish', 1);
                }])
            ->where('competition_id', $competition->id)
            ->orderBy('no', 'asc')
            ->get();

        $cls = collect();
        foreach ($stages as $stage){
            // przed kazdym etapem ustawiamy wszystkim 0 punktow w ostatnim wyscigu, jak zapunktuja to sie nadpisze
            foreach ($cls as $k => $item){
                $arr = $cls->pull($k);
                $arr['ord_1'] = 0;
                $cls->put($k, $arr);
            }
            foreach ($stage->team_results as $result){
                $point = round($result->point);
                if (!$point){
                    continue;
                }
                if ($cls->has($result->team_id)){
                    $arr = $cls->pull($result->team_id);
                }else{
                    $arr = [
                        'team_id' => $result->team_id,
                        'pos' => 99999,
                        'is_finish' => 0,
                        'point' => 0,
                        'ord_1' => 0,
                        'stage_count' => [],
                    ];
                }
                $arr['point'] += $point;
                $arr['ord_1'] = $point;
                $arr_point = [
                    'stage' => $stage->id,
                    'stage_name' => $stage->name,
                    'pos' => $result->pos,
                    'point' => $point
                ];
                $arr['stage_count'][$stage->id] = $arr_point;

                $cls->put($result->team_id, $arr);
            }
        }

        // utworzenie sortera
        foreach ($cls as $k => $item){
            $arr = $cls->pull($k);
            $arr['sort'] = sprintf('%1$07d%2$07d', $arr['point'], $arr['ord_1']);
            $cls->put($k, $arr);
        }

        // Sortowanie
        $cls = $cls->sortByDesc(function($item){
            return $item['sort'];
        });

        //Cleaning old team classification for this competition
        \DB::table('cls_team')->where('competition_id', $competition->id)->delete();
        ez_reset_ai('cls_team');

        // zapis danych do bazy
        $insert = [];
        $pos = 1;
        foreach ($cls as $item){
            $arr = [
                'pos' => $pos,
                'point' => $item['point'],
                'ord_1' => $item['ord_1'],
                'stage_count' => json_encode($item['stage_count']),
                'team_id' => $item['team_id'],
                'competition_id' => $competition->id,
                'is_finish' => 1,
            ];
            $pos++;
            $insert[] = $arr;
        }
        \App\Models\ClsTeam::insert($insert);
        return $insert;
    }

    public function calcTeam(Stage $stage)
    {
        $stage_result = StageResult::with('race', 'player')
            ->active()
            ->where('stage_id', $stage->id)
            ->where('is_finish', true)
            ->where('point', '>', 0)
            ->orderBy('race_id', 'asc')->orderBy('pos', 'asc')
            ->get();

        $points = collect();
        foreach ($stage_result as $result){
            if (isset($this->config['ctg'][$result->category_id])){
                $arr = [
                    'player' => $result->player,
                    'race' => $result->race,
                    'category_id' => $result->category_id,
                    'race_id' => $result->race_id,
                    'team_id' => $result->team_id,
                    'point' => $result->point * $this->config['ctg'][$result->category_id],
                    'pos' => $result->pos,
                ];
                $points->put($result->player_id, $arr);
            }
        }
        $points = $points->sortByDesc('point');

        $team_points = collect();
        $team_points_ctg = collect();
        foreach ($points as $point){
            // wywalamy niestowarzyszonych i co tam trzeba bedzie
            if (in_array($point['team_id'],  array("18", "68", "73", "86", "21", "74", "75"))){
                continue;
            }
            if($this->config['player_count'] == 'stage'){
                $key = $point['team_id'];
            }elseif($this->config['player_count'] == 'race'){
                $key = $point['team_id'] . '_' . $point['category_id'];
            }else{
                $key = $point['team_id'];
            }
            
            if ($team_points_ctg->has($key)){
                $arr = $team_points_ctg->pull($key);
            }else{
                $arr = [
                    'team_id' => $point['team_id'],
                    'point' => 0,
                    'counter' => 0,
                    'stage_id' => $stage->id,
                    'race_id' => $point['race_id'],
                    'player_count' => []
                ];
            }
            if ($arr['counter'] < $this->config['player_count'] || !$this->config['player_count']){
                $arr['point'] += $point['point'];
                $arr['counter'] ++;
                $arr['player_count'][] = [
                    'player' => $point['player']->getName(),
                    'race' => $point['race']->name,
                    'point' => $point['point'],
                    'pos' => $point['pos']
                ];
            }

            $team_points_ctg->put($key, $arr);
        }
        foreach ($team_points_ctg as $pctg){
            $key = $pctg['team_id'];
            if ($team_points->has($key)){
                $arr = $team_points->pull($key);
            }else{
                $arr = [
                    'team_id' => $pctg['team_id'],
                    'point' => 0,
                    'stage_id' => $stage->id,
                    'race_id' => $pctg['race_id'],
                    'counter' => $pctg['counter'],
                    'player_count' => []
                ];
            }
            $arr['point'] += $pctg['point'];
            foreach ($pctg['player_count'] as $pc){
                $arr['player_count'][] = $pc;
            }

            $team_points->put($key, $arr);
        }
        #dd($team_points_ctg);

        $team_points = $team_points->sortByDesc('point');


        StageTeam::where('stage_id', $stage->id)->delete();
        ez_reset_ai('stage_team');
        $pos = 1;
        foreach ($team_points as $item){
            $stage_team = new StageTeam();
            $stage_team->pos = $pos;
            
            //$stage_team->is_finish = true;  //gdy wszyscy
			// conajmniej 2 zawodników to klub
            if($item['counter'] >= 2){
                $stage_team->pos = $pos;
                $stage_team->is_finish = true;
                $pos++;
            }else{
                $stage_team->pos = 9999;
                $stage_team->is_finish = false;
            }
            $stage_team->point = $item['point'];
            $stage_team->player_count = json_encode($item['player_count']);
            $stage_team->team_id = $item['team_id'];
            $stage_team->stage_id = $stage->id;
            $stage_team->save();

           // $pos++;
        }

        return $points;
    }

}
