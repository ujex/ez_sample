<?php

namespace App\Calc;

use App\Models\Stage;
use App\Models\Competition;
use App\Models\Race;
use App\Models\StageResult;

class CalcBase implements CalcInterface
{

    public $config = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function generalIndividual(Competition $competition)
    {
        
    }

    public function generalTeam(Competition $competition)
    {
        
    }

    public function calcTeam(Stage $stage)
    {
        
    }

    public function calcRacePoints(Race $race)
    {
        $race_result = StageResult::with('race', 'player')
            ->active()
            ->where('race_id', $race->id)
            ->orderBy('is_finish', 'desc')->orderBy('pos', 'asc')->orderBy('info', 'asc')->orderBy('bib', 'asc')
            ->get();

        $pos = 1;
        $race_result->each(function($item) use(&$pos){
            if (isset($this->config['pts'][$item->pos])){
                $item->point = $this->config['pts'][$item->pos];
            }else{
                $item->point = null;
            }
            $item->save();
        });
        return $race_result;
    }

}
