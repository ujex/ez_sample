<?php

return [
    'version' => '1.0',
    
    'u23' => [1998,1999,2000,2001],
    
    'hidden_series' => [3,4],
    
    'print' => [
        'row_height' => 5,
        'row_height_big' => 7,
        'doc_font' => 'Calibri',
   

    ],
    
    
    'pagination' => [
        'global' => 50,
        'team' => 100
    ],
    
    // Cache times (minutes)
    'cache_time' => [
        'country_list' => (60*24),
        'player_info' => (60*8)
    ],
    
    'validator' => [
        // for mp3 set mimes:mpga (https://github.com/symfony/symfony/issues/8678#issuecomment-22171646)
        'file_upload_in_manager' => 'pdf,doc,docx,odt,xls,xlsx,jpg,jpeg,png,gif,mpga,wmv,wav',
        'file_upload_in_calendar' => 'pdf,doc,docx,odt,xls,xlsx,jpg,jpeg,png,gif',
    ],

];
