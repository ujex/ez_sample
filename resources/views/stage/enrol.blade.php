@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title col-md-6">Lista zgłoszeń</h3>
          <div class="col-md-6"><a href="{{ route('race.stage.edit', ['id' => $stage_info->id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i> Powrót do edycji zawodów</a></div>
        </div>
        <div class="box-body table-responsive">
          {{ Form::open(['route' => ['race.stage.enrol', 'id' => $stage->id]]) }}
          <table class="table table-bordered table-condensed table-hover" id="sorted_table">
            <thead>
              <tr>
                <th>Lp</th>
                <th>Nazwisko Imię</th>
                <th>Data ur.</th>
                <th>Klub</th>
                <th>Płeć</th>
                <th>Numer</th>
                <th>Data zapisu</th>
                <th>Kategoria</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($stage_result as $enrol)
              @php
              $cls = '';
              if($enrol->confirmed == 0){
              $cls = 'danger';
              }
              if($enrol->confirmed == 1){
              $cls = 'info';
              }
              if($enrol->confirmed == 2){
              $cls = 'success';
              }
              @endphp
              <tr class="{{ $cls }}">
                <td class="ez-iter">{{ $loop->iteration }}</td>
                <td><a href="{{ route('player.edit', ['id' => $enrol->player->id, 'stage_id' => $stage->id]) }}">{{ $enrol->player->surname }} {{ $enrol->player->name }}</a>
<!--                  <br><small><a href="mailto:{{ $enrol->player->email }}">{{ $enrol->player->email }}</a></small> <small>{{ $enrol->player->phone }}</small>-->
                </td>
                <td>{{ $enrol->player->birth_date }}</td>
                <td>{{ $enrol->team->name ?? '' }}</td>
                <td>{{ $enrol->player->polSex() }}</td>
                <td><b>{{ $enrol->bib }}</b></td>
                <td>{{ $enrol->created_at }}</td>
                <td>
                  {{ $enrol->race->name ?? '' }}
                  <select name="stage_race[{{ $enrol->player->id }}]" class="form-control input-sm">
                    <option value="0">-- Brak --</option>
                    @foreach($stage_info->races as $race)
                    @php
                    $selected = '';
                    if($race->id == $enrol->race_id){
                    $selected = 'selected="selected"';
                    }
                    @endphp
                    <option value="{{ $race->id }}" {{ $selected }}>{{ $race->name }}</option>
                    @endforeach
                  </select>

                </td>
                <td>
                  {{ $status[$enrol->confirmed]['name'] }}
                  <select name="stage_status[{{ $enrol->player->id }}]" class="form-control  input-sm">
                    @foreach($status as $s)
                    @php
                    $selected = '';
                    if($enrol->confirmed == $s['status']){
                    $selected = 'selected="selected"';
                    }
                    @endphp
                    <option value="{{ $s['status'] }}" {{ $selected }}>{{ $s['name'] }}</option>
                    @endforeach
                  </select>

                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Linki</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">

            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Zarządzaj:</h4></li>
              <li><a href="{{ route('s.sign', ['id' => $stage->id]) }}" class="from-checkbox" target="_blank">Dodaj zawodnika</a></li>
            </ul>
            <br style="clear:both;">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>

$(function () {
    $('#sorted_table').DataTable({
        paging: false,
        lengthChange: false,
        stateSave: true,
        searching: true,
//        columns: [
//            null,
//            null,
//            null,
//            null,
//            null,
//            null,
//            null,
//            null,
//            {orderable: false},
//        ],
//        scrollY: 700
    });
});
</script>
@endsection