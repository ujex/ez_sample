@extends('layouts.main')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Zawody</h3><br style="clear: both;" />
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['race.stage'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', null, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>LP</th>
              <th>Nazwa</th>
              <th>Miejsce</th>
              <th>Data</th>
              <th>Seria</th>
              <th>Organizator</th>


            </tr>
            </thead>
            <tbody>
        @foreach ($data as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row->no }}</td>
              <td><a href="{{ route('race.stage.edit', ['id' => $row->id]) }}">{{ $row->name }}</a></td>
              <td>{{ $row->place }}</td>
              <td>{{ $row->date }}</td>
              <td>{{ $row->competition->name }}</td>
              <td>{{ $row->competition->org->name }}</td>


            </tr>
         @endforeach


            </tbody>
          </table>
            
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
