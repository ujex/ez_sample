@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Edycja zawodów</h3>
        </div>
        {{ Form::model($stage, [
            'route' => ['race.stage.edit', 'id' => $stage->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) }}
        <div class="box-body">
          @include('inc.errors')
          <div class="form-group">
            {{ Form::label('no', 'No', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('no', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('name', 'Nazwa', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('place', 'Miejsce', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('place', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date', 'Data', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_sign', 'Ostatni dzień zapisów', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_sign', null, ['class' => 'form-control', 'placeholder' => 'RRRR-MM-DD']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('tags', 'Tagi', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('tags', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('img_top', 'Grafika nagłówek', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">{{ Form::file('img_top', null, ['class' => 'form-control']) }}</div>
            <div class="col-sm-6">
              @if($stage->img_top)
              <img src="/img/pdf_baner/{{ $stage->img_top }}" style="width:210px;">
              <a href="{{ route('race.stage.edit', ['id' => $stage->id, 'pos' => 'img_top', 'unlink_img' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              @endif
            </div>
          </div>
          <div class="form-group">
            {{ Form::label('img_bottom', 'Grafika stopka', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">{{ Form::file('img_bottom', null, ['class' => 'form-control']) }}</div>
            <div class="col-sm-6">
              @if($stage->img_bottom)
              <img src="/img/pdf_baner/{{ $stage->img_bottom }}" style="width:210px;">
              <a href="{{ route('race.stage.edit', ['id' => $stage->id, 'pos' => 'img_bottom', 'unlink_img' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              @endif
            </div>
          </div>

          <div class="form-group">
            {{ Form::label('image_stage', 'Grafika wyścigu', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-4">{{ Form::file('image_stage', null, ['class' => 'form-control']) }}</div>
            <div class="col-sm-6">
              @if($stage->image)
              <img src="/img/thumbnail/750/{{ $stage->image }}" style="width:210px;">
              <a href="{{ route('race.stage.edit', ['id' => $stage->id, 'pos' => 'image', 'unlink_img' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              @endif
            </div>
          </div>
          <div class="box-footer">



            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>

    <div class="col-md-6">
      <div class="box-default">
        <a href="{{ route('race.race', ['id' => $stage->id]) }}" class="btn btn-s btn-success pull-left" title="Przejdź do listy wyścigów"><i class="fa fa-cubes"></i> Przejdź do listy wyścigów</a>
        <button data-toggle="modal" data-target="#modalAddRace" class="btn btn-s btn-info pull-right" title="Dodaj wyścig"><i class="fa fa-plus-circle"></i> Dodaj wyścig</button>
      </div><br style="clear:both;">
      @if ($stage_info->races->count())
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Wyścigi</h3>
        </div>
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tr>
              <th style="width: 10px">#</th>
              <th>Nazwa</th>
              <th>Kategoria</th>
              <th>Długość</th>
              <th>Godzina startu</th>
            </tr>
            @foreach ($stage_info->races as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td><a href="{{ route('race.race.edit', ['id' => $row->id]) }}"><strong>{{ $row->name }}</strong></a></td>
              <td>{{ $row->category->name }}</td>
              <td>{{ $row->length }} km</td>
              <td>
                @if ($row->start_time == '00:00:00')
                <span class="label label-danger">
                  @else
                  <span class="label label-success">
                    @endif
                    {{ $row->start_time }}
                  </span>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
      @endif



      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Pliki</h3>
        </div>
        <div class="box-body box-profile">
          @if ($stage->files->count())
          <ul class="list-unstyled">
            @foreach ($stage->files as $file)
            <li>
              <a href="{{ route('race.stage.edit', ['id' => $stage->id, 'file_id' => $file->id, 'unlink' => true]) }}" class="text-red confirm" data-toggle="tooltip" title="Usuń plik"><i class="glyphicon glyphicon-remove-circle"></i></a> 
              <a href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}" target="_blank">{{ $file->description }}</a>
            </li>
            @endforeach
          </ul>
          @endif
        </div>
        <div class="box-footer">
          {{ Form::model($stage, [
                'route' => ['race.stage.edit', 'id' => $stage->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) }}
          <p>Dodaj plik:</p>
          <p>{{ Form::text('desc', null, ['class' => 'form-control', 'placeholder' => 'opis...']) }}</p>

          <div class="form-group">
            {{ Form::label('category', 'Kategoria', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('category', [
                \App\Models\File::CATEGORY_OTHER => 'Inne',
                \App\Models\File::CATEGORY_RESULT => 'Wyniki',
                \App\Models\File::CATEGORY_REGULATION => 'Regulaminy',
              ], 0, ['class' => 'form-control']) }}</div>
          </div>

          <p>{{ Form::file('reg_file', null, ['class' => 'form-control', 'placeholder' => 'wybierz plik']) }}</p>
          {{ Form::hidden('is_reg_form', 1) }}
          <p>{{ Form::submit('Prześlij', ['class' => 'btn btn-success pull-right']) }}</p>
          {{ Form::close() }}

        </div>
      </div>





    </div>

  </div>





  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Linki</a></li>
          @if(Auth::user()->role == 'admin')
          <li><a href="#tab_2" data-toggle="tab">Import zawodników</a></li>
          <li><a href="#tab_3" data-toggle="tab">Import rankigu</a></li>
          @endif
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Publiczne:</h4></li>
              <li><a href="{{ route('s.stage', ['id' => $stage->id]) }}" class="from-checkbox" target="_blank">Strona zawodów</a></li>
              @if ($stage->date_sign)
              <li><a href="{{ route('s.sign', ['id' => $stage->id]) }}" class="from-checkbox" target="_blank">Strona zapisów</a></li>
              @endif
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Inne:</h4></li>
              <li><a href="{{ route('race.stage.enrol', ['id' => $stage->id]) }}" class="from-checkbox">Lista zgłoszeń</a></li>
              <li><a href="{{ route('race.race', ['id' => $stage->id]) }}" class="from-checkbox">Lista wyścigów</a></li>
              <li><a href="{{ route('player.double-list', ['id' => $stage->competition_id]) }}" class="from-checkbox">Wyszukaj zdublowanych zawodników</a></li>

            </ul>
            <br style="clear:both;">
          </div>
          @if(Auth::user()->role == 'admin')
          <div class="tab-pane" id="tab_2">
            {{ Form::open(['route' => ['race.stage.import-player', 'id' => $stage], 'files' => true]) }}

            <div class="form-group" >
              numer;uciid;imie;nazwisko;data_ur;plec;klub;email;telefon;kategoria;kraj;data_zgl
              {{ Form::label('csv_file', 'Plik CSV', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::file('csv_file', null, ['class' => 'form-control']) }}</div>
            </div>
            
            {{ Form::submit('Import', ['class' => 'btn btn-info pull-right']) }}
            {{ Form::close() }}
            <br style="clear: both;">
          </div>
          <div class="tab-pane" id="tab_3">
            {{ Form::open(['route' => ['race.stage.import-ranking', 'id' => $stage], 'files' => true]) }}

            <div class="form-group" >
              uci_id;kategoria;punkty uci;punkty pzkol<br>
              {{ Form::label('csv_file', 'Plik CSV', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::file('csv_file', null, ['class' => 'form-control']) }}</div>
            {{ Form::submit('Import', ['class' => 'btn btn-info pull-right']) }}
            {{ Form::close() }}
            <br style="clear: both;">
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>


</section>


<div class="modal" id="modalAddRace">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ Form::model($race, [
        'route' => ['race.race.add'],
        'class' => 'form-horizontal'
    ]) }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dodaj wyścig</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          {{ Form::label('name', 'Nazwa', ['class' => 'col-sm-2 control-label']) }}
          <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group">
          {{ Form::label('category_id', 'Kategoria', ['class' => 'col-sm-2 control-label']) }}
          <div class="col-sm-10">{{ Form::select('category_id', $category, null, ['class' => 'form-control']) }}</div>
        </div>
        {{ Form::hidden('stage_id', $stage->id) }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Zamknij</button>
        <input type="submit" name="submit" class="btn btn-primary" value="Dodaj wyśig" />
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>


@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>

$(".ez-sel").click(function () {
    var $this = $(this);
    $this.select();
});
$('#communique').wysihtml5();
</script>
@endsection