@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">New Competition</h3>
        </div>
        {{ Form::model($competition, [
            'route' => ['race.competition.add', 'id' => $competition->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('inc.errors')
          <div class="form-group">
            {{ Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('place', 'Place', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('place', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_start', 'Date start', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_start', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('date_end', 'Date end', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('date_end', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('user_id', 'Organiser', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('user_id', $organisers, null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('calc_model', 'Calc model', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('calc_model', $calc_type, null, ['class' => 'form-control']) }}</div>
          </div>            
          <div class="form-group">
            {{ Form::label('org_email', 'Email', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('org_email', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('org_phone', 'Telefon', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('org_phone', null, ['class' => 'form-control']) }}</div>
          </div>



          <div class="box-footer">
            {{ Form::submit('Save', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>

    <div class="col-md-8">
      @if (isset($competition_info->stages))
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Stages</h3>
        </div>
        <div class="box-body no-padding">
          <table class="table table-striped table-condensed">
            <tr>
              <th style="width: 10px">#</th>
              <th>No, Name</th>
              <th>Place</th>
              <th>Date</th>
              <th style="width: 70px">&nbsp;</th>
            </tr>
            @foreach ($competition_info->stages as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td><a href="{{ route('race.stage.edit', ['id' => $row->id]) }}"><strong>{{ $row->no }}, {{ $row->name }}</strong></a></td>
              <td>{{ $row->place }}</td>
              <td>{{ $row->date }}</td>
              <td>
                <a href="{{ route('race.stage.copy', ['id' => $row->id]) }}"><i class="fa fa-files-o"></i></a> 
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
      @endif







    </div>

  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script>
$(function () {
    $('#player_search').autocomplete({
        source: "/api/player/search/ALL",
        minLength: 3,
        select: function (a, b) {
            var res = b.item;
            $('#player_id').val(res.id);
            console.log(res);
        }
    });

    $('#commissarie').autocomplete({
        source: "/api/player/search/C",
        minLength: 3,
        select: function (a, b) {
            var res = b.item;
            $('#com_id_new').val(res.id);
            console.log(res);
        }
    });
    $('#director').autocomplete({
        source: "/api/player/search/P|M|O|C",
        minLength: 3,
        select: function (a, b) {
            var res = b.item;
            $('#dir_id_new').val(res.id);
            console.log(res);
        }
    });

    var availableTags = ["road_kwk", "road_mwg", "road_pt", "mtb_pss", "mtb_pp"];
    $('#calc_model').autocomplete({
        source: availableTags
    });

});
</script>
@endsection