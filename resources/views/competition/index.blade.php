@extends('layouts.main')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Serie wyścigów</h3><br style="clear: both;" />
          <a href="{{ route('race.competition.add') }}" class="btn btn-success pull-left">Utwórz nową serie</a>
          <div class="box-tools pull-right">
            {{ Form::open(['route' => ['race.competition'], 'method' => 'get']) }}
            <div class="has-feedback">
              {{ Form::text('q', null, ['class' => 'form-control input-sm', 'placeholder' => 'Szukaj...']) }}
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Name</th>
              <th>Place</th>
              <th>Dates</th>
              <th>Organiser</th>


            </tr>
            </thead>
            <tbody>
        @foreach ($data as $row)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $row->id }}</td>
              <td><a href="{{ route('race.competition.add', ['id' => $row->id]) }}">{{ $row->name }}</a></td>
              <td>{{ $row->place }}</td>
              <td>{{ $row->date_start }} - {{ $row->date_end }}</td>
              <td>{{ $row->org->name ?? '' }}</td>


            </tr>
         @endforeach


            </tbody>
          </table>
            
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
