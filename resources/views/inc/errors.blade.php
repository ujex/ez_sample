@if (isset($errors) && count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}
        @endforeach
    </div>
@endif

@if(Session::has('info'))
    <div class="alert alert-info">
        {{ Session::get('info') }}
    </div>
@endif