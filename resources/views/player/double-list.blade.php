@extends('layouts.main')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Zdublowani zawodnicy</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover table-striped table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Nazwisko, Imię</th>
                <th>Data ur.</th>
                <th>UCI ID</th>
                <th>Ilość dubli</th>
              </tr>
            </thead>
            <tbody>
              @if (count($player))
              @foreach ($player as $row)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                  <a href="{{ route('player.double-merge', ['id' => $row['player']->id, 'competition_id' => $competition_id]) }}">
                    <strong>{{ $row['player']->surname }}, {{ $row['player']->name }}</strong>
                  </a>
                </td>
                <td>{{ $row['player']->birth_date }}</td>
                <td>{{ $row['player']->uci_id }}</td>
                <td>{{ $row['num'] }}</td>
              </tr>
              @endforeach
              @else
              <p>Brak zdublowanych zawodników</p>
              @endif


            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
