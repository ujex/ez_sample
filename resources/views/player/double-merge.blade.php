@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box">
        <div class="box-header">
          <h2 class="box-title" style="color:#1167b1; font-weight: bold;">{{ $player->surname }}, {{ $player->name }}  
            (Wiek: {{ (new \Carbon\Carbon($player->birth_date))->age }})
          </h2>
        </div>
        {{ Form::model($player, [
            'route' => ['player.edit', 'id' => $player->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('inc.errors')
          <div class="form-group">
            {{ Form::label('surname', trans('html.player.add.form.surname'), ['class' => 'col-sm-2 control-label ']) }}
            <div class="col-sm-10">{{ Form::text('surname', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('name', trans('html.player.add.form.name'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('birth_date', trans('html.player.add.form.birth'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('birth_date', null, ['class' => 'form-control', 'autocomplete' => 'off', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('uci_id', trans('html.player.add.form.uci_id'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('uci_id', null, ['class' => 'form-control', 'autocomplete' => 'off', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('team', trans('html.player.add.form.team'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              {{ Form::text('team', ($player->team->name ?? ''), ['class' => 'form-control', 'id' => 'team_search', 'disabled'=>'disabled']) }}
            </div>
          </div>


          <div class="form-group">
            {{ Form::label('sex', trans('html.player.add.form.sex'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              <label>{{ Form::radio('sex', 'F', false, ['class' => 'minimal', 'disabled'=>'disabled']) }} Kobieta</label>
              <label>{{ Form::radio('sex', 'M', true, ['class' => 'minimal', 'disabled'=>'disabled']) }} Mężczyzna</label>
            </div>
          </div>

          <div class="form-group">
            {{ Form::label('region', 'Region', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('region', [
                null => 'brak',
                'MAL' => 'małopolskie',
                'SLA' => 'śląskie',
                'SWI' => 'świętokorzyskie',
                'DLS' => 'dolnośląskie',
                'KPO' => 'kujawsko pomorskie',
                'LOD' => 'łódzkie',
                'LUB' => 'lubuskie',
                'LUL' => 'lubelskie',
                'MAZ' => 'mazowieckie',
                'OPO' => 'opolskie',
                'PDL' => 'podlaskie',
                'PKA' => 'podkarpackie',
                'MAZ' => 'mazowieckie',
                'POM' => 'pomorskie',
                'MAZ' => 'mazowieckie',
                'WLK' => 'wielkopolskie',
                'WMA' => 'warmińsko - mazurskie',
                'ZPO' => 'zachodnio pomorskie',
                'POL' => 'Polska',
              ], null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('email', trans('html.player.add.form.email'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::email('email', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('phone', trans('html.player.add.form.phone'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('phone', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('city', 'City', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('city', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('address', 'Address', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::textArea('address', null, ['class' => 'form-control', 'disabled'=>'disabled']) }}</div>
          </div>
          {{ Form::close() }}
        </div>
      </div>

    </div>


    <div class="col-md-8">
      <div class="box">
        <div class="box-header">
          <h2 class="box-title" style="color:#1167b1; font-weight: bold;">Zdublowani zawodnicy:</h2>
        </div>

        <div class="box-body">
          @if (count($player_double))
          <div class="box-body table-responsive">
            <div class="alert alert-info">
              UWAGA! Po wykonaniu połączenia zawodników należy przeliczyć klasyfikację generalną.
            </div>
            <table class="table table-bordered table-hover table-striped table-condensed">
              <thead>
                <tr>
                  <th>Nazwisko, Imię</th>
                  <th>Data ur.</th>
                  <th>UCI ID</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($player_double as $row)
                <tr>
                  <td>
                    <strong>{{ $row->surname }}, {{ $row->name }}</strong>
                  </td>
                  <td>{{ $row->birth_date }}</td>
                  <td>{{ $row->uci_id }}</td>
                  <td><a href="{{ route('player.double-merge', ['id' => $player->id, 'competition_id' => $competition_id, 'merge' => $row->id]) }}" class="btn btn-sm btn-success pull-left" title="Połącz"><i class="fa fa-arrow-circle-left"></i> Połącz</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          <p>Brak dubli dla danego zawodnika</p>
          @endif
        </div>
      </div>

    </div>

  </div>

</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {

    $('#birth_date').focusout(function () {
        var input_date = $('#birth_date').val();
        if (input_date.length < 5) {
            $('#birth_date').val(input_date + "-01-01");
        }
    });

});
</script>
@endsection