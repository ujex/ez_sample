@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/select2/select2.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="box">
        <div class="box-header">
          <h2 class="box-title" style="color:#1167b1; font-weight: bold;">{{ $player->surname }}, {{ $player->name }}  
            (Wiek: {{ (new \Carbon\Carbon($player->birth_date))->age }})
          </h2>
        </div>
        {{ Form::model($player, [
            'route' => ['player.edit', 'id' => $player->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
          @include('inc.errors')
          <div class="form-group">
            {{ Form::label('surname', trans('html.player.add.form.surname'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('surname', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('name', trans('html.player.add.form.name'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('birth_date', trans('html.player.add.form.birth'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('birth_date', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('uci_id', trans('html.player.add.form.uci_id'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('uci_id', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('team', trans('html.player.add.form.team'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              {{ Form::text('team', ($player_team ?? ''), ['class' => 'form-control', 'id' => 'team_search']) }}
              {{ Form::hidden('team_id_old', ($player_info->team->id ?? 0), ['id' => 'team_id_old']) }}
              {{ Form::hidden('team_id_new', null, ['id' => 'team_id_new']) }}
            </div>
          </div>


          <div class="form-group">
            {{ Form::label('sex', trans('html.player.add.form.sex'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">
              <label>{{ Form::radio('sex', 'F', false, ['class' => 'minimal']) }} Kobieta</label>
              <label>{{ Form::radio('sex', 'M', true, ['class' => 'minimal']) }} Mężczyzna</label>
            </div>
          </div>
          <div class="form-group">
            {{ Form::label('country_id', trans('html.player.add.form.country'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('country_id', $country, null, ['class' => 'form-control select2']) }}</div>
          </div>
          @if($bib)
          <div class="form-group">
            {{ Form::label('bib', 'Numer startowy', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('bib', $bib, ['class' => 'form-control']) }}</div>
          </div>
          @endif

          <div class="form-group">
            {{ Form::label('region', 'Region', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::select('region', [
                null => 'brak',
                'MAL' => 'małopolskie',
                'SLA' => 'śląskie',
                'SWI' => 'świętokorzyskie',
                'DLS' => 'dolnośląskie',
                'KPO' => 'kujawsko pomorskie',
                'LOD' => 'łódzkie',
                'LUB' => 'lubuskie',
                'LUL' => 'lubelskie',
                'MAZ' => 'mazowieckie',
                'OPO' => 'opolskie',
                'PDL' => 'podlaskie',
                'PKA' => 'podkarpackie',
                'MAZ' => 'mazowieckie',
                'POM' => 'pomorskie',
                'MAZ' => 'mazowieckie',
                'WLK' => 'wielkopolskie',
                'WMA' => 'warmińsko - mazurskie',
                'ZPO' => 'zachodnio pomorskie',
                'POL' => 'Polska',
              ], null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('license', 'License', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('license', null, ['class' => 'form-control']) }}</div>
          </div>

          <div class="form-group">
            {{ Form::label('email', trans('html.player.add.form.email'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::email('email', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('phone', trans('html.player.add.form.phone'), ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('phone', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('city', 'City', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::text('city', null, ['class' => 'form-control']) }}</div>
          </div>
          <div class="form-group">
            {{ Form::label('address', 'Address', ['class' => 'col-sm-2 control-label']) }}
            <div class="col-sm-10">{{ Form::textArea('address', null, ['class' => 'form-control']) }}</div>
          </div>
          {{ Form::hidden('stage_id', $stage_id, ) }}

          <div class="box-footer">
            {{ Form::submit(trans('html.player.add.form.save'), ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>

    @if($double_check)
    <div class="col-md-4">
      <div class="box">
        <div class="box-body">
          <div class="alert alert-danger">
            UWAGA! Ten zawodnik jest zdublowany..
          </div>
          <a href="{{ route('player.double-merge', ['id' => $player->id, 'competition_id' => $player->competition_id]) }}" class="btn btn-sm btn-success pull-right" title="Połącz"><i class="fa fa-arrow-circle-right"></i> Przejdź aby usunać duble</a>
        </div>
      </div>
    </div>  
    @endif
    @if($stage)
    <div class="col-md-4">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">{{ $stage->name }}</h3>
        </div>
        <div class="box-body">
          <a href="{{ route('race.stage.enrol', ['id' => $stage->id]) }}" title="Lista startowa"><i class="fa fa-lg fa-list"></i> Lista Zgłoszeń</a><br /><br />
          <a href="{{ route('race.race', ['id' => $stage->id]) }}" title="Przeglądaj wyniki"><i class="fa fa-lg fa-cubes"></i> Lista wyścigów</a><br /><br />
          <a href="{{ route('s.sign', ['id' => $stage->id]) }}" target="_blank"><i class="fa fa-lg fa-plus-square"></i> Dodaj zawodnika</a>
        </div>
      </div>
    </div>  
    @endif


  </div>

</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function () {

    $('#birth_date').focusout(function () {
        var input_date = $('#birth_date').val();
        if (input_date.length < 5) {
            $('#birth_date').val(input_date + "-01-01");
        }
    });

});
</script>
@endsection