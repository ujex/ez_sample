@extends('layouts.site')


@section('content')

<div class="cy_event_box">
  <div class="cy_event_data">
    <h2>{{ $page->title }}</h2>
  </div>

  <div class="row" >
    <div class="col-lg-12 col-md-12">
      <div class="comment-respond">
        {!! $page->content !!}
      </div>
    </div>
  </div>
</div>


@endsection
