@extends('layouts.site')


@section('content')
<div class="cy_event_box">
    <div class="cy_event_data">
        <h2><a href="{{ route('s.stage', ['id' => $stage->id]) }}">Zapisy do {{ $stage->name }}</a></h2>
    </div>
  
      <div class="cy_event_details">
        <div class="row">
          <div class="col-lg-6 col-md-6">
              <div class="cy_event_det_box">
                  <h1>Szczegóły</h1>
                  @if($stage->competition->org_phone || $stage->competition->org_email)
                  <p style="text-transform:none;"><span>Kontakt do organizatora:</span> 
                    @if($stage->competition->org_phone)
                    <br>tel: {{ $stage->competition->org_phone }} 
                    @endif
                    @if($stage->competition->org_email)
                    <br>e-mail: {{ $stage->competition->org_email }}
                    @endif
                  </p>
                  @endif
                  <p><span>Miejscowość:</span> {{ $stage->place }}</p>
                  <p><span>Data:</span> {{ ez_date_long( $stage->date ) }}</p>
                  @if($stage->date_sign)
                  <p><span>Termin zgłoszeń:</span> {{ ez_date_long( $stage->date_sign ) }}</p>
                  @endif
              </div>
          </div>
          @if ($stage->files->count())
          <div class="col-lg-6 col-md-6">
              <div class="cy_event_det_box">
                  <h1>Pliki do pobrania</h1>

            @foreach ($stage->files as $file)
             <p><a href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}" target="_blank">{{ $file->description }}</a></p>
            @endforeach
              </div>
          </div>
          @endif
        </div>

      </div>

  <div class="row" >
          <div class="col-lg-12 col-md-12">
              <div class="comment-respond">
                  <h3 id="reply-title" class="comment-reply-title">Dodaj zawodnika</h3>
                    {{ Form::model($player, [
                          'route' => ['s.sign', 'id' => $stage->id],
                          'class' => '',

                      ]) }}
                    @csrf
                    
            @include('inc.errors')
            <div class="row" style="padding:10px;">
              {{ Form::label('uci_id', __('site.reg.uciid').'', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('uci_id', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
            </div>

            <div class="row" style="padding:10px;">
              {{ Form::label('name', __('site.reg.name').'*', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
            </div>

            <div class="row" style="padding:10px;">
              {{ Form::label('surname', __('site.reg.surname').'*', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('surname', null, ['class' => 'form-control']) }}</div>
            </div>

            <div class="row" style="padding:10px;">
              {{ Form::label('birth_date', __('site.reg.birth').'*', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-4">{{ Form::text('birth_date', null, ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
              <div class="col-sm-3"><br>({{ __('site.reg.birth_desc') }})</div>
            </div>
            <div class="row" style="padding:10px;">
              {{ Form::label('sex', __('site.reg.sex'), ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-5">
                <label>{{ Form::radio('sex', 'F', false, ['class' => 'minimal']) }} {{ __('site.reg.female') }}</label>
                <label>{{ Form::radio('sex', 'M', true, ['class' => 'minimal']) }} {{ __('site.reg.male') }}</label>
              </div>
            </div>
          <div class="row" style="padding:10px;">
            {{ Form::label('country_id', 'Narodowość', ['class' => 'col-sm-3 control-label']) }}
            <div class="col-sm-7">{{ Form::select('country_id', $country, null, ['class' => 'form-control']) }}</div>
          </div>
            <div class="row" style="padding:10px;">
              {{ Form::label('ctg', 'Kategoria', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::select('ctg', $races, null, ['class' => 'form-control ignore']) }}</div>
            </div>
            <div class="row" style="padding:10px;">
              {{ Form::label('team', __('site.reg.team'), ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('team', null, ['class' => 'form-control']) }}</div>
            </div>

            <div class="row" style="padding:10px;">
              {{ Form::label('email', __('site.reg.email').'*', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::email('email', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="row" style="padding:10px;">
              {{ Form::label('phone', __('site.reg.phone').'', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('phone', null, ['class' => 'form-control']) }}</div>
            </div>
            @if($allow_admin)
            <div class="row" style="padding:10px;">
              {{ Form::label('bib', __('site.reg.bib').'', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-7">{{ Form::text('bib', null, ['class' => 'form-control']) }}</div>
            </div>
            @endif
            
                           <div class="col-lg-12 col-md-12" style="padding:10px;">
                              <div class="comment-form-submit">

                                  {{ Form::submit(__('site.reg.save'), ['class' => 'submit cy_button']) }}
                              </div>
                          </div>
                  {{ Form::close() }}
              </div>

          </div>
      </div>
  </div>
  
  
  


@endsection








@section('page-script')
<script>
@if($racesjs)
    races = new Array();
    @foreach($racesjs as $r)
        races.push('{{ $r['name'] }}');
    @endforeach
@endif
    $(function () {
        $("#uci_id").on("keyup", function (event) {
            if (event.which == 17) {
                return;
            }

            var q = $(this).val();
            if (/^[0-9]{11}$/.test(q) || /^[0-9 ]{14}$/.test(q)) {

                q = "" + q;
                var id = q.replaceAll(' ', '');
                $.ajax({
                    url: "/api/searcg/uci_id/" + id,
                    dataType: 'json',
                    success: function (r) {
                        if (r.status == 'ok') {
                            $('#name').val(r.data.name);
                            $('#surname').val(r.data.surname);
                            $('#team').val(r.data.team);
                            $('#birth_date').val(r.data.year + "-01-01");
                            if (r.data.sex == 'M') {
                                $('input:radio[name=sex][value=M]').click();
                            } else {
                                $('input:radio[name=sex][value=F]').click();
                            }
                            var index;
                            for (index = 0; index < races.length; ++index) {
                                var tmp = races[index].split('_');
                                var rang = tmp[2].split('|');
                                for (i = 0; i < rang.length; ++i) {
                                    if(rang[i] == r.data.age && tmp[1] == r.data.sex){
                                        $('#ctg').val(tmp[0]);
                                        console.log(tmp[0]);
                                    }
                                }
                            }
                            
                            
                        } else {
                            $('#name').val('');
                            $('#surname').val('');
                            $('#team').val('');
                            $('#birth_date').val('');
                        }
                    }
                });


            }
        });
    });
</script>
@endsection