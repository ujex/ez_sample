@extends('layouts.site')


@section('content')
<div class="cy_event_box">
    <div class="cy_event_img">
        <img src="/img/thumbnail/750/{{ $stage->image }}" alt="event single" class="img-fluid" />
        <div class="cy_event_detail">
            <div class="cy_event_time">
                <ul>
                    <li><i><img src="/images/svg/clock.svg" alt="event time"></i> {{ $stage->date }}</li>
                    <li><i><img src="/images/svg/map.svg" alt="event address"></i>{{ $stage->place }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cy_event_data">
        <h2>{{ $stage->name }}</h2>
    </div>
  
    <div class="cy_event_details">
        <div class="row">
          <div class="col-lg-6 col-md-6">
              <div class="cy_event_det_box">
                  <h1>Szczegóły</h1>
                  @if($stage->competition->org_phone || $stage->competition->org_email)
                  <p style="text-transform:none;"><span>Kontakt do organizatora:</span> 
                    @if($stage->competition->org_phone)
                    <br>tel: {{ $stage->competition->org_phone }} 
                    @endif
                    @if($stage->competition->org_email)
                    <br>e-mail: {{ $stage->competition->org_email }}
                    @endif
                  </p>
                  @endif
                  <p><span>Miejscowość:</span> {{ $stage->place }}</p>
                  <p><span>Data:</span> {{ ez_date_long( $stage->date ) }}</p>
                  @if($stage->date_sign)
                  <p><span>Termin zgłoszeń:</span> {{ ez_date_long( $stage->date_sign ) }}</p>
                  @endif
				  @if($stage->link)
                  <p><span>Link do zgłoszeń:</span> <a href='{{$stage->link}}' target='_blank'>{{$stage->link}}</a></p>
                  @endif
              </div>
          </div>
          @if ($stage->files->count())
          <div class="col-lg-6 col-md-6">
              <div class="cy_event_det_box">
                  <h1>Pliki do pobrania</h1>

            @foreach ($stage->files as $file)
             <p><a href="{{ route('download', ['id' => $file->id, 'name' => $file->name, 'out' => 's']) }}" target="_blank">{{ $file->description }}</a></p>
            @endforeach
              </div>
          </div>
          @endif
        </div>
      @if($stage->date_sign >= date('Y-m-d'))
                           <div class="col-lg-12 col-md-12" style="padding:10px;">
                              <div class="comment-form-submit">

                                <a href="{{ route('s.sign', ['id'=>$stage->id]) }}" class="submit cy_button">Zapisz się</a>
                              </div>
                          </div>
      @endif
      </div>

            @if(count($stage->results))
<div class="row">
  <h4>{{ __('site.reg.added') }}:</h4><br><br>
  <div class="col-lg-12">
    <table class="table table-bordered table-hover table-striped table-condensed" style="font-size:11px;">
            <thead>
              <tr>
                <th>Lp</th>
                <th>Nazwisko Imię</th>
                <th>Klub</th>
                <th>Kategoria</th>
              </tr>
            </thead>
            <tbody>
                          @foreach($stage->results as $row)
              <tr>
                <td style="padding:0.5rem">{{ $loop->iteration }}</td>
                <td style="padding:0.5rem">{{ $row->player->surname }} {{ $row->player->name }}</td>
                <td style="padding:0.5rem">{{ $row->team->name ?? '' }}</td>
                <td style="padding:0.5rem">{{ $row->race->name ?? '' }}</td>
              </tr>

                          @endforeach
            </tbody>
          </table>
  </div>
</div>
            @endif
  </div>
  
  


@endsection








@section('page-css')
<script>
    $(function () {
        $("#uci_id").on("keyup", function (event) {
            if (event.which == 17) {
                return;
            }

            var q = $(this).val();
            if (/^[0-9]{11}$/.test(q) || /^[0-9 ]{14}$/.test(q)) {

                q = "" + q;
                var id = q.replaceAll(' ', '');
                $.ajax({
                    url: "/api/searcg/uci_id/" + id,
                    dataType: 'json',
                    success: function (r) {
                        if (r.status == 'ok') {
                            $('#name').val(r.data.name);
                            $('#surname').val(r.data.surname);
                            $('#team').val(r.data.team);
                            $('#birth_date').val(r.data.year + "-01-01");
                            if (r.data.sex == 'M') {
                                console.log(r.data);
                                $('input:radio[name=sex][value=M]').click();
                            } else {
                                $('input:radio[name=sex][value=F]').click();
                            }
                        } else {
                            $('#name').val('');
                            $('#surname').val('');
                            $('#team').val('');
                            $('#birth_date').val('');
                        }
                    }
                });


            }
        });
    });
</script>
@endsection