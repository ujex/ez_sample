@extends('layouts.site')


@section('content')
    <div class="cy_blog_wrapper cy_section_padding  cy_blog_page">
        <div class="container">
            <div class="row">
              @if (count($stage))
              @foreach ($stage as $s)
                <div class="col-lg-6 col-md-6">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="/img/thumbnail/750/{{ $s->image }}" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="/images/svg/clock.svg" alt="event time"></i> {{ ez_date_long($s->date) }}</li>
                                        <li><i><img src="/images/svg/map.svg" alt="event address"></i>{{ $s->place }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2><a href="{{ route('s.stage', ['id' => $s->id]) }}">{{ $s->name }}</a></h2>
                        </div>
                    </div>
                </div>
              
              @endforeach
              @endif
            </div>
        </div>
    </div>





@endsection








@section('page-css')
<script>
    $(function () {
        $("#uci_id").on("keyup", function (event) {
            if (event.which == 17) {
                return;
            }

            var q = $(this).val();
            if (/^[0-9]{11}$/.test(q) || /^[0-9 ]{14}$/.test(q)) {

                q = "" + q;
                var id = q.replaceAll(' ', '');
                $.ajax({
                    url: "/api/searcg/uci_id/" + id,
                    dataType: 'json',
                    success: function (r) {
                        if (r.status == 'ok') {
                            $('#name').val(r.data.name);
                            $('#surname').val(r.data.surname);
                            $('#team').val(r.data.team);
                            $('#birth_date').val(r.data.year + "-01-01");
                            if (r.data.sex == 'M') {
                                console.log(r.data);
                                $('input:radio[name=sex][value=M]').click();
                            } else {
                                $('input:radio[name=sex][value=F]').click();
                            }
                        } else {
                            $('#name').val('');
                            $('#surname').val('');
                            $('#team').val('');
                            $('#birth_date').val('');
                        }
                    }
                });


            }
        });
    });
</script>
@endsection