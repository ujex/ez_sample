@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title col-md-6">Lista startowa {{ $race->stage->name }} - {{ $race->name }}</h3>
          <div class="col-md-6"><a href="{{ route('race.race', ['id' => $race->stage_id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i> Powrót do listy wyścigów</a></div>
        </div>
        <div class="box-body table-responsive">
          {{ Form::open(['route' => ['race.race.show-start', 'race' => $races]]) }}
          <table class="table table-bordered table-hover table-striped table-condensed" id="sorted_table">
            <thead>
              <tr>
                <th>#</th>
                <th style="width:30px;">*</th>
                <th style="width:70px;">Numer edycja</th>
                <th style="width:70px;">Numer</th>
                
                <th style="width:70px;">Klasyfikacja</th>
                <th>Kraj</th>
                <th>Nazwisko Imię</th>
                <th>Data ur.</th>
                <th>UCI ID</th>
                <th>Drużyna</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($stage_start as $row)
              <tr>
                <td>{{ $loop->iteration }}{{ Form::hidden($row->id.'[id]', $row->id) }}</td>
                <td>{{ Form::text($row->id.'[ext]', $row->ext, ['class' => 'form-control input-sm ext-symbol', 'data-year' => $row->player->birth_year()]) }}</td>
                <td>{{ Form::text($row->id.'[bib]', $row->bib, ['class' => 'form-control input-sm bib-numberer']) }}</td>
                <td >{{ $row->bib }}</td>
                <td >{{ round($row->point) }}</td>
                <td >{{ $row->player->country->code }}</td>
                <td><a href="{{ route('player.edit', ['id' => $row->player->id, 'stage_id' => $stage->id]) }}">
                    <strong>{{ $row->player->surname }} {{ $row->player->name }}</strong>
                  </a></td>
                <td >{{ $row->player->birth_date }}</td>
                <td >{{ $row->player->uciId() }}</td>
                <td>{{ $row->team->name ?? '' }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Linki</a></li>
          <li><a href="#tab_2" data-toggle="tab">Nadaj Numery</a></li>
          <li><a href="#tab_3" data-toggle="tab">Ext</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Lista startowa:</h4></li>
              <li><a href="{{ url('prints/start/race/pdf/bib', $races) }}" class="from-checkbox">Pobierz Pdf wg numerów</a></li>
              <li><a href="{{ url('prints/start/race/xls/bib', $races) }}" class="from-checkbox">Pobierz csv wg numerów</a></li>
              <li><a href="{{ url('prints/start/race/pdf/cls', $races) }}" class="from-checkbox">Pobierz Pdf wg klasyfikacji</a></li>
              <li><a href="{{ url('prints/start/race/xls/cls', $races) }}" class="from-checkbox">Pobierz csv wg klasyfikacji</a></li>
              <li><a href="{{ url('prints/start/race/pdf/rand', $races) }}" class="from-checkbox">Pobierz Pdf losowo</a></li>
              <li><a href="{{ url('prints/start/race/xls/rand', $races) }}" class="from-checkbox">Pobierz csv losowo</a></li>
              @if(Auth::user()->role == 'admin')
              <li><a href="{{ url('prints/start/race/pdf/rank', $races) }}" class="from-checkbox">Pobierz Pdf wg rankingów</a></li>
              <li><a href="{{ url('prints/start/race/xls/rank', $races) }}" class="from-checkbox">Pobierz csv wg rankingów</a></li>
              @endif
            </ul>

            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Zarządzaj:</h4></li>
              <li><a href="{{ route('race.stage.enrol', ['id' => $stage->id]) }}" class="from-checkbox">Lista zgłoszeń</a></li>
              <li><a href="{{ route('s.sign', ['id' => $stage->id]) }}" class="from-checkbox" target="_blank">Dodaj zawodnika</a></li>
            </ul>
            <br style="clear:both;">
          </div>
          <div class="tab-pane" id="tab_2">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Nadaj Numery</h3>
              </div>
              <div class="box-body form-horizontal">
                <div class="form-group">
                  <label for="first-bib" class="col-sm-2 control-label">Pierwszy numer:</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="first-bib" value="1">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-info pull-right" id="bib-do">Zastosuj</button>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab_3">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Ext</h3>
              </div>
              <div class="box-body form-horizontal">
                <div class="form-group">
                  <label for="ext-symbol" class="col-sm-2 control-label">Symbol</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="ext-symbol" value="*">
                  </div>
                  <div class="col-sm-5">
                    <strong>U-23: {{ date('Y') - 22 }}-{{ date('Y') - 19 }}</strong>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ext-range" class="col-sm-2 control-label">Zakres roczników</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="ext-range" value="{{ date('Y') - 22 }}-{{ date('Y') - 19 }}">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button class="btn btn-info pull-right" id="ext-do">Zastosuj</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>
$(function () {

    $('#sorted_table').DataTable({
        paging: false,
        lengthChange: false,
        stateSave: true,
        searching: true,
        columns: [
            null,
            {orderable: false},
            {orderable: false},
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ],
    });

    // Start Time
    $('#start-time-do').click(function () {
        var start_time = new Date("2016-01-01 " + $('#start-time-start').val()),
            start_hour = new Date("2016-01-01 " + $('#start-time-start-hour').val()),
            modifier = parseInt($('#start-time-diff').val());
        $('.time_start').each(function () {
            $(this).val(start_time.toLocaleTimeString());
            start_time.setSeconds(start_time.getSeconds() + modifier);
        });
        $('.time_start_hour').each(function () {
            $(this).val(start_hour.toLocaleTimeString());
            start_hour.setSeconds(start_hour.getSeconds() + modifier);
        });
    });

    // BIB
    $('#bib-do').click(function () {
        var first_bib = parseInt($('#first-bib').val()),
            modifier = 1;
        $('.bib-numberer').each(function () {
            $(this).val(first_bib);
            first_bib = first_bib + modifier;
        });
    });

    // Ext
    $('#ext-do').click(function () {
        var symbol = $('#ext-symbol').val(),
            raw_range = $('#ext-range').val();
        if (raw_range.indexOf('|') >= 0) {
            var range = raw_range.split('|');
        } else {
            var range = new Array();
            var tmp = raw_range.split('-');
            for (i = tmp[0]; i <= tmp[1]; i++) {
                range.push(i.toString());
            }
        }
        $('.ext-symbol').each(function () {
            if (range.indexOf($(this).data('year').toString()) != -1) {
                $(this).val(symbol);
            } else {
                $(this).val('');
            }
        });
    });
});
</script>
@endsection