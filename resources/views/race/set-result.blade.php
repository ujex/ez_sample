@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-3">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Wprowadź wyniki</h3>
        </div>
        {{ Form::model($stage_result, [
            'route' => ['race.race.set-result', 'stage' => $race->stage_id, 'race' => $races],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
            @include('inc.errors')
            <div class="form-group">
              {{ Form::label('pos', 'M-ce', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-9">{{ Form::number('pos', $next_record['pos'], ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
            </div>
            <div class="form-group form-group-lg">
              {{ Form::label('bib', 'Nr', ['class' => 'col-sm-3 control-label']) }}
              <div class="col-sm-9">{{ Form::number('bib', null, ['class' => 'form-control text-bold', 'autocomplete' => 'off']) }}</div>
            </div>
            <div class="form-group form-group-lg">
              {{ Form::label('time', 'Czas', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">
                <div class="col-sm-4 no-padding">{{ Form::number('time_h', $next_record['time_h'], ['class' => 'form-control text-bold', 'autocomplete' => 'off']) }}</div> 
                <div class="col-sm-4 no-padding">{{ Form::number('time_m', $next_record['time_m'], ['class' => 'form-control text-bold', 'autocomplete' => 'off']) }}</div> 
                <div class="col-sm-4 no-padding">{{ Form::number('time_s', $next_record['time_s'], ['class' => 'form-control text-bold', 'autocomplete' => 'off']) }}</div> 
              </div>
            </div>
<!--            <div class="form-group hidden">
              {{ Form::label('time_set', 'Set', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::number('time_set', $next_record['time_set'], ['class' => 'form-control', 'autocomplete' => 'off']) }}</div>
            </div>-->
            <div class="form-group">
              {{ Form::label('info', 'Info', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-5">{{ Form::text('info', $next_record['info'], ['class' => 'form-control']) }}</div>
<!--              <div class="col-sm-5">{{ Form::number('minus', $next_record['info'], ['class' => 'form-control', 'id' => 'minus', 'title' => 'Minus']) }}</div>-->
            </div>

          <div class="box-footer">
            {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
            {{ Form::submit('Reset', ['class' => 'btn btn-danger pull-left', 'name' => 'reset']) }}
          </div>
        </div>
        {{ Form::close() }}
        @if($results_all)
        <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Pozostało: {{ $standings['to_set'] }}/{{ $standings['all'] }}</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-condensed-max small">
                <tr>
                  <th>Nr</th>
                  <th>Nazwa</th>
                  <th>Drużyna</th>
                </tr>
                @foreach($results_all as $row)
                    @if(!$row->is_finish && !$row->info) 
                    <tr class="danger">
                      <td><strong>{{ $row->bib }}</strong></td>
                      <td><a href="{{ route('player.edit', ['id' => $row->player->id, 'stage_id' => $race->stage_id]) }}">
                          {{ $row->ext or '' }}{{ $row->player->surname }} {{ $row->player->name }}
                      </a></td>
                      <td>{{ ($row->team->name ?? '') }}</td>
                    </tr>
                    @endif
                @endforeach
              </table>
            </div>
        </div>
        @endif
      </div>
    </div>
    @if($results_all)
    <div class="col-md-9">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title col-md-6">Wyniki <a href="{{ route('race.race', ['id' => $race->stage_id]) }}">{{ $race->stage->name }}</a> - {{ $race->name }}</h3>
                <div class="col-md-6"><a href="{{ route('race.race', ['id' => $race->stage_id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i> Powrót do listy wyścigów</a></div>
            </div>
            <div class="box-body no-padding">
              <div class="col-md-4">
              <a href="{{ route('race.race.edit', ['id' => $races]) }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edytuj wyścig</a> | 
              <a href="{{ url('prints/result/race/pdf', $races) }}" class="btn btn-xs btn-info"><i class="fa fa-download"></i> Pobierz wyniki</a>
              </div>
              <div class="col-md-8">
        {{ Form::model($stage_result, [
            'route' => ['race.race.set-result', 'stage' => $race->stage_id, 'race' => $races],
            'class' => 'form-inline'
        ]) }}
              {{ Form::label('length', 'Dystans wyścigu (km)', ['class' => '']) }}
              {{ Form::text('length', $race->length, ['class' => '', 'autocomplete' => 'off']) }}
              {{ Form::hidden('length_change', 1, ['id' => 'team_id_new']) }}
              {{ Form::submit('Zapisz', ['class' => 'btn btn-success']) }}
        {{ Form::close() }}
        </div>
              <table class="table table-condensed">
                <tr>

                  <th>Poz</th>
                  <th>Nr</th>
                  <th>Nazwa</th>
                  <th>Drużyna</th>
                  <th>Czas</th>
                </tr>
                @foreach($results_all as $row)
                    @if($row->is_finish) 
                    <tr class="success">
                    @elseif($row->info)
                    <tr class="warning">
                    @else 
                    <tr class="danger">
                    @endif
                      <td
                          @if($loop->iteration != $row->pos_finish)
                          class="text-red" 
                          @endif
                          >{{ $row->pos }}</td>
                      <td><strong>{{ $row->bib }}</strong></td>
                      <td><a href="{{ route('player.edit', ['id' => $row->player->id, 'stage_id' => $race->stage_id]) }}">
                          {{ $row->player->surname }} {{ $row->player->name }}
                      </a></td>
                      <td>{{ $row->team->name ?? '' }}</td>
                      <td>{{ $row->info ?? $row->time }}</td>
                    </tr>
                @endforeach
              </table>
            </div>
        </div>
    </div>
    @endif
  </div>
</section>

@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script>
$(function () {
    $('#bib').focus();
    
    var ez_minus_sec = localStorage.getItem("ez_minus_sec");
    if(ez_minus_sec){
        $('#minus').val(ez_minus_sec);
    }else{
        $('#minus').val(0);
    }
    $('#minus').keyup(function(){
        localStorage.setItem("ez_minus_sec", $('#minus').val());
    });
    
    $(window).on("keydown", function( event ){
        // f
        if(event.which === 70){
            $('#info').val('dnf');
        }
        // s
        if(event.which === 83){
            $('#info').val('dns');
        }
    });
    var availableTags = ["-1", "-2", "-3", "-4", "-5", "dnf", "dns", "dsq", "otl"];
    $('#info').autocomplete({
        source: availableTags
    });
});
</script>
@endsection