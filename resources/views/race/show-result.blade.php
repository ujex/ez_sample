@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title col-md-6">Wyniki {{ $race->stage->name }} - {{ $race->name }}</h3>
          <div class="col-md-6"><a href="{{ route('race.race', ['id' => $race->stage_id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i> Powrót do listy wyścigów</a></div>
        </div>
        <div class="box-body table-responsive">
          {{ Form::open(['route' => ['race.race.show-result', 'race' => $races]]) }}
          <table class="table table-bordered table-hover table-striped table-condensed-max">
            <thead>
            <tr>
              <th>#</th>
              <th style="width:60px;">Poz</th>
              <th style="width:60px;">Nr</th>
              <th>Nazwa</th>
              <th>UCI ID</th>
              <th>Drużyna</th>
              <th style="width:50px;">Ukończył</th>
              <th style="width:50px;">Wprowadzony</th>
              <th style="width:80px;">Czas</th>
              <th style="width:70px;">Info</th>
              <th style="width:73px;">Punkty</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($stage_result as $row)
            <tr style="line-height: 10px; min-height: 10px; height: 10px;">
              <td class="ez-iter">{{ $loop->iteration }}</td>
              <td>{{ Form::hidden($row->id.'[id]', $row->id) }}{{ Form::text($row->id.'[pos]', $row->pos, ['class' => 'form-control input-sm']) }}</td>
              <td><strong>{{ $row->bib }}</strong></td>
              <td><a href="{{ route('player.edit', ['id' => $row->player->id, 'stage_id' => $stage->id]) }}">
                      <strong>{{ $row->ext }}{{ $row->player->surname }} {{ $row->player->name }}</strong> 
                      @if(in_array((date('Y')-$row->player->birth_year()), [19,20,21,22]))
                      <span class="text-red">
                      @else
                      <span>
                      @endif
                      [rocznik: {{ $row->player->birth_year() }}]
                      </span>
              </a></td>
              <td>{{ $row->player->uci_id }}</td>
              <td>{{ $row->team->name ?? '' }}</td>
              <td>
                  {{ Form::hidden($row->id.'[is_finish]', 0) }}
                  {{ Form::checkbox($row->id.'[is_finish]', 1, $row->is_finish, ['class' => 'flat-green']) }}
                  {{-- Form::text($row->id.'[is_finish]', $row->is_finish, ['class' => 'form-control input-sm']) --}}
              </td>
              <td>
                  {{ Form::hidden($row->id.'[is_set]', 0) }}
                  {{ Form::checkbox($row->id.'[is_set]', 1, $row->is_set, ['class' => 'flat-green']) }}
              </td>
              <td>{{ Form::text($row->id.'[time]', $row->time, ['class' => 'form-control input-sm time']) }}</td>
              <td>{{ Form::text($row->id.'[info]', $row->info, ['class' => 'form-control input-sm']) }}</td>
              <td>{{ Form::text($row->id.'[point]', $row->point, ['class' => 'form-control input-sm']) }}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
          {{ Form::submit('Zapisz', ['class' => 'btn btn-info pull-right']) }}
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
    
<div class="row">
  <div class="col-md-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Linki</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <ul class="col-md-3 list-unstyled text-bold">
                <li><h4>Lista startowa:</h4></li>
              <li><a href="{{ url('prints/start/race/pdf/st', $races) }}" class="from-checkbox">Pobierz Pdf</a></li>
              <li><a href="{{ url('prints/start/race/xls/st', $races) }}" class="from-checkbox">Pobierz csv</a></li>
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
                <li><h4>Wyniki:</h4></li>
                <li><a href="{{ url('prints/result/race/pdf', $races) }}" class="from-checkbox">Pobierz Pdf</a></li>
                <li><a href="{{ url('prints/result/race/xls', $races) }}" class="from-checkbox">Pobierz csv</a></li>
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
                <li><h4>Zarządzaj:</h4></li>
                <li><a href="{{ route('race.stage.enrol', ['id' => $stage->id]) }}" class="from-checkbox">Lista zgłoszeń</a></li>
                <li><a href="{{ route('s.sign', ['id' => $stage->id]) }}" class="from-checkbox" target="_blank">Dodaj zawodnika</a></li>
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
                <li><h4>Narzędzia:</h4></li>
                <li><a href="{{ route('calc.recalculate', ['id' => $races]) }}" class="from-checkbox">Przelicz punkty</a></li>
            </ul>
            <br style="clear:both;">
        </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script>
$(function () {
    check_times();
});
function check_times(){
    var time_start = '00:00:00';
    $('.time').each(function(){
        $(this).removeClass('bg-red');
        if($(this).val() < time_start){
            $(this).addClass('bg-red');
        }
        time_start = $(this).val();
    });
}
</script>
@endsection