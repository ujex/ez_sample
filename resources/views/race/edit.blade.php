@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Edycja wyścigu</h3>
          <a href="{{ route('race.stage.edit', ['id' => $race->stage_id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i></a>
        </div>
        {{ Form::model($race, [
            'route' => ['race.race.edit', 'id' => $race->id],
            'class' => 'form-horizontal'
        ]) }}
        <div class="box-body">
            @include('inc.errors')
            <div class="form-group">
              {{ Form::label('name', 'Nazwa', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::text('name', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group">
              {{ Form::label('start_time', 'Godzina startu', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::text('start_time', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group">
              {{ Form::label('length', 'Długość (km)', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::text('length', null, ['class' => 'form-control', 'placeholder' => '0.00']) }}</div>
            </div>

            <div class="form-group">
              {{ Form::label('category_id', 'Kategoria', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::select('category_id', $category, null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group">
              {{ Form::label('info', 'Info', ['class' => 'col-sm-2 control-label']) }}
              <div class="col-sm-10">{{ Form::textArea('info', null, ['class' => 'form-control']) }}</div>
            </div>

          <div class="box-footer">
            {{ Form::submit('Save', ['class' => 'btn btn-info pull-right']) }}
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Linki</h3>
            </div>
            <div class="box-body">
                <a href="{{ route('race.race.show-start', ['race' => $race->id]) }}" title="Lista startowa"><i class="fa fa-lg fa-list"></i> Lista startowa</a><br />
                <a href="{{ route('race.race.set-result', ['stage' => $race->stage->id, 'race' => $race->id]) }}" title="Wprowadź wyniki"><i class="fa fa-lg fa-tasks"></i> Wprowadź wyniki</a><br />
                <a href="{{ route('race.race.show-result', ['race' => $race->id]) }}" title="Przeglądaj wyniki"><i class="fa fa-lg fa-cubes"></i> Przeglądaj wyniki</a><br />
            </div>
        </div>
    </div>

  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
$('#info').wysihtml5();
$(function () {
    $('#length').focus();

});
</script>
@endsection