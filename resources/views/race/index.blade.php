@extends('layouts.main')

@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/all.css') }}">
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <!-- /.box-header -->
        @foreach ($data as $stage)
        @php
        $total = 0;
        @endphp
        <div class="box-header">
          <h3 class="box-title col-md-6">{{ $stage->name }}</h3>
          <div class="col-md-6"><a href="{{ route('race.stage.edit', ['id' => $stage->id]) }}" class="btn btn-xs btn-info" ><i class="fa fa-history"></i> Powrót do edycji zawodów</a></div>
        </div>
        <div class="box-body table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></th>
                <th>Nazwa</th>
                <th>Zawodników</th>
                <th>Kategoria (wiek)</th>
                <th>Długość</th>
                <th>Godzina startu</th>
                <th>Akcje</th>
                <th>Wyniki</th>
              </tr>
            </thead>
            <tbody>

              @foreach ($stage->races as $row)
              @php
              $total += $row->results->count();
              @endphp

              @if($row->length>0) 
              <tr class="success">
                @else 
              <tr class="danger">
                @endif
                <td>
                  {{ Form::checkbox('id', $row->id, null, ['class' => 'flat-green']) }}
                </td>
                <td><a href="{{ route('race.race.set-result', ['stage' => $stage->id, 'race' => $row->id]) }}" title="Wprowadź wyniki">{{ $row->name }}</a></td>
                @if($row->results->count())
                <td>{{ $row->results->count() }}</td>
                @else
                <td class="text-red">{{ $row->results->count() }}</td>
                @endif
                @if(strlen($row->range) > 50)
                <td><small>{{ substr($row->range, 0, 20) }} . . . {{ substr($row->range, -20, 20) }}</small></td>
                @else
                <td><small>{{ $row->range }}</small></td>
                @endif
                <td><small>{{ round($row->length/1, 2) }} km</small></td>
                <td>{{ $row->start_time }}</td>
                <td>
                  <a href="{{ route('race.race.show-start', ['race' => $row->id]) }}" title="Lista startowa"><i class="fa fa-lg fa-list"></i></a>
                  <a href="{{ route('race.race.set-result', ['stage' => $stage->id, 'race' => $row->id]) }}" title="Wprowadź wyniki"><i class="fa fa-lg fa-tasks"></i></a>
                  <a href="{{ route('race.race.show-result', ['race' => $row->id]) }}" title="Przeglądaj wyniki"><i class="fa fa-lg fa-cubes"></i></a>

                  &nbsp;&nbsp;&nbsp;
                  <a href="{{ route('race.race.edit', ['id' => $row->id]) }}" title="Edycja wyścigu"><i class="fa fa-lg fa-edit"></i></a>
                </td>
                <td>
                  <a href="{{ route('prints.result.race', ['type' => 'xls', 'id' => $row->id]) }}"><i class="fa fa-lg fa-file-excel-o"></i></a>
                  <a href="{{ route('prints.result.race', ['type' => 'pdf', 'id' => $row->id]) }}"><i class="fa fa-lg fa-file-pdf-o"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th></th>
                <th>Razem: </th>
                <th>{{ $total }}</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Linki</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Lista startowa:</h4></li>
              <li><a href="{{ url('prints/start/race/pdf/bib', 0) }}" class="from-checkbox">Pobierz Pdf wg numerów</a></li>
              <li><a href="{{ url('prints/start/race/xls/bib', 0) }}" class="from-checkbox">Pobierz csv wg numerów</a></li>
              <li><a href="{{ url('prints/start/race/pdf/cls', 0) }}" class="from-checkbox">Pobierz Pdf wg klasyfikacji</a></li>
              <li><a href="{{ url('prints/start/race/xls/cls', 0) }}" class="from-checkbox">Pobierz csv wg klasyfikacji</a></li>
              <li><a href="{{ url('prints/start/race/pdf/rand', 0) }}" class="from-checkbox">Pobierz Pdf losowo</a></li>
              <li><a href="{{ url('prints/start/race/xls/rand', 0) }}" class="from-checkbox">Pobierz csv losowo</a></li>
              @if(Auth::user()->role == 'admin')
              <li><a href="{{ url('prints/start/race/pdf/rank', 0) }}" class="from-checkbox">Pobierz Pdf wg rankingów</a></li>
              <li><a href="{{ url('prints/start/race/xls/rank', 0) }}" class="from-checkbox">Pobierz csv wg rankingów</a></li>
              @endif
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Wyniki:</h4></li>
              <li><a href="{{ url('prints/result/race/pdf', 0) }}" class="from-checkbox">Pobierz Pdf</a></li>
              <li><a href="{{ url('prints/result/race/xls', 0) }}" class="from-checkbox">Pobierz csv</a></li>
              <li><a href="{{ route('prints.result.team', ['id' => $stage->id, 'type' => 'pdf']) }}" class="">Pobierz klasyfikację drużynową zawodów</a></li>
              <li><a href="{{ url('prints/result/general/pdf/'.$stage->id.'?races=') }}"  class="from-checkbox">Pobierz klasyfikację generalną</a></li>
<!--              <li><a href="{{ route('prints.result.general', ['id' => $stage->id, 'type' => 'pdf']) }}"  class="">Pobierz klasyfikację generalną</a></li>-->
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Zarządzaj:</h4></li>
              <li><a href="{{ route('race.stage.enrol', ['id' => $stage->id]) }}" class="">Lista zgłoszeń</a></li>
              <li><a href="{{ route('s.sign', ['id' => $stage->id]) }}" class="" target="_blank">Dodaj zawodnika</a></li>
            </ul>
            <ul class="col-md-3 list-unstyled text-bold">
              <li><h4>Klasyfikacje:</h4></li>
              <li><a href="{{ route('calc.team', ['id' => $stage->id]) }}" class="">Przelicz klasyfikację drużynową zawodów</a></li>
              <li><a href="{{ route('calc.general', ['id' => $stage->id]) }}" class="">Przelicz klasyfikację generalną</a></li>
            </ul>
            <br style="clear:both;">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
$(function () {
    $('input[type="checkbox"].flat-green').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $(".checkbox-toggle").click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            $('input[type="checkbox"].flat-green').iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            $('input[type="checkbox"].flat-green').iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    $('.from-checkbox').click(function () {
        var curr_link = $(this);
        $('input[type="checkbox"].flat-green').each(function (i, e) {
            if ($(this).is(':checked')) {
                var href = curr_link.attr('href');
                href = href + $(this).attr('value') + ",";
                curr_link.attr('href', href);
            }
        });
    });
    $('.ez-load-file').click(function () {
        var _curr = $('#races').val();
        $('input[type="checkbox"].flat-green').each(function (i, e) {
            if ($(this).is(':checked')) {
                _curr = _curr + $(this).attr('value') + ",";
            }
        });
        $('#races').val(_curr);
    });
//    $('#start-many').click(function(){
//        $('input[type="checkbox"].flat-green').each(function(i, e){
//            if($(this).is(':checked')){
//                var href = $('#start-many').attr('href');
//                href = href + $(this).attr('value') + ",";
//                $('#start-many').attr('href', href);
//            }
//        });
//    });
//    $('#result-many').click(function(){
//        $('input[type="checkbox"].flat-green').each(function(i, e){
//            if($(this).is(':checked')){
//                var href = $('#result-many').attr('href');
//                href = href + $(this).attr('value') + ",";
//                $('#result-many').attr('href', href);
//            }
//        });
//    });
});
</script>
@endsection