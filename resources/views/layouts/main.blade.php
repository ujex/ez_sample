<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>biurozawodow.eu</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('assets/lte/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
  
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/lte/dist/css/skins/skin-black.min.css') }}">
  
  <link rel="stylesheet" href="{{ asset('assets/css/ez.css') }}">
  @yield('page-css')
</head>
<body class="layout-top-nav hold-transition skin-black sidebar-mini sidebar-collapse">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          @if (Auth::user()->role == 'admin')
          <li class="dropdown messages-menu"><a href="{{ route('race.competition') }}" title="Lista serii"><i class="fa fa-list"></i> Lista serii</a></li>
          @endif
          <li class="dropdown messages-menu"><a href="{{ route('race.stage') }}" title="Lista zawodów"><i class="fa fa-list"></i> Lista zawodów</a></li>
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('assets/lte/dist/img/avatar5.png') }}" class="user-image" alt="{{ Auth::user()->name }}">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('assets/lte/dist/img/avatar5.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
                <p><a href="{{ route('profile.show') }}">
                  {{ Auth::user()->name }} {{ Auth::user()->surname }} - {{ Auth::user()->type }}
                  <small>Zarejestrowany od {{ Auth::user()->created_at->format('j.m.Y') }}</small>
                </a></p>
              </li>
              <li class="user-footer">
                <div class="pull-left">

                </div>
                <div class="pull-right">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-jet-dropdown-link class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-jet-dropdown-link>
                            </form>

                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 
 
 @yield('content')
 
 
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> {{config('ez.version') }}
    </div>
  </footer>
</div>
<!-- ./wrapper -->
<script>is_scan=false;</script>
<!-- jQuery 2.2.0 -->
<script src="{{ asset('assets/lte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/lte/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('assets/lte/dist/js/app.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/jQuery-Scanner-Detection/jquery.scannerdetection.js') }}"></script>
<script src="{{ asset('assets/js/ez.js') }}"></script>

<!-- page script -->
@yield('page-script')
<script>
$(function () {
    // Scanner
    $(window).scannerDetection();
    $(window).bind('scannerDetectionComplete',function(e,data){
        if(!is_scan){
            location.href = '/player/scanner/' + data.string;
        }
    })
        .bind('scannerDetectionError',function(e,data){
        //console.log('detection error '+data.string);
    })
        .bind('scannerDetectionReceive',function(e,data){
        //console.log(data);
    });

    // Test scan
    $('#scanner-test-btn').click(function(){
        console.log(1, $('#scanner-test-input').val());
        $(window).scannerDetection($('#scanner-test-input').val());
    });
});
</script>
</body>
</html>
