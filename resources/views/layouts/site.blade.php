<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <!--<![endif]-->

  <head>
    <title>{{ $title ?? '' }} Biuro zawodów</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Hsoft">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="/css/style.css?c">
    @yield('page-css')
  </head>

  <body>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
      <div class="cy_top_info">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="cy_top_detail">
                <ul>
                  <li><a href="#">EMAIL: kontakt@biurozawodow.eu</a></li>
                  <li>TELEFON: +48 503 143 095</li>
                  <!--
                  <li>
                      <ul>
                          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
                      </ul>
                  </li>
                  <li class="cart"><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                      <span>2</span></a>
                      <ul class="cart_box">
                          <li>
                              <div class="cart_section">
                                  <div class="cart_img">
                                      <a href="#"><img src="http://via.placeholder.com/68x68" alt="cart" class="img-fluid"></a>
                                  </div>
                                  <div class="cart_detail">
                                      <h4><a href="cart.html">Road Bicycle</a></h4>
                                      <h5>$2200</h5>
                                  </div>
                                  <a class="cart_delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                          </li>
                          <li>
                              <div class="cart_section">
                                  <div class="cart_img">
                                      <a href="#"><img src="http://via.placeholder.com/68x68" alt="cart" class="img-fluid"></a>
                                  </div>
                                  <div class="cart_detail">
                                      <h4><a href="cart.html">Road Bicycle</a></h4>
                                      <h5>$2500</h5>
                                  </div>
                                  <a class="cart_delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                              </div>
                          </li>
                          <li>
                              <div class="cart_section">
                                  <div class="cart_total">
                                      <h4>Total<span>$4700</span></h4>
                                  </div>
                              </div>
                          </li>
                          <li>
                              <div class="cart_section">
                                  <a href="cart.html" class="cy_button">view cart</a>
                                  <a href="checkout.html" class="cy_button">Checkout</a>
                              </div>
                          </li>
                      </ul>
                  </li>
                  -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- sign icons -->
    <div class="cy_sign_wrapper">
      @if (Auth::guest())
      <h2 class="signin"><i class="fa fa-sign-in" aria-hidden="true"></i><a href="/login" >Zaloguj</a></h2>
      @else
      <h2 class="signin"><i class="fa fa-dashboard" aria-hidden="true"></i><a href="/p/stage" >Panel</a></h2>
      @endif
        <!--<h2 class="signup"><i class="fa fa-user-plus" aria-hidden="true"></i><a href="#signup" data-toggle="modal">Sign Up</a></h2>-->
    </div>
    <!-- sign in modal -->
    <div class="modal" id="signin">
      <div class="cy_signin">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="cy_sign_img">
              <img src="/images/popup.jpg" alt="popup" class="img-fluid">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="cy_sign_form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h1>Zaloguj się</h1>
              <input type="text" placeholder="Username" class="form-control">
              <input type="password" placeholder="Password" class="form-control">
              <div class="forget_password">
                <div class="remember_checkbox">
                  <label>Pozostań zalogowany
                    <input type="checkbox">
                    <span class="checkmark"></span>
                  </label>
                </div>
                <a href="#">Zapomniałeś hasła ?</a>
              </div>
              <a href="#" class="cy_button">Zaloguj się</a>
              <!--<p>Dont Have An Account? <a href="#signup" data-toggle="modal" class="cy_modal">Sign Up</a></p>-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- sign up modal -->
    <div class="modal" id="signup">
      <div class="cy_signup">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="cy_sign_img">
              <img src="http://via.placeholder.com/376x496" alt="popup" class="img-fluid">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="cy_sign_form">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h1>Sign up</h1>
              <input type="text" placeholder="Username" class="form-control">
              <input type="text" placeholder="Email or Phone" class="form-control">
              <input type="password" placeholder="Password" class="form-control">
              <a href="#" class="cy_button">register</a>
              <p>Already Have An Account? <a href="#">Sign In</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Banner section start-->
    <div class="cy_bread_wrapper">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <h1>WIRTUALNE BIURO ZAWODÓW</h1>
          </div>
        </div>
      </div>
    </div>
    <!--Menus Start-->
    <div class="cy_menu_wrapper">
      <div class="cy_logo_box" >
        <a href="/"><img src="/images/logo.png" alt="logo" class="img-fluid"/></a>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <button class="cy_menu_btn">
              <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <div class="cy_menu">
              <nav>
                <ul>
                  <li><a href="/">home</a></li>
                  <li><a href="{{ route('s.page', ['slug' => 'o-nas']) }}">o nas</a></li>
                  <li class="dropdown"><a href="javascript:;">zawody</a>
                    <ul class="sub-menu">
                      <li><a href="{{ route('s.list', ['year' => '2021']) }}">2021</a></li>
                      <li><a href="{{ route('s.list', ['year' => '2020']) }}">2020</a></li>
                    </ul>
                  </li>
                  <li><a href="{{ route('s.list.sign') }}">zgłoszenia</a></li>
                  <!--
                  <li class="dropdown"><a href="blog.html">blog</a>
                      <ul class="sub-menu">
                          <li><a href="blog.html">blog</a></li>
                          <li><a href="blog_single.html">blog single</a></li>
                      </ul>
                  </li>
                  <li class="dropdown"><a href="javascript:;">pages</a>
                      <ul class="sub-menu">
                          <li><a href="shop.html">shop</a></li>
                          <li><a href="shop_single.html">shop single</a></li>
                          <li><a href="cart.html">cart</a></li>
                          <li><a href="checkout.html">checkout</a></li>
                          <li><a href="404.html">404</a></li>
                      </ul>
                  </li>
                  <li><a href="gallery.html">gallery</a></li>-->
                  <li><a href="{{ route('s.page', ['slug' => 'kontakt']) }}">kontakt</a></li>
                </ul>
              </nav>
            </div>
            <div class="cy_search">
              <a href="#" class="search_open"><i class="fa fa-search"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- search section -->
    <div class="cy_search_form">
      <form class="search-form" method="POST" action="{{ route('s.search') }}">
        @csrf
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
          <input type="search" name="q_s" placeholder="szukaj">
          <i class="fa fa-search"></i>
        </div>
      </form>
    </div>
    <!--Blog section start-->
    <div class="cy_event_wrapper cy_event_single">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-12">

            @yield('content')


          </div>
          <!--Sidebar Start-->
          <div class="col-lg-4 col-md-12">
            <div class="sidebar cy_sidebar cy_event_sidebar">
              <!--Search-->
              <div class="widget widget_search">
                <form class="search-form" method="POST" action="{{ route('s.search') }}">
                  @csrf
                  <div class="input-group">
                    <input type="text" value="" name="s_q" class="form-control" placeholder="Szukaj">
                    <span class="cy_search_btn">
                      <button class="btn btn-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div>
                </form>
              </div>
              <!--Categories-->
              <!--
              <div class="widget widget_categories">
                  <h3 class="widget-title">Categories</h3>
                  <ul>
                      <li><a href="#"> Cycling</a></li>
                      <li><a href="#"> Biking</a></li>
                      <li><a href="#"> Rider</a></li>
                      <li><a href="#"> Sports</a></li>
                      <li><a href="#"> Life Style</a></li>
                      <li><a href="#"> Cycling</a></li>
                      <li><a href="#"> biking</a></li>
                  </ul>
              </div>
              -->
              <!--Recent Post-->
              @if($next_stage)
              <div class="widget widget_recent_entries">
                <h3 class="widget-title">NAJBLIŻSZE ZAWODY</h3>
                <ul>
                  @foreach($next_stage as $stage)
                  <li>
                    <div class="recent_cmnt_img">
                      <img src="/img/thumbnail/70/{{ $stage->image }}" alt="post" />
                    </div>
                    <div class="recent_cmnt_data">
                      <h4><a href="{{ route('s.stage', ['id' => $stage->id]) }}">{{ $stage->name }}</a></h4>
                      <span>{{ ez_date_long($stage->date) }}</span>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
              @endif
              <!--Classes-->
              @if($last_stage)
              <div class="widget event_class">
                <h3 class="widget-title">OSTATNIE WYNIKI</h3>
                <ul>
                  @foreach($last_stage as $stage)
                  <li>
                    <div class="ev_cls_img">
                      <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
                    </div>
                    <div class="ev_cls_data">
                      <h4><a href="{{ route('s.stage', ['id' => $stage->id]) }}">{{ $stage->name }}</a></h4>
                      <p>{{ ez_date_long($stage->date) }}</p>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
              @endif
              <!--Tag Cloud-->
              <div class="widget widget_tag_cloud">
                <h3 class="widget-title">Tagi</h3>
                <ul>
                  <li><a href="{{ route('s.search', ['q' => 'Kolarstwo']) }}">Kolarstwo</a></li>
                  <li><a href="{{ route('s.search', ['q' => 'MTB']) }}">MTB</a></li>
                  <li><a href="{{ route('s.search', ['q' => 'Szosa']) }}">Szosa</a></li>
                  <li><a href="{{ route('s.search', ['q' => 'Tor']) }}">Tor</a></li>
                  <li><a href="{{ route('s.search', ['q' => 'Przełaj']) }}">Przełaj</a></li>
                  <li><a href="{{ route('s.page', ['slug' => 'o-nas']) }}">Biuro zawodów</a></li>
                  <li><a href="{{ route('s.list.sign') }}">Zapisy</a></li>
                  <li><a href="/">Wyniki</a></li>
                  <!--<li><a href="#">Club </a></li>-->
                </ul>
              </div>
              <!--Sociallinks-->
              <!--
              <div class="widget widget_social_links">
                  <h3 class="widget-title">follow us</h3>
                  <ul>
                      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                  </ul>
              </div>
              -->
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--Footer section start-->
    <div class="cy_footer_wrapper cy_section_padding padder_bottom75">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="footer-widget cy_footer_about">
              <img src="/images/logo.png" alt="logo" class="img-fluid"/>
<!--                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>-->
            </div>  
          </div>
          <!--
          <div class="col-lg-3 col-md-6">
              <div class="footer-widget">
              <h1 class="widget-title">recent blog</h1>
              <div class="cy_recent_post">
                  <div class="cy_post_img">
                      <a href="#"><img src="http://via.placeholder.com/70x70" alt="post"/></a>
                  </div>
                  <div class="cy_post_data">
                      <h3><a href="#">Selecting The Proper Bicycle</a></h3>
                      <p>14 Apr, 2017</p>
                  </div>  
              </div>
              <div class="cy_recent_post">
                  <div class="cy_post_img">
                      <a href="#"><img src="http://via.placeholder.com/70x70" alt="post"/></a>
                  </div>
                  <div class="cy_post_data">
                      <h3><a href="#">Selecting The Proper Bicycle</a></h3>
                      <div class="cy_post_info"><p>14 Apr, 2017</p></div>
                  </div>  
              </div>
              </div>
          </div>
          -->
          <div class="col-lg-3 col-md-6">
            <div class="footer-widget">
              <h1 class="widget-title">KONTAKT</h1>
            </div>
            <div class="cy_foo_contact">
              <span><img src="/images/svg/map-mark.svg" alt="map-mark"></span>
              <div class="cy_post_info">
                <p>Katowice, woj. śląskie, Polska</p>
              </div>
            </div>
            <div class="cy_foo_contact">
              <span><img src="/images/svg/phone.svg" alt="phone"></span>
              <div class="cy_post_info">
                <p>Telefon: +48 503 143 095</p>
                <!--<p>Office :- +1-512-555-0190</p>-->
              </div>
            </div>
            <div class="cy_foo_contact">
              <span><img src="/images/svg/email.svg" alt="email"></span>
              <div class="cy_post_info">
                <p><a href="mailto:kontakt@biurozawodow.eu">kontakt@biurozawodow.eu</a></p>
              </div>
            </div>
          </div>
          <!--
          <div class="col-lg-3 col-md-6">
              <div class="footer-widget">
                  <h1 class="widget-title">flicker widget</h1>
                  <div class="cy_foo_gallery">
                      <ul>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                          <li><a href="#"><img src="http://via.placeholder.com/78x78" alt="flicker gallery"></a></li>
                      </ul>
                  </div>
              </div>
          </div> 
          -->				
        </div>
      </div>
    </div>
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <P>Copyright &copy; 2021, <a href="/">Wirtualne Biuro Zawodów</a>. All Rights Reserved.</P>
          </div>
        </div>
      </div>
    </div>
    <!--Go to top start-->
    <div class="cy_go_to">
      <div class="cy_go_top">
        <img src="/images/go_to_top.png" alt="Back to top">
      </div>  
    </div>
    <!--scripts start-->
    <script src="/js/jquery.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/js/jquery.countTo.js"></script>    
    <script src="/js/appear.js"></script>
    <script src="/js/plugin/owl/owl.carousel.min.js"></script>
    <script src="/js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <script src="/js/jquery.nice-select.min.js"></script>
    <script src="/js/custom.js"></script>
    @yield('page-script')
  </body>

</html>