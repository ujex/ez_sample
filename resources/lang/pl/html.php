<?php

return [
    'player' => [
        'add' => [
            'form' => [
                'surname' => 'Nazwisko',
                'name' => 'Imię',
                'birth' => 'Data ur.',
                'uci_id' => 'UCI ID',
                'team' => 'Klub',
                'team_from' => 'Klub od',
                'country' => 'Kraj',
                'sex' => 'Płeć',
                'type' => 'Typ',
                'medical' => 'Badania',
                'email' => 'Email',
                'phone' => 'Telefon',
                'save' => 'Zapisz'
            ]
        ]
    ]
];
